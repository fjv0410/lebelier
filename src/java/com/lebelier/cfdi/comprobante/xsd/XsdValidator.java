package com.lebelier.cfdi.comprobante.xsd;

import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import com.lebelier.suppliers.web.exception.CfdiXsdValidationException;

public class XsdValidator {
	
	public void validateComprobanteUsingXsd(String p_fileName, InputStream p_isCfdiXmlFile) throws CfdiXsdValidationException {
		
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        
        try {        	
        	String sCfdXsdPath = getClass().getResource("cfdv33.xsd").toExternalForm();

        	Schema schemaCfd = schemaFactory.newSchema(new StreamSource(sCfdXsdPath));

            Validator validator = schemaCfd.newValidator();
            
            validator.validate(new StreamSource(p_isCfdiXmlFile));
            
        }catch (Exception e) {
        	e.printStackTrace();
        	
        	throw new CfdiXsdValidationException(p_fileName, e);
        	
        }
		
	}
	
}
