package com.lebelier.suppliers.db.utils;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;

import com.lebelier.suppliers.db.cayenne.entities.TInvoicesQuery;
import com.lebelier.suppliers.web.exception.Db_UuidAlredyExist;

public class InvoiceValidator {
	
    @Autowired
	public static void existUuid(ServerRuntime cayenneRuntime, String p_uuid) throws Db_UuidAlredyExist {
		
		ObjectContext cayenneContext = cayenneRuntime.newContext(); 
    	
		String sUuid = ObjectSelect.columnQuery(TInvoicesQuery.class, TInvoicesQuery.UUID).where(TInvoicesQuery.UUID.eq(p_uuid)).selectOne(cayenneContext);
		
		if(sUuid != null && !sUuid.trim().equals("")) {
			throw new Db_UuidAlredyExist(p_uuid);
		}
		
	}
}
