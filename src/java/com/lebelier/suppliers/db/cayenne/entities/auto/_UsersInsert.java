package com.lebelier.suppliers.db.cayenne.entities.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.cayenne.exp.Property;

import com.lebelier.suppliers.db.cayenne.entities.UsersBase;

/**
 * Class _UsersInsert was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _UsersInsert extends UsersBase {

    private static final long serialVersionUID = 1L; 

    public static final String ID_PK_COLUMN = "id";

    public static final Property<Integer> ID_LANGUAGE = Property.create("id_language", Integer.class);
    public static final Property<Integer> ID_SUPPLIERS = Property.create("id_suppliers", Integer.class);

    protected int id_language;
    protected int id_suppliers;


    public void setId_language(int id_language) {
        beforePropertyWrite("id_language", this.id_language, id_language);
        this.id_language = id_language;
    }

    public int getId_language() {
        beforePropertyRead("id_language");
        return this.id_language;
    }

    public void setId_suppliers(int id_suppliers) {
        beforePropertyWrite("id_suppliers", this.id_suppliers, id_suppliers);
        this.id_suppliers = id_suppliers;
    }

    public int getId_suppliers() {
        beforePropertyRead("id_suppliers");
        return this.id_suppliers;
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "id_language":
                return this.id_language;
            case "id_suppliers":
                return this.id_suppliers;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "id_language":
                this.id_language = val == null ? 0 : (int)val;
                break;
            case "id_suppliers":
                this.id_suppliers = val == null ? 0 : (int)val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeInt(this.id_language);
        out.writeInt(this.id_suppliers);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.id_language = in.readInt();
        this.id_suppliers = in.readInt();
    }

}
