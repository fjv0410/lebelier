package com.lebelier.suppliers.db.cayenne.entities.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.cayenne.exp.Property;

import com.lebelier.suppliers.db.cayenne.entities.TSuppliersContactsBase;

/**
 * Class _TSuppliersContactsInsert was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _TSuppliersContactsInsert extends TSuppliersContactsBase {

    private static final long serialVersionUID = 1L; 

    public static final String ID_CONTACT_TYPE_PK_COLUMN = "id_contact_type";
    public static final String ID_SUPPLIER_PK_COLUMN = "id_supplier";

    public static final Property<Integer> ID_SUPPLIER = Property.create("id_supplier", Integer.class);
    public static final Property<Integer> ID_CONTACT_TYPE = Property.create("id_contact_type", Integer.class);

    protected int id_supplier;
    protected int id_contact_type;


    public void setId_supplier(int id_supplier) {
        beforePropertyWrite("id_supplier", this.id_supplier, id_supplier);
        this.id_supplier = id_supplier;
    }

    public int getId_supplier() {
        beforePropertyRead("id_supplier");
        return this.id_supplier;
    }

    public void setId_contact_type(int id_contact_type) {
        beforePropertyWrite("id_contact_type", this.id_contact_type, id_contact_type);
        this.id_contact_type = id_contact_type;
    }

    public int getId_contact_type() {
        beforePropertyRead("id_contact_type");
        return this.id_contact_type;
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "id_supplier":
                return this.id_supplier;
            case "id_contact_type":
                return this.id_contact_type;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "id_supplier":
                this.id_supplier = val == null ? 0 : (int)val;
                break;
            case "id_contact_type":
                this.id_contact_type = val == null ? 0 : (int)val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeInt(this.id_supplier);
        out.writeInt(this.id_contact_type);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.id_supplier = in.readInt();
        this.id_contact_type = in.readInt();
    }

}
