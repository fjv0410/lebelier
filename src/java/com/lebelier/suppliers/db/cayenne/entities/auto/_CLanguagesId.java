package com.lebelier.suppliers.db.cayenne.entities.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.cayenne.BaseDataObject;
import org.apache.cayenne.exp.Property;

import com.lebelier.suppliers.db.cayenne.entities.CCountriesTrans;
import com.lebelier.suppliers.db.cayenne.entities.CLanguagesTrans;
import com.lebelier.suppliers.db.cayenne.entities.CRolesTrans;
import com.lebelier.suppliers.db.cayenne.entities.UsersQuery;

/**
 * Class _CLanguagesId was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _CLanguagesId extends BaseDataObject {

    private static final long serialVersionUID = 1L; 

    public static final String ID_PK_COLUMN = "id";

    public static final Property<String> ID_SAP = Property.create("idSap", String.class);
    public static final Property<LocalDateTime> CREATED_ON = Property.create("createdOn", LocalDateTime.class);
    public static final Property<List<CLanguagesTrans>> C_LANGUAGES_TRANSS = Property.create("cLanguagesTranss", List.class);
    public static final Property<List<UsersQuery>> USERSS = Property.create("userss", List.class);
    public static final Property<List<CRolesTrans>> C_ROLES_TRANSS = Property.create("cRolesTranss", List.class);
    public static final Property<List<CCountriesTrans>> C_COUNTRIES_TRANSS = Property.create("cCountriesTranss", List.class);

    protected String idSap;
    protected LocalDateTime createdOn;

    protected Object cLanguagesTranss;
    protected Object userss;
    protected Object cRolesTranss;
    protected Object cCountriesTranss;

    public void setIdSap(String idSap) {
        beforePropertyWrite("idSap", this.idSap, idSap);
        this.idSap = idSap;
    }

    public String getIdSap() {
        beforePropertyRead("idSap");
        return this.idSap;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        beforePropertyWrite("createdOn", this.createdOn, createdOn);
        this.createdOn = createdOn;
    }

    public LocalDateTime getCreatedOn() {
        beforePropertyRead("createdOn");
        return this.createdOn;
    }

    public void addToCLanguagesTranss(CLanguagesTrans obj) {
        addToManyTarget("cLanguagesTranss", obj, true);
    }

    public void removeFromCLanguagesTranss(CLanguagesTrans obj) {
        removeToManyTarget("cLanguagesTranss", obj, true);
    }

    @SuppressWarnings("unchecked")
    public List<CLanguagesTrans> getCLanguagesTranss() {
        return (List<CLanguagesTrans>)readProperty("cLanguagesTranss");
    }

    public void addToUserss(UsersQuery obj) {
        addToManyTarget("userss", obj, true);
    }

    public void removeFromUserss(UsersQuery obj) {
        removeToManyTarget("userss", obj, true);
    }

    @SuppressWarnings("unchecked")
    public List<UsersQuery> getUserss() {
        return (List<UsersQuery>)readProperty("userss");
    }

    public void addToCRolesTranss(CRolesTrans obj) {
        addToManyTarget("cRolesTranss", obj, true);
    }

    public void removeFromCRolesTranss(CRolesTrans obj) {
        removeToManyTarget("cRolesTranss", obj, true);
    }

    @SuppressWarnings("unchecked")
    public List<CRolesTrans> getCRolesTranss() {
        return (List<CRolesTrans>)readProperty("cRolesTranss");
    }

    public void addToCCountriesTranss(CCountriesTrans obj) {
        addToManyTarget("cCountriesTranss", obj, true);
    }

    public void removeFromCCountriesTranss(CCountriesTrans obj) {
        removeToManyTarget("cCountriesTranss", obj, true);
    }

    @SuppressWarnings("unchecked")
    public List<CCountriesTrans> getCCountriesTranss() {
        return (List<CCountriesTrans>)readProperty("cCountriesTranss");
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "idSap":
                return this.idSap;
            case "createdOn":
                return this.createdOn;
            case "cLanguagesTranss":
                return this.cLanguagesTranss;
            case "userss":
                return this.userss;
            case "cRolesTranss":
                return this.cRolesTranss;
            case "cCountriesTranss":
                return this.cCountriesTranss;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "idSap":
                this.idSap = (String)val;
                break;
            case "createdOn":
                this.createdOn = (LocalDateTime)val;
                break;
            case "cLanguagesTranss":
                this.cLanguagesTranss = val;
                break;
            case "userss":
                this.userss = val;
                break;
            case "cRolesTranss":
                this.cRolesTranss = val;
                break;
            case "cCountriesTranss":
                this.cCountriesTranss = val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeObject(this.idSap);
        out.writeObject(this.createdOn);
        out.writeObject(this.cLanguagesTranss);
        out.writeObject(this.userss);
        out.writeObject(this.cRolesTranss);
        out.writeObject(this.cCountriesTranss);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.idSap = (String)in.readObject();
        this.createdOn = (LocalDateTime)in.readObject();
        this.cLanguagesTranss = in.readObject();
        this.userss = in.readObject();
        this.cRolesTranss = in.readObject();
        this.cCountriesTranss = in.readObject();
    }

}
