package com.lebelier.suppliers.db.cayenne.entities.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.cayenne.exp.Property;

import com.lebelier.suppliers.db.cayenne.entities.CContactsTypes;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersContactsBase;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;

/**
 * Class _TSuppliersContactsQuery was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _TSuppliersContactsQuery extends TSuppliersContactsBase {

    private static final long serialVersionUID = 1L; 

    public static final String ID_CONTACT_TYPE_PK_COLUMN = "id_contact_type";
    public static final String ID_SUPPLIER_PK_COLUMN = "id_supplier";

    public static final Property<CContactsTypes> C_CONTACTS_TYPES = Property.create("cContactsTypes", CContactsTypes.class);
    public static final Property<TSuppliersQuery> T_SUPPLIERS = Property.create("tSuppliers", TSuppliersQuery.class);


    protected Object cContactsTypes;
    protected Object tSuppliers;

    public void setCContactsTypes(CContactsTypes cContactsTypes) {
        setToOneTarget("cContactsTypes", cContactsTypes, true);
    }

    public CContactsTypes getCContactsTypes() {
        return (CContactsTypes)readProperty("cContactsTypes");
    }

    public void setTSuppliers(TSuppliersQuery tSuppliers) {
        setToOneTarget("tSuppliers", tSuppliers, true);
    }

    public TSuppliersQuery getTSuppliers() {
        return (TSuppliersQuery)readProperty("tSuppliers");
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "cContactsTypes":
                return this.cContactsTypes;
            case "tSuppliers":
                return this.tSuppliers;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "cContactsTypes":
                this.cContactsTypes = val;
                break;
            case "tSuppliers":
                this.tSuppliers = val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeObject(this.cContactsTypes);
        out.writeObject(this.tSuppliers);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.cContactsTypes = in.readObject();
        this.tSuppliers = in.readObject();
    }

}
