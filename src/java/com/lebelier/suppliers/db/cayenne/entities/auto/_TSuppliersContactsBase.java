package com.lebelier.suppliers.db.cayenne.entities.auto;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.cayenne.BaseDataObject;
import org.apache.cayenne.exp.Property;

/**
 * Class _TSuppliersContactsBase was generated by Cayenne.
 * It is probably a good idea to avoid changing this class manually,
 * since it may be overwritten next time code is regenerated.
 * If you need to make any customizations, please use subclass.
 */
public abstract class _TSuppliersContactsBase extends BaseDataObject {

    private static final long serialVersionUID = 1L; 

    public static final String ID_CONTACT_TYPE_PK_COLUMN = "id_contact_type";
    public static final String ID_SUPPLIER_PK_COLUMN = "id_supplier";

    public static final Property<String> NAME = Property.create("name", String.class);
    public static final Property<String> PHONE = Property.create("phone", String.class);
    public static final Property<String> EMAIL = Property.create("email", String.class);

    protected String name;
    protected String phone;
    protected String email;


    public void setName(String name) {
        beforePropertyWrite("name", this.name, name);
        this.name = name;
    }

    public String getName() {
        beforePropertyRead("name");
        return this.name;
    }

    public void setPhone(String phone) {
        beforePropertyWrite("phone", this.phone, phone);
        this.phone = phone;
    }

    public String getPhone() {
        beforePropertyRead("phone");
        return this.phone;
    }

    public void setEmail(String email) {
        beforePropertyWrite("email", this.email, email);
        this.email = email;
    }

    public String getEmail() {
        beforePropertyRead("email");
        return this.email;
    }

    @Override
    public Object readPropertyDirectly(String propName) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch(propName) {
            case "name":
                return this.name;
            case "phone":
                return this.phone;
            case "email":
                return this.email;
            default:
                return super.readPropertyDirectly(propName);
        }
    }

    @Override
    public void writePropertyDirectly(String propName, Object val) {
        if(propName == null) {
            throw new IllegalArgumentException();
        }

        switch (propName) {
            case "name":
                this.name = (String)val;
                break;
            case "phone":
                this.phone = (String)val;
                break;
            case "email":
                this.email = (String)val;
                break;
            default:
                super.writePropertyDirectly(propName, val);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        writeSerialized(out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        readSerialized(in);
    }

    @Override
    protected void writeState(ObjectOutputStream out) throws IOException {
        super.writeState(out);
        out.writeObject(this.name);
        out.writeObject(this.phone);
        out.writeObject(this.email);
    }

    @Override
    protected void readState(ObjectInputStream in) throws IOException, ClassNotFoundException {
        super.readState(in);
        this.name = (String)in.readObject();
        this.phone = (String)in.readObject();
        this.email = (String)in.readObject();
    }

}
