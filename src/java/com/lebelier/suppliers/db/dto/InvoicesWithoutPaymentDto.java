package com.lebelier.suppliers.db.dto;

public class InvoicesWithoutPaymentDto {
	
	private String supplier;
	private String reference;
	private String folio;
	private String company;
	private String paymentDoc;
	private String paymentDate;
	private String amount;
	
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPaymentDoc() {
		return paymentDoc;
	}
	public void setPaymentDoc(String paymentDoc) {
		this.paymentDoc = paymentDoc;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	
}
