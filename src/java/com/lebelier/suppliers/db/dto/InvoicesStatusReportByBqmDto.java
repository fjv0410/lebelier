package com.lebelier.suppliers.db.dto;

import java.util.List;

public class InvoicesStatusReportByBqmDto extends InvoicesStatusReportByCompanyDto {

	private List<InvoicesStatusReportDto> bqm;

	public List<InvoicesStatusReportDto> getBqm() {
		return bqm;
	}

	public void setBqm(List<InvoicesStatusReportDto> bqm) {
		this.bqm = bqm;
	}

}
