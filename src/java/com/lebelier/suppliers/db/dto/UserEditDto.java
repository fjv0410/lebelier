package com.lebelier.suppliers.db.dto;

public class UserEditDto extends UserDto {
	
	private int supplierId;
	private int roleId;
	
	public int getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
}
