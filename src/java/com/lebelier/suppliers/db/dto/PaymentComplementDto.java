package com.lebelier.suppliers.db.dto;

public class PaymentComplementDto {
	
	private String referenceDoc;
	private String referenceFolio;
	private String amount;
	private String paymentDoc;
	
	private String company;
	private String year;
	
	private boolean visible;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPaymentDoc() {
		return paymentDoc;
	}
	public void setPaymentDoc(String paymentDoc) {
		this.paymentDoc = paymentDoc;
	}
	public String getReferenceDoc() {
		return referenceDoc;
	}
	public void setReferenceDoc(String referenceDoc) {
		this.referenceDoc = referenceDoc;
	}
	public String getReferenceFolio() {
		return referenceFolio;
	}
	public void setReferenceFolio(String referenceFolio) {
		this.referenceFolio = referenceFolio;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
}
