package com.lebelier.suppliers.db.dto;

public class AnnouncementDto {
	
	private int id;
	private String date;
	private String subject;
	private String body;
	private String file_name;
	private boolean has_file;
        private int estatus;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public boolean isHas_file() {
		return has_file;
	}
	public void setHas_file(boolean has_file) {
		this.has_file = has_file;
	}

        public int getEstatus() {
            return estatus;
        }

        public void setEstatus(int estatus) {
            this.estatus = estatus;
        }
        
        
}
