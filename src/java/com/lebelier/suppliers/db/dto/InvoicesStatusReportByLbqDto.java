package com.lebelier.suppliers.db.dto;

import java.util.List;

public class InvoicesStatusReportByLbqDto extends InvoicesStatusReportByCompanyDto {

	private List<InvoicesStatusReportDto> lbq;

	public List<InvoicesStatusReportDto> getLbq() {
		return lbq;
	}

	public void setLbq(List<InvoicesStatusReportDto> lbq) {
		this.lbq = lbq;
	}

}
