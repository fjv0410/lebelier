package com.lebelier.suppliers.db.dto;

public class SupplierDto {
	
	private int id;
	private String idSap;
	private String country;
	private String name;
	private String taxId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdSap() {
		return idSap;
	}
	public void setIdSap(String idSap) {
		this.idSap = idSap;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	
}
