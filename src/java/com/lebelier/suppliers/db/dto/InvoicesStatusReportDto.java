package com.lebelier.suppliers.db.dto;

public class InvoicesStatusReportDto {
	
	private int id;
	
	private String reference;
	private String recDate;
	private String amount;
	private String currency;
	private String payDate;
	private String status;
	private String uuid;
	private String company;
	
	private String sYear;
	private String sDoc;
	private String sComp;
	private String docSap;
        private String numProveedor; 
        
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getRecDate() {
		return recDate;
	}
	public void setRecDate(String recDate) {
		this.recDate = recDate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getsYear() {
		return sYear;
	}
	public void setsYear(String sYear) {
		this.sYear = sYear;
	}
	public String getsDoc() {
		return sDoc;
	}
	public void setsDoc(String sDoc) {
		this.sDoc = sDoc;
	}
	public String getsComp() {
		return sComp;
	}
	public void setsComp(String sComp) {
		this.sComp = sComp;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

    public String getDocSap() {
        return docSap;
    }

    public void setDocSap(String docSap) {
        this.docSap = docSap;
    }

    public String getNumProveedor() {
        return numProveedor;
    }

    public void setNumProveedor(String numProveedor) {
        this.numProveedor = numProveedor;
    }

   
	
        
}
