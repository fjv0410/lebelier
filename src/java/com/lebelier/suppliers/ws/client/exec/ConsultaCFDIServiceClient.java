package com.lebelier.suppliers.ws.client.exec;

import javax.xml.bind.JAXBElement;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.lebelier.suppliers.web.exception.WebServiceSatException;
import com.lebelier.suppliers.ws.sat.client.types.Consulta;
import com.lebelier.suppliers.ws.sat.client.types.ConsultaResponse;
import com.lebelier.suppliers.ws.sat.client.types.ObjectFactory;

public class ConsultaCFDIServiceClient extends WebServiceGatewaySupport{
	
	public ConsultaResponse executeConsulta(String p_Wsdl, String p_action, String p_rfcEmisor, String p_rfcReceptor, String p_total, String p_uuid) throws WebServiceSatException{
		ConsultaResponse consultaResp;
		
		try {
			ObjectFactory objFact = new ObjectFactory();
			String sExpImp = "?re=" + p_rfcEmisor + "&rr=" + p_rfcReceptor + "&tt=" + p_total + "&id=" + p_uuid;
			JAXBElement<String> jbx = objFact.createConsultaExpresionImpresa(sExpImp);
			
			Consulta consulta = objFact.createConsulta();
			consulta.setExpresionImpresa(jbx);
			
	        consultaResp = (ConsultaResponse) getWebServiceTemplate().marshalSendAndReceive(
	        		p_Wsdl,
					consulta,
					new SoapActionCallback(p_action)
				);
		}catch(Exception e) {
			throw new WebServiceSatException(e);
		}
		
        return consultaResp;
        
	}
	
}
