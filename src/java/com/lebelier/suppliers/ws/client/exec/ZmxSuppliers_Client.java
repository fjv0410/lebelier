package com.lebelier.suppliers.ws.client.exec;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesCxp;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesCxpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesExt;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesExtResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmFacturasSinCp;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmFacturasSinCpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmInvoiceEstatus;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmInvoiceEstatusResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentComplment;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentComplmentResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentUuid;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentUuidResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmUpdateSupplier;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmUpdateSupplierResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmValidateDeliveries;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmValidateDeliveriesResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmFactAduana;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmFactAduanaResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60Cxp;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60CxpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01Response;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPrelmaryFv60Ext;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPrelmaryFv60ExtResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZttFactsincomp;
import com.lebelier.suppliers.ws.sap.client.types.ZttInoviceStatus;
import com.lebelier.suppliers.ws.sap.client.types.ZttPaymentComp;
import com.lebelier.suppliers.ws.sap.client.types.ZttRproveedor;

public class ZmxSuppliers_Client extends WebServiceGatewaySupport{

	public ZMxfmPaymentUuidResponse executeZMxfmPaymentUuid(
			ZMxfmPaymentUuid req,
			String sUri
		)
	{
		
		req.setBapiret2(new TableOfBapiret2());
		
		ZMxfmPaymentUuidResponse resp = (ZMxfmPaymentUuidResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				req
			);
		
		return resp;
	}

	public ZMxfmPaymentComplmentResponse executeZMxfmPaymentComplment(
			ZMxfmPaymentComplment req,
			String sUri
		)
	{
		
		req.setTtPaymentComp(new ZttPaymentComp());
		
		ZMxfmPaymentComplmentResponse resp = (ZMxfmPaymentComplmentResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				req
			);
		
		return resp;
	}

	public ZMxfmInvoiceEstatusResponse executeZMxfmInvoiceEstatus(
			ZMxfmInvoiceEstatus wsRequest,
			String sUri
		) 
	{

		wsRequest.setItFacturas(new ZttInoviceStatus());
		
		ZMxfmInvoiceEstatusResponse resp = (ZMxfmInvoiceEstatusResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZMxfmValidateDeliveriesResponse executeZMxfmValidateDeliveries(
			ZMxfmValidateDeliveries wsRequest,
			String sUri
		) 
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZMxfmValidateDeliveriesResponse resp = (ZMxfmValidateDeliveriesResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZMxfmDeliveriesCxpResponse executeZMxfmDeliveriesCxp(
			ZMxfmDeliveriesCxp wsRequest,
			String sUri
		) 
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZMxfmDeliveriesCxpResponse resp = (ZMxfmDeliveriesCxpResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZmxfmPreliminaryPostingFb01Response executeZmxfmPreliminaryPostingFb01(
			ZmxfmPreliminaryPostingFb01 wsRequest,
			String sUri
		)
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZmxfmPreliminaryPostingFb01Response resp = (ZmxfmPreliminaryPostingFb01Response) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZMxfmDeliveriesExtResponse executeZMxfmDeliveriesExt(
			ZMxfmDeliveriesExt wsRequest,
			String sUri 
		)
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZMxfmDeliveriesExtResponse resp = (ZMxfmDeliveriesExtResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZmxfmPrelmaryFv60ExtResponse executeZmxfmPrelmaryFv60Ext(
			ZmxfmPrelmaryFv60Ext wsRequest,
			String sUri 
		)
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZmxfmPrelmaryFv60ExtResponse resp = (ZmxfmPrelmaryFv60ExtResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}

	public ZmxfmPreliminaryFv60CxpResponse executeZmxfmPreliminaryFv60Cxp(
			ZmxfmPreliminaryFv60Cxp wsRequest,
			String sUri 
		)
	{
		
		wsRequest.setBapiret2(new TableOfBapiret2());
		
		ZmxfmPreliminaryFv60CxpResponse resp = (ZmxfmPreliminaryFv60CxpResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsRequest
			);
		
		return resp;
		
	}
	
	public ZMxfmFacturasSinCpResponse executeZMxfmFacturasSinCp(
			ZMxfmFacturasSinCp wsReq,
			String sUri
		) 
	{

		wsReq.setTFactUuid(new ZttFactsincomp());
		
		ZMxfmFacturasSinCpResponse resp = (ZMxfmFacturasSinCpResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsReq
			);
		
		return resp;
		
	}
	

	public ZMxfmUpdateSupplierResponse executeZMxfmUpdateSupplier(
			String sUri
		) 
	{

        ZMxfmUpdateSupplier wsReq = new ZMxfmUpdateSupplier();
        wsReq.setTProveedor(new ZttRproveedor());
        
		ZMxfmUpdateSupplierResponse resp = (ZMxfmUpdateSupplierResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsReq
			);
		
		return resp;
		
	}

	public ZmxfmFactAduanaResponse executeZmxfmFactAduana(
			ZmxfmFactAduana wsReq,
			String sUri
		) 
	{
		
        ZmxfmFactAduanaResponse resp = (ZmxfmFactAduanaResponse) getWebServiceTemplate().marshalSendAndReceive(
				sUri,
				wsReq
			);
		
		return resp;
		
	}

	
}
