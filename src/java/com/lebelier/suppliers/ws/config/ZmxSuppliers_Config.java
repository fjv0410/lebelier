package com.lebelier.suppliers.ws.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.ws.client.exec.ConsultaCFDIServiceClient;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;

@Configuration
@PropertySource({"classpath:/com/lebelier/suppliers/web/properties/suppliers_app.properties"})
public class ZmxSuppliers_Config {

	@Resource
    private Environment environment;
	
	@Bean
	public SaajSoapMessageFactory messageFactory() {
		return new SaajSoapMessageFactory();
	}
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPaths("com.lebelier.suppliers.ws.sap.client.types", "com.lebelier.suppliers.ws.sat.client.types");
		return marshaller;
	}
	
	@Bean
	public ZmxSuppliers_Client configZmxSuppliers_Client(Jaxb2Marshaller marshaller,
			SaajSoapMessageFactory messageFactory) {
		
		String sUsername = environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_USERNAME);
		String sPassword = environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_PASSWORD	);
		
		ZmxSuppliers_Client client = new ZmxSuppliers_Client();
		client.setMessageFactory(messageFactory);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		
		client.setMessageSender(new MessageSenderBasicAuthorization(sUsername, sPassword));
		
		return client;
	}

	@Bean
	public ConsultaCFDIServiceClient getConsultaCFDIServiceClient(Jaxb2Marshaller marshaller, SaajSoapMessageFactory messageFactory) {
		
		ConsultaCFDIServiceClient client = new ConsultaCFDIServiceClient();
		client.setMessageFactory(messageFactory);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		
		return client;
	}

    @Bean
    public WebServiceMessageSender webServiceMessageSender() {
      HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
      
      httpComponentsMessageSender.setConnectionTimeout(90 * 1000);
      httpComponentsMessageSender.setReadTimeout(90 * 1000);

      return httpComponentsMessageSender;
    }
}
