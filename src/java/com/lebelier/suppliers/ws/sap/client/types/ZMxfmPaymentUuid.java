//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bapiret2" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfBapiret2"/&gt;
 *         &lt;element name="PNosapProv" type="{urn:sap-com:document:sap:rfc:functions}char10" minOccurs="0"/&gt;
 *         &lt;element name="PRfcEmisor" type="{urn:sap-com:document:sap:rfc:functions}char16" minOccurs="0"/&gt;
 *         &lt;element name="PRfcReceptor" type="{urn:sap-com:document:sap:rfc:functions}char48" minOccurs="0"/&gt;
 *         &lt;element name="PTipoCfdi" type="{urn:sap-com:document:sap:rfc:functions}char1" minOccurs="0"/&gt;
 *         &lt;element name="PUuid" type="{urn:sap-com:document:sap:soap:functions:mc-style}char36" minOccurs="0"/&gt;
 *         &lt;element name="TUuid" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttFactUuid"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bapiret2",
    "pNosapProv",
    "pRfcEmisor",
    "pRfcReceptor",
    "pTipoCfdi",
    "pUuid",
    "tUuid"
})
@XmlRootElement(name = "ZMxfmPaymentUuid")
public class ZMxfmPaymentUuid {

    @XmlElement(name = "Bapiret2", required = true)
    protected TableOfBapiret2 bapiret2;
    @XmlElement(name = "PNosapProv")
    protected String pNosapProv;
    @XmlElement(name = "PRfcEmisor")
    protected String pRfcEmisor;
    @XmlElement(name = "PRfcReceptor")
    protected String pRfcReceptor;
    @XmlElement(name = "PTipoCfdi")
    protected String pTipoCfdi;
    @XmlElement(name = "PUuid")
    protected String pUuid;
    @XmlElement(name = "TUuid", required = true)
    protected ZttFactUuid tUuid;

    /**
     * Obtiene el valor de la propiedad bapiret2.
     * 
     * @return
     *     possible object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public TableOfBapiret2 getBapiret2() {
        return bapiret2;
    }

    /**
     * Define el valor de la propiedad bapiret2.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public void setBapiret2(TableOfBapiret2 value) {
        this.bapiret2 = value;
    }

    /**
     * Obtiene el valor de la propiedad pNosapProv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNosapProv() {
        return pNosapProv;
    }

    /**
     * Define el valor de la propiedad pNosapProv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNosapProv(String value) {
        this.pNosapProv = value;
    }

    /**
     * Obtiene el valor de la propiedad pRfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRfcEmisor() {
        return pRfcEmisor;
    }

    /**
     * Define el valor de la propiedad pRfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRfcEmisor(String value) {
        this.pRfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad pRfcReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRfcReceptor() {
        return pRfcReceptor;
    }

    /**
     * Define el valor de la propiedad pRfcReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRfcReceptor(String value) {
        this.pRfcReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad pTipoCfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTipoCfdi() {
        return pTipoCfdi;
    }

    /**
     * Define el valor de la propiedad pTipoCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTipoCfdi(String value) {
        this.pTipoCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pUuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPUuid() {
        return pUuid;
    }

    /**
     * Define el valor de la propiedad pUuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPUuid(String value) {
        this.pUuid = value;
    }

    /**
     * Obtiene el valor de la propiedad tUuid.
     * 
     * @return
     *     possible object is
     *     {@link ZttFactUuid }
     *     
     */
    public ZttFactUuid getTUuid() {
        return tUuid;
    }

    /**
     * Define el valor de la propiedad tUuid.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttFactUuid }
     *     
     */
    public void setTUuid(ZttFactUuid value) {
        this.tUuid = value;
    }

}
