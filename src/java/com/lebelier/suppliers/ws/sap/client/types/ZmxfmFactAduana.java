//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IProceso" type="{urn:sap-com:document:sap:rfc:functions}char1"/&gt;
 *         &lt;element name="PLifnr" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="PXmlfiles" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttXml"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "iProceso",
    "pLifnr",
    "pXmlfiles"
})
@XmlRootElement(name = "ZmxfmFactAduana")
public class ZmxfmFactAduana {

    @XmlElement(name = "IProceso", required = true)
    protected String iProceso;
    @XmlElement(name = "PLifnr", required = true)
    protected String pLifnr;
    @XmlElement(name = "PXmlfiles", required = true)
    protected ZttXml pXmlfiles;

    /**
     * Obtiene el valor de la propiedad iProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIProceso() {
        return iProceso;
    }

    /**
     * Define el valor de la propiedad iProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIProceso(String value) {
        this.iProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad pLifnr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLifnr() {
        return pLifnr;
    }

    /**
     * Define el valor de la propiedad pLifnr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLifnr(String value) {
        this.pLifnr = value;
    }

    /**
     * Obtiene el valor de la propiedad pXmlfiles.
     * 
     * @return
     *     possible object is
     *     {@link ZttXml }
     *     
     */
    public ZttXml getPXmlfiles() {
        return pXmlfiles;
    }

    /**
     * Define el valor de la propiedad pXmlfiles.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttXml }
     *     
     */
    public void setPXmlfiles(ZttXml value) {
        this.pXmlfiles = value;
    }

}
