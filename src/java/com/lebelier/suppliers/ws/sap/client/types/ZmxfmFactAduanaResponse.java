//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RBapiret2" type="{urn:sap-com:document:sap:soap:functions:mc-style}Bapiret2tab"/&gt;
 *         &lt;element name="RMessage" type="{urn:sap-com:document:sap:rfc:functions}string"/&gt;
 *         &lt;element name="RStatus" type="{urn:sap-com:document:sap:rfc:functions}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rBapiret2",
    "rMessage",
    "rStatus"
})
@XmlRootElement(name = "ZmxfmFactAduanaResponse")
public class ZmxfmFactAduanaResponse {

    @XmlElement(name = "RBapiret2", required = true)
    protected Bapiret2Tab rBapiret2;
    @XmlElement(name = "RMessage", required = true)
    protected String rMessage;
    @XmlElement(name = "RStatus", required = true)
    protected String rStatus;

    /**
     * Obtiene el valor de la propiedad rBapiret2.
     * 
     * @return
     *     possible object is
     *     {@link Bapiret2Tab }
     *     
     */
    public Bapiret2Tab getRBapiret2() {
        return rBapiret2;
    }

    /**
     * Define el valor de la propiedad rBapiret2.
     * 
     * @param value
     *     allowed object is
     *     {@link Bapiret2Tab }
     *     
     */
    public void setRBapiret2(Bapiret2Tab value) {
        this.rBapiret2 = value;
    }

    /**
     * Obtiene el valor de la propiedad rMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRMessage() {
        return rMessage;
    }

    /**
     * Define el valor de la propiedad rMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRMessage(String value) {
        this.rMessage = value;
    }

    /**
     * Obtiene el valor de la propiedad rStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRStatus() {
        return rStatus;
    }

    /**
     * Define el valor de la propiedad rStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRStatus(String value) {
        this.rStatus = value;
    }

}
