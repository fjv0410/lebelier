//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bapiret2" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfBapiret2"/&gt;
 *         &lt;element name="PExtranjero" type="{urn:sap-com:document:sap:rfc:functions}char1"/&gt;
 *         &lt;element name="PFechaCfdi" type="{urn:sap-com:document:sap:rfc:functions}date10"/&gt;
 *         &lt;element name="PFolioCfdi" type="{urn:sap-com:document:sap:rfc:functions}char20"/&gt;
 *         &lt;element name="PFormapago" type="{urn:sap-com:document:sap:rfc:functions}char2"/&gt;
 *         &lt;element name="PImporte" type="{urn:sap-com:document:sap:rfc:functions}curr13.2"/&gt;
 *         &lt;element name="PMetodopago" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *         &lt;element name="PMonedaCfdi" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *         &lt;element name="PNosapProv" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="PRfcEmisor" type="{urn:sap-com:document:sap:rfc:functions}char16" minOccurs="0"/&gt;
 *         &lt;element name="PRfcReceptor" type="{urn:sap-com:document:sap:rfc:functions}char48"/&gt;
 *         &lt;element name="PSerie" type="{urn:sap-com:document:sap:rfc:functions}string"/&gt;
 *         &lt;element name="PTasaOCuota" type="{urn:sap-com:document:sap:rfc:functions}string"/&gt;
 *         &lt;element name="PTipoCambio" type="{urn:sap-com:document:sap:soap:functions:mc-style}decimal9.5" minOccurs="0"/&gt;
 *         &lt;element name="PTipoCfdi" type="{urn:sap-com:document:sap:rfc:functions}char1"/&gt;
 *         &lt;element name="PTotalImpRet" type="{urn:sap-com:document:sap:rfc:functions}curr13.2"/&gt;
 *         &lt;element name="PUsocfdi" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *         &lt;element name="PUuid" type="{urn:sap-com:document:sap:rfc:functions}char36"/&gt;
 *         &lt;element name="PtConceptosCfdi" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttCfdiConcepto"/&gt;
 *         &lt;element name="PtEntregas" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttDeliveryNumb"/&gt;
 *         &lt;element name="TRetenciones" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttRetenciones"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bapiret2",
    "pExtranjero",
    "pFechaCfdi",
    "pFolioCfdi",
    "pFormapago",
    "pImporte",
    "pMetodopago",
    "pMonedaCfdi",
    "pNosapProv",
    "pRfcEmisor",
    "pRfcReceptor",
    "pSerie",
    "pTasaOCuota",
    "pTipoCambio",
    "pTipoCfdi",
    "pTotalImpRet",
    "pUsocfdi",
    "pUuid",
    "ptConceptosCfdi",
    "ptEntregas",
    "tRetenciones"
})
@XmlRootElement(name = "ZMxfmValidateDeliveries")
public class ZMxfmValidateDeliveries {

    @XmlElement(name = "Bapiret2", required = true)
    protected TableOfBapiret2 bapiret2;
    @XmlElement(name = "PExtranjero", required = true)
    protected String pExtranjero;
    @XmlElement(name = "PFechaCfdi", required = true)
    protected String pFechaCfdi;
    @XmlElement(name = "PFolioCfdi", required = true)
    protected String pFolioCfdi;
    @XmlElement(name = "PFormapago", required = true)
    protected String pFormapago;
    @XmlElement(name = "PImporte", required = true)
    protected BigDecimal pImporte;
    @XmlElement(name = "PMetodopago", required = true)
    protected String pMetodopago;
    @XmlElement(name = "PMonedaCfdi", required = true)
    protected String pMonedaCfdi;
    @XmlElement(name = "PNosapProv", required = true)
    protected String pNosapProv;
    @XmlElement(name = "PRfcEmisor")
    protected String pRfcEmisor;
    @XmlElement(name = "PRfcReceptor", required = true)
    protected String pRfcReceptor;
    @XmlElement(name = "PSerie", required = true)
    protected String pSerie;
    @XmlElement(name = "PTasaOCuota", required = true)
    protected String pTasaOCuota;
    @XmlElement(name = "PTipoCambio")
    protected BigDecimal pTipoCambio;
    @XmlElement(name = "PTipoCfdi", required = true)
    protected String pTipoCfdi;
    @XmlElement(name = "PTotalImpRet", required = true)
    protected BigDecimal pTotalImpRet;
    @XmlElement(name = "PUsocfdi", required = true)
    protected String pUsocfdi;
    @XmlElement(name = "PUuid", required = true)
    protected String pUuid;
    @XmlElement(name = "PtConceptosCfdi", required = true)
    protected ZttCfdiConcepto ptConceptosCfdi;
    @XmlElement(name = "PtEntregas", required = true)
    protected ZttDeliveryNumb ptEntregas;
    @XmlElement(name = "TRetenciones", required = true)
    protected ZttRetenciones tRetenciones;

    /**
     * Obtiene el valor de la propiedad bapiret2.
     * 
     * @return
     *     possible object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public TableOfBapiret2 getBapiret2() {
        return bapiret2;
    }

    /**
     * Define el valor de la propiedad bapiret2.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public void setBapiret2(TableOfBapiret2 value) {
        this.bapiret2 = value;
    }

    /**
     * Obtiene el valor de la propiedad pExtranjero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPExtranjero() {
        return pExtranjero;
    }

    /**
     * Define el valor de la propiedad pExtranjero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPExtranjero(String value) {
        this.pExtranjero = value;
    }

    /**
     * Obtiene el valor de la propiedad pFechaCfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFechaCfdi() {
        return pFechaCfdi;
    }

    /**
     * Define el valor de la propiedad pFechaCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFechaCfdi(String value) {
        this.pFechaCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pFolioCfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFolioCfdi() {
        return pFolioCfdi;
    }

    /**
     * Define el valor de la propiedad pFolioCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFolioCfdi(String value) {
        this.pFolioCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pFormapago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFormapago() {
        return pFormapago;
    }

    /**
     * Define el valor de la propiedad pFormapago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFormapago(String value) {
        this.pFormapago = value;
    }

    /**
     * Obtiene el valor de la propiedad pImporte.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPImporte() {
        return pImporte;
    }

    /**
     * Define el valor de la propiedad pImporte.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPImporte(BigDecimal value) {
        this.pImporte = value;
    }

    /**
     * Obtiene el valor de la propiedad pMetodopago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMetodopago() {
        return pMetodopago;
    }

    /**
     * Define el valor de la propiedad pMetodopago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMetodopago(String value) {
        this.pMetodopago = value;
    }

    /**
     * Obtiene el valor de la propiedad pMonedaCfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMonedaCfdi() {
        return pMonedaCfdi;
    }

    /**
     * Define el valor de la propiedad pMonedaCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMonedaCfdi(String value) {
        this.pMonedaCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pNosapProv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNosapProv() {
        return pNosapProv;
    }

    /**
     * Define el valor de la propiedad pNosapProv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNosapProv(String value) {
        this.pNosapProv = value;
    }

    /**
     * Obtiene el valor de la propiedad pRfcEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRfcEmisor() {
        return pRfcEmisor;
    }

    /**
     * Define el valor de la propiedad pRfcEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRfcEmisor(String value) {
        this.pRfcEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad pRfcReceptor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRfcReceptor() {
        return pRfcReceptor;
    }

    /**
     * Define el valor de la propiedad pRfcReceptor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRfcReceptor(String value) {
        this.pRfcReceptor = value;
    }

    /**
     * Obtiene el valor de la propiedad pSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSerie() {
        return pSerie;
    }

    /**
     * Define el valor de la propiedad pSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSerie(String value) {
        this.pSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad pTasaOCuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTasaOCuota() {
        return pTasaOCuota;
    }

    /**
     * Define el valor de la propiedad pTasaOCuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTasaOCuota(String value) {
        this.pTasaOCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad pTipoCambio.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPTipoCambio() {
        return pTipoCambio;
    }

    /**
     * Define el valor de la propiedad pTipoCambio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPTipoCambio(BigDecimal value) {
        this.pTipoCambio = value;
    }

    /**
     * Obtiene el valor de la propiedad pTipoCfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTipoCfdi() {
        return pTipoCfdi;
    }

    /**
     * Define el valor de la propiedad pTipoCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTipoCfdi(String value) {
        this.pTipoCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pTotalImpRet.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPTotalImpRet() {
        return pTotalImpRet;
    }

    /**
     * Define el valor de la propiedad pTotalImpRet.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPTotalImpRet(BigDecimal value) {
        this.pTotalImpRet = value;
    }

    /**
     * Obtiene el valor de la propiedad pUsocfdi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPUsocfdi() {
        return pUsocfdi;
    }

    /**
     * Define el valor de la propiedad pUsocfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPUsocfdi(String value) {
        this.pUsocfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad pUuid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPUuid() {
        return pUuid;
    }

    /**
     * Define el valor de la propiedad pUuid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPUuid(String value) {
        this.pUuid = value;
    }

    /**
     * Obtiene el valor de la propiedad ptConceptosCfdi.
     * 
     * @return
     *     possible object is
     *     {@link ZttCfdiConcepto }
     *     
     */
    public ZttCfdiConcepto getPtConceptosCfdi() {
        return ptConceptosCfdi;
    }

    /**
     * Define el valor de la propiedad ptConceptosCfdi.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttCfdiConcepto }
     *     
     */
    public void setPtConceptosCfdi(ZttCfdiConcepto value) {
        this.ptConceptosCfdi = value;
    }

    /**
     * Obtiene el valor de la propiedad ptEntregas.
     * 
     * @return
     *     possible object is
     *     {@link ZttDeliveryNumb }
     *     
     */
    public ZttDeliveryNumb getPtEntregas() {
        return ptEntregas;
    }

    /**
     * Define el valor de la propiedad ptEntregas.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttDeliveryNumb }
     *     
     */
    public void setPtEntregas(ZttDeliveryNumb value) {
        this.ptEntregas = value;
    }

    /**
     * Obtiene el valor de la propiedad tRetenciones.
     * 
     * @return
     *     possible object is
     *     {@link ZttRetenciones }
     *     
     */
    public ZttRetenciones getTRetenciones() {
        return tRetenciones;
    }

    /**
     * Define el valor de la propiedad tRetenciones.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttRetenciones }
     *     
     */
    public void setTRetenciones(ZttRetenciones value) {
        this.tRetenciones = value;
    }

}
