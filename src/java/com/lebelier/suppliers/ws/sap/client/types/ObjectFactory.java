//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.lebelier.suppliers.ws.sap.client.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.lebelier.suppliers.ws.sap.client.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZmxfmFactAduana }
     * 
     */
    public ZmxfmFactAduana createZmxfmFactAduana() {
        return new ZmxfmFactAduana();
    }

    /**
     * Create an instance of {@link ZttXml }
     * 
     */
    public ZttXml createZttXml() {
        return new ZttXml();
    }

    /**
     * Create an instance of {@link ZmxfmFactAduanaResponse }
     * 
     */
    public ZmxfmFactAduanaResponse createZmxfmFactAduanaResponse() {
        return new ZmxfmFactAduanaResponse();
    }

    /**
     * Create an instance of {@link Bapiret2Tab }
     * 
     */
    public Bapiret2Tab createBapiret2Tab() {
        return new Bapiret2Tab();
    }

    /**
     * Create an instance of {@link ZmxfmPreliminaryFv60Cxp }
     * 
     */
    public ZmxfmPreliminaryFv60Cxp createZmxfmPreliminaryFv60Cxp() {
        return new ZmxfmPreliminaryFv60Cxp();
    }

    /**
     * Create an instance of {@link TableOfBapiret2 }
     * 
     */
    public TableOfBapiret2 createTableOfBapiret2() {
        return new TableOfBapiret2();
    }

    /**
     * Create an instance of {@link ZttRetenciones }
     * 
     */
    public ZttRetenciones createZttRetenciones() {
        return new ZttRetenciones();
    }

    /**
     * Create an instance of {@link ZmxfmPreliminaryFv60CxpResponse }
     * 
     */
    public ZmxfmPreliminaryFv60CxpResponse createZmxfmPreliminaryFv60CxpResponse() {
        return new ZmxfmPreliminaryFv60CxpResponse();
    }

    /**
     * Create an instance of {@link ZmxfmPreliminaryPostingFb01 }
     * 
     */
    public ZmxfmPreliminaryPostingFb01 createZmxfmPreliminaryPostingFb01() {
        return new ZmxfmPreliminaryPostingFb01();
    }

    /**
     * Create an instance of {@link ZmxfmPreliminaryPostingFb01Response }
     * 
     */
    public ZmxfmPreliminaryPostingFb01Response createZmxfmPreliminaryPostingFb01Response() {
        return new ZmxfmPreliminaryPostingFb01Response();
    }

    /**
     * Create an instance of {@link ZmxfmPrelmaryFv60Ext }
     * 
     */
    public ZmxfmPrelmaryFv60Ext createZmxfmPrelmaryFv60Ext() {
        return new ZmxfmPrelmaryFv60Ext();
    }

    /**
     * Create an instance of {@link ZmxfmPrelmaryFv60ExtResponse }
     * 
     */
    public ZmxfmPrelmaryFv60ExtResponse createZmxfmPrelmaryFv60ExtResponse() {
        return new ZmxfmPrelmaryFv60ExtResponse();
    }

    /**
     * Create an instance of {@link ZMxfmDeliveriesCxp }
     * 
     */
    public ZMxfmDeliveriesCxp createZMxfmDeliveriesCxp() {
        return new ZMxfmDeliveriesCxp();
    }

    /**
     * Create an instance of {@link ZttCfdiConcepto }
     * 
     */
    public ZttCfdiConcepto createZttCfdiConcepto() {
        return new ZttCfdiConcepto();
    }

    /**
     * Create an instance of {@link ZttDeliveryNumb }
     * 
     */
    public ZttDeliveryNumb createZttDeliveryNumb() {
        return new ZttDeliveryNumb();
    }

    /**
     * Create an instance of {@link ZMxfmDeliveriesCxpResponse }
     * 
     */
    public ZMxfmDeliveriesCxpResponse createZMxfmDeliveriesCxpResponse() {
        return new ZMxfmDeliveriesCxpResponse();
    }

    /**
     * Create an instance of {@link ZMxfmDeliveriesExt }
     * 
     */
    public ZMxfmDeliveriesExt createZMxfmDeliveriesExt() {
        return new ZMxfmDeliveriesExt();
    }

    /**
     * Create an instance of {@link ZMxfmDeliveriesExtResponse }
     * 
     */
    public ZMxfmDeliveriesExtResponse createZMxfmDeliveriesExtResponse() {
        return new ZMxfmDeliveriesExtResponse();
    }

    /**
     * Create an instance of {@link ZMxfmFacturasSinCp }
     * 
     */
    public ZMxfmFacturasSinCp createZMxfmFacturasSinCp() {
        return new ZMxfmFacturasSinCp();
    }

    /**
     * Create an instance of {@link ZttFactsincomp }
     * 
     */
    public ZttFactsincomp createZttFactsincomp() {
        return new ZttFactsincomp();
    }

    /**
     * Create an instance of {@link ZMxfmFacturasSinCpResponse }
     * 
     */
    public ZMxfmFacturasSinCpResponse createZMxfmFacturasSinCpResponse() {
        return new ZMxfmFacturasSinCpResponse();
    }

    /**
     * Create an instance of {@link ZMxfmInvoiceEstatus }
     * 
     */
    public ZMxfmInvoiceEstatus createZMxfmInvoiceEstatus() {
        return new ZMxfmInvoiceEstatus();
    }

    /**
     * Create an instance of {@link ZttInoviceStatus }
     * 
     */
    public ZttInoviceStatus createZttInoviceStatus() {
        return new ZttInoviceStatus();
    }

    /**
     * Create an instance of {@link ZMxfmInvoiceEstatusResponse }
     * 
     */
    public ZMxfmInvoiceEstatusResponse createZMxfmInvoiceEstatusResponse() {
        return new ZMxfmInvoiceEstatusResponse();
    }

    /**
     * Create an instance of {@link ZMxfmPaymentComplment }
     * 
     */
    public ZMxfmPaymentComplment createZMxfmPaymentComplment() {
        return new ZMxfmPaymentComplment();
    }

    /**
     * Create an instance of {@link ZttPaymentComp }
     * 
     */
    public ZttPaymentComp createZttPaymentComp() {
        return new ZttPaymentComp();
    }

    /**
     * Create an instance of {@link ZMxfmPaymentComplmentResponse }
     * 
     */
    public ZMxfmPaymentComplmentResponse createZMxfmPaymentComplmentResponse() {
        return new ZMxfmPaymentComplmentResponse();
    }

    /**
     * Create an instance of {@link ZMxfmPaymentUuid }
     * 
     */
    public ZMxfmPaymentUuid createZMxfmPaymentUuid() {
        return new ZMxfmPaymentUuid();
    }

    /**
     * Create an instance of {@link ZttFactUuid }
     * 
     */
    public ZttFactUuid createZttFactUuid() {
        return new ZttFactUuid();
    }

    /**
     * Create an instance of {@link ZMxfmPaymentUuidResponse }
     * 
     */
    public ZMxfmPaymentUuidResponse createZMxfmPaymentUuidResponse() {
        return new ZMxfmPaymentUuidResponse();
    }

    /**
     * Create an instance of {@link ZMxfmUpdateSupplier }
     * 
     */
    public ZMxfmUpdateSupplier createZMxfmUpdateSupplier() {
        return new ZMxfmUpdateSupplier();
    }

    /**
     * Create an instance of {@link ZttRproveedor }
     * 
     */
    public ZttRproveedor createZttRproveedor() {
        return new ZttRproveedor();
    }

    /**
     * Create an instance of {@link ZMxfmUpdateSupplierResponse }
     * 
     */
    public ZMxfmUpdateSupplierResponse createZMxfmUpdateSupplierResponse() {
        return new ZMxfmUpdateSupplierResponse();
    }

    /**
     * Create an instance of {@link ZMxfmValidateDeliveries }
     * 
     */
    public ZMxfmValidateDeliveries createZMxfmValidateDeliveries() {
        return new ZMxfmValidateDeliveries();
    }

    /**
     * Create an instance of {@link ZMxfmValidateDeliveriesResponse }
     * 
     */
    public ZMxfmValidateDeliveriesResponse createZMxfmValidateDeliveriesResponse() {
        return new ZMxfmValidateDeliveriesResponse();
    }

    /**
     * Create an instance of {@link ZstXml }
     * 
     */
    public ZstXml createZstXml() {
        return new ZstXml();
    }

    /**
     * Create an instance of {@link Bapiret2 }
     * 
     */
    public Bapiret2 createBapiret2() {
        return new Bapiret2();
    }

    /**
     * Create an instance of {@link ZtyRetenciones }
     * 
     */
    public ZtyRetenciones createZtyRetenciones() {
        return new ZtyRetenciones();
    }

    /**
     * Create an instance of {@link ZtyCfdiConcepto }
     * 
     */
    public ZtyCfdiConcepto createZtyCfdiConcepto() {
        return new ZtyCfdiConcepto();
    }

    /**
     * Create an instance of {@link ZtyDeliveryNumb }
     * 
     */
    public ZtyDeliveryNumb createZtyDeliveryNumb() {
        return new ZtyDeliveryNumb();
    }

    /**
     * Create an instance of {@link ZtyFactsincomp }
     * 
     */
    public ZtyFactsincomp createZtyFactsincomp() {
        return new ZtyFactsincomp();
    }

    /**
     * Create an instance of {@link ZtyInoviceStatus }
     * 
     */
    public ZtyInoviceStatus createZtyInoviceStatus() {
        return new ZtyInoviceStatus();
    }

    /**
     * Create an instance of {@link ZtyPaymentComp }
     * 
     */
    public ZtyPaymentComp createZtyPaymentComp() {
        return new ZtyPaymentComp();
    }

    /**
     * Create an instance of {@link ZtyFactUuid }
     * 
     */
    public ZtyFactUuid createZtyFactUuid() {
        return new ZtyFactUuid();
    }

    /**
     * Create an instance of {@link ZtyProveedor }
     * 
     */
    public ZtyProveedor createZtyProveedor() {
        return new ZtyProveedor();
    }

}
