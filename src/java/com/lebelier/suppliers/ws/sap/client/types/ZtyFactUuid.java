//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ZtyFactUuid complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ZtyFactUuid"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UuidFact" type="{urn:sap-com:document:sap:rfc:functions}char36"/&gt;
 *         &lt;element name="Belnr" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="Bukrs" type="{urn:sap-com:document:sap:rfc:functions}char4"/&gt;
 *         &lt;element name="Gjahr" type="{urn:sap-com:document:sap:rfc:functions}numeric4"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZtyFactUuid", propOrder = {
    "uuidFact",
    "belnr",
    "bukrs",
    "gjahr"
})
public class ZtyFactUuid {

    @XmlElement(name = "UuidFact", required = true)
    protected String uuidFact;
    @XmlElement(name = "Belnr", required = true)
    protected String belnr;
    @XmlElement(name = "Bukrs", required = true)
    protected String bukrs;
    @XmlElement(name = "Gjahr", required = true)
    protected String gjahr;

    /**
     * Obtiene el valor de la propiedad uuidFact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuidFact() {
        return uuidFact;
    }

    /**
     * Define el valor de la propiedad uuidFact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuidFact(String value) {
        this.uuidFact = value;
    }

    /**
     * Obtiene el valor de la propiedad belnr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBelnr() {
        return belnr;
    }

    /**
     * Define el valor de la propiedad belnr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBelnr(String value) {
        this.belnr = value;
    }

    /**
     * Obtiene el valor de la propiedad bukrs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBukrs() {
        return bukrs;
    }

    /**
     * Define el valor de la propiedad bukrs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBukrs(String value) {
        this.bukrs = value;
    }

    /**
     * Obtiene el valor de la propiedad gjahr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGjahr() {
        return gjahr;
    }

    /**
     * Define el valor de la propiedad gjahr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGjahr(String value) {
        this.gjahr = value;
    }

}
