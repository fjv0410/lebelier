//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ZtyRetenciones complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ZtyRetenciones"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Base" type="{urn:sap-com:document:sap:soap:functions:mc-style}curr15.2"/&gt;
 *         &lt;element name="Impuesto" type="{urn:sap-com:document:sap:rfc:functions}char3"/&gt;
 *         &lt;element name="Tipofactor" type="{urn:sap-com:document:sap:soap:functions:mc-style}char10"/&gt;
 *         &lt;element name="Tasaocuota" type="{urn:sap-com:document:sap:soap:functions:mc-style}char10"/&gt;
 *         &lt;element name="Importe" type="{urn:sap-com:document:sap:soap:functions:mc-style}curr15.2"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZtyRetenciones", propOrder = {
    "base",
    "impuesto",
    "tipofactor",
    "tasaocuota",
    "importe"
})
public class ZtyRetenciones {

    @XmlElement(name = "Base", required = true)
    protected BigDecimal base;
    @XmlElement(name = "Impuesto", required = true)
    protected String impuesto;
    @XmlElement(name = "Tipofactor", required = true)
    protected String tipofactor;
    @XmlElement(name = "Tasaocuota", required = true)
    protected String tasaocuota;
    @XmlElement(name = "Importe", required = true)
    protected BigDecimal importe;

    /**
     * Obtiene el valor de la propiedad base.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBase() {
        return base;
    }

    /**
     * Define el valor de la propiedad base.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBase(BigDecimal value) {
        this.base = value;
    }

    /**
     * Obtiene el valor de la propiedad impuesto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImpuesto() {
        return impuesto;
    }

    /**
     * Define el valor de la propiedad impuesto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImpuesto(String value) {
        this.impuesto = value;
    }

    /**
     * Obtiene el valor de la propiedad tipofactor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipofactor() {
        return tipofactor;
    }

    /**
     * Define el valor de la propiedad tipofactor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipofactor(String value) {
        this.tipofactor = value;
    }

    /**
     * Obtiene el valor de la propiedad tasaocuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTasaocuota() {
        return tasaocuota;
    }

    /**
     * Define el valor de la propiedad tasaocuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTasaocuota(String value) {
        this.tasaocuota = value;
    }

    /**
     * Obtiene el valor de la propiedad importe.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getImporte() {
        return importe;
    }

    /**
     * Define el valor de la propiedad importe.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setImporte(BigDecimal value) {
        this.importe = value;
    }

}
