//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PProveedor" type="{urn:sap-com:document:sap:rfc:functions}char10" minOccurs="0"/&gt;
 *         &lt;element name="TtPaymentComp" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttPaymentComp"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pProveedor",
    "ttPaymentComp"
})
@XmlRootElement(name = "ZMxfmPaymentComplment")
public class ZMxfmPaymentComplment {

    @XmlElement(name = "PProveedor")
    protected String pProveedor;
    @XmlElement(name = "TtPaymentComp", required = true)
    protected ZttPaymentComp ttPaymentComp;

    /**
     * Obtiene el valor de la propiedad pProveedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPProveedor() {
        return pProveedor;
    }

    /**
     * Define el valor de la propiedad pProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPProveedor(String value) {
        this.pProveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad ttPaymentComp.
     * 
     * @return
     *     possible object is
     *     {@link ZttPaymentComp }
     *     
     */
    public ZttPaymentComp getTtPaymentComp() {
        return ttPaymentComp;
    }

    /**
     * Define el valor de la propiedad ttPaymentComp.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttPaymentComp }
     *     
     */
    public void setTtPaymentComp(ZttPaymentComp value) {
        this.ttPaymentComp = value;
    }

}
