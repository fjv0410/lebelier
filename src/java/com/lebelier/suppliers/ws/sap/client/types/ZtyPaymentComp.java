//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ZtyPaymentComp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ZtyPaymentComp"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Belnr" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="Xblnr" type="{urn:sap-com:document:sap:soap:functions:mc-style}char16"/&gt;
 *         &lt;element name="Wrbtr" type="{urn:sap-com:document:sap:rfc:functions}curr13.2"/&gt;
 *         &lt;element name="Augbl" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="Gjahr" type="{urn:sap-com:document:sap:rfc:functions}numeric4"/&gt;
 *         &lt;element name="Bukrs" type="{urn:sap-com:document:sap:rfc:functions}char4"/&gt;
 *         &lt;element name="Augdt" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZtyPaymentComp", propOrder = {
    "belnr",
    "xblnr",
    "wrbtr",
    "augbl",
    "gjahr",
    "bukrs",
    "augdt"
})
public class ZtyPaymentComp {

    @XmlElement(name = "Belnr", required = true)
    protected String belnr;
    @XmlElement(name = "Xblnr", required = true)
    protected String xblnr;
    @XmlElement(name = "Wrbtr", required = true)
    protected BigDecimal wrbtr;
    @XmlElement(name = "Augbl", required = true)
    protected String augbl;
    @XmlElement(name = "Gjahr", required = true)
    protected String gjahr;
    @XmlElement(name = "Bukrs", required = true)
    protected String bukrs;
    @XmlElement(name = "Augdt", required = true)
    protected String augdt;

    /**
     * Obtiene el valor de la propiedad belnr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBelnr() {
        return belnr;
    }

    /**
     * Define el valor de la propiedad belnr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBelnr(String value) {
        this.belnr = value;
    }

    /**
     * Obtiene el valor de la propiedad xblnr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXblnr() {
        return xblnr;
    }

    /**
     * Define el valor de la propiedad xblnr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXblnr(String value) {
        this.xblnr = value;
    }

    /**
     * Obtiene el valor de la propiedad wrbtr.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getWrbtr() {
        return wrbtr;
    }

    /**
     * Define el valor de la propiedad wrbtr.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setWrbtr(BigDecimal value) {
        this.wrbtr = value;
    }

    /**
     * Obtiene el valor de la propiedad augbl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAugbl() {
        return augbl;
    }

    /**
     * Define el valor de la propiedad augbl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAugbl(String value) {
        this.augbl = value;
    }

    /**
     * Obtiene el valor de la propiedad gjahr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGjahr() {
        return gjahr;
    }

    /**
     * Define el valor de la propiedad gjahr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGjahr(String value) {
        this.gjahr = value;
    }

    /**
     * Obtiene el valor de la propiedad bukrs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBukrs() {
        return bukrs;
    }

    /**
     * Define el valor de la propiedad bukrs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBukrs(String value) {
        this.bukrs = value;
    }

    /**
     * Obtiene el valor de la propiedad augdt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAugdt() {
        return augdt;
    }

    /**
     * Define el valor de la propiedad augdt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAugdt(String value) {
        this.augdt = value;
    }

}
