//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItFacturas" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZttInoviceStatus"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "itFacturas"
})
@XmlRootElement(name = "ZMxfmInvoiceEstatusResponse")
public class ZMxfmInvoiceEstatusResponse {

    @XmlElement(name = "ItFacturas", required = true)
    protected ZttInoviceStatus itFacturas;

    /**
     * Obtiene el valor de la propiedad itFacturas.
     * 
     * @return
     *     possible object is
     *     {@link ZttInoviceStatus }
     *     
     */
    public ZttInoviceStatus getItFacturas() {
        return itFacturas;
    }

    /**
     * Define el valor de la propiedad itFacturas.
     * 
     * @param value
     *     allowed object is
     *     {@link ZttInoviceStatus }
     *     
     */
    public void setItFacturas(ZttInoviceStatus value) {
        this.itFacturas = value;
    }

}
