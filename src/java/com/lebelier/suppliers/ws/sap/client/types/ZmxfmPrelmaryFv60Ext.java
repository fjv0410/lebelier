//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bapiret2" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfBapiret2"/&gt;
 *         &lt;element name="PConcepto" type="{urn:sap-com:document:sap:rfc:functions}string" minOccurs="0"/&gt;
 *         &lt;element name="PFechaFactura" type="{urn:sap-com:document:sap:rfc:functions}date10" minOccurs="0"/&gt;
 *         &lt;element name="PImporte" type="{urn:sap-com:document:sap:rfc:functions}curr13.2" minOccurs="0"/&gt;
 *         &lt;element name="PIsFactONc" type="{urn:sap-com:document:sap:rfc:functions}char1" minOccurs="0"/&gt;
 *         &lt;element name="PMoneda" type="{urn:sap-com:document:sap:rfc:functions}char3" minOccurs="0"/&gt;
 *         &lt;element name="PNoFactONc" type="{urn:sap-com:document:sap:rfc:functions}char10" minOccurs="0"/&gt;
 *         &lt;element name="PNoSapProv" type="{urn:sap-com:document:sap:rfc:functions}char10" minOccurs="0"/&gt;
 *         &lt;element name="PSociedad" type="{urn:sap-com:document:sap:rfc:functions}char4" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bapiret2",
    "pConcepto",
    "pFechaFactura",
    "pImporte",
    "pIsFactONc",
    "pMoneda",
    "pNoFactONc",
    "pNoSapProv",
    "pSociedad"
})
@XmlRootElement(name = "ZmxfmPrelmaryFv60Ext")
public class ZmxfmPrelmaryFv60Ext {

    @XmlElement(name = "Bapiret2", required = true)
    protected TableOfBapiret2 bapiret2;
    @XmlElement(name = "PConcepto")
    protected String pConcepto;
    @XmlElement(name = "PFechaFactura")
    protected String pFechaFactura;
    @XmlElement(name = "PImporte")
    protected BigDecimal pImporte;
    @XmlElement(name = "PIsFactONc")
    protected String pIsFactONc;
    @XmlElement(name = "PMoneda")
    protected String pMoneda;
    @XmlElement(name = "PNoFactONc")
    protected String pNoFactONc;
    @XmlElement(name = "PNoSapProv")
    protected String pNoSapProv;
    @XmlElement(name = "PSociedad")
    protected String pSociedad;

    /**
     * Obtiene el valor de la propiedad bapiret2.
     * 
     * @return
     *     possible object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public TableOfBapiret2 getBapiret2() {
        return bapiret2;
    }

    /**
     * Define el valor de la propiedad bapiret2.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public void setBapiret2(TableOfBapiret2 value) {
        this.bapiret2 = value;
    }

    /**
     * Obtiene el valor de la propiedad pConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPConcepto() {
        return pConcepto;
    }

    /**
     * Define el valor de la propiedad pConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPConcepto(String value) {
        this.pConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad pFechaFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFechaFactura() {
        return pFechaFactura;
    }

    /**
     * Define el valor de la propiedad pFechaFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFechaFactura(String value) {
        this.pFechaFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad pImporte.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPImporte() {
        return pImporte;
    }

    /**
     * Define el valor de la propiedad pImporte.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPImporte(BigDecimal value) {
        this.pImporte = value;
    }

    /**
     * Obtiene el valor de la propiedad pIsFactONc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIsFactONc() {
        return pIsFactONc;
    }

    /**
     * Define el valor de la propiedad pIsFactONc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIsFactONc(String value) {
        this.pIsFactONc = value;
    }

    /**
     * Obtiene el valor de la propiedad pMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMoneda() {
        return pMoneda;
    }

    /**
     * Define el valor de la propiedad pMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMoneda(String value) {
        this.pMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad pNoFactONc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNoFactONc() {
        return pNoFactONc;
    }

    /**
     * Define el valor de la propiedad pNoFactONc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNoFactONc(String value) {
        this.pNoFactONc = value;
    }

    /**
     * Obtiene el valor de la propiedad pNoSapProv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNoSapProv() {
        return pNoSapProv;
    }

    /**
     * Define el valor de la propiedad pNoSapProv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNoSapProv(String value) {
        this.pNoSapProv = value;
    }

    /**
     * Obtiene el valor de la propiedad pSociedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSociedad() {
        return pSociedad;
    }

    /**
     * Define el valor de la propiedad pSociedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSociedad(String value) {
        this.pSociedad = value;
    }

}
