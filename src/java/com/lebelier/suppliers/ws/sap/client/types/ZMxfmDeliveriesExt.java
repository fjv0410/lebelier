//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.10.25 a las 11:50:41 AM CDT 
//


package com.lebelier.suppliers.ws.sap.client.types;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Bapiret2" type="{urn:sap-com:document:sap:soap:functions:mc-style}TableOfBapiret2"/&gt;
 *         &lt;element name="PConcepto" type="{urn:sap-com:document:sap:rfc:functions}string"/&gt;
 *         &lt;element name="PEntrega" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="PFacturaNota" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="PFechaEntrega" type="{urn:sap-com:document:sap:rfc:functions}date10"/&gt;
 *         &lt;element name="PImporte" type="{urn:sap-com:document:sap:rfc:functions}curr13.2"/&gt;
 *         &lt;element name="PNosapProv" type="{urn:sap-com:document:sap:rfc:functions}char10"/&gt;
 *         &lt;element name="POpcionFN" type="{urn:sap-com:document:sap:rfc:functions}char1" minOccurs="0"/&gt;
 *         &lt;element name="PSociedad" type="{urn:sap-com:document:sap:rfc:functions}char4"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bapiret2",
    "pConcepto",
    "pEntrega",
    "pFacturaNota",
    "pFechaEntrega",
    "pImporte",
    "pNosapProv",
    "pOpcionFN",
    "pSociedad", 
    "pMoneda"
})
@XmlRootElement(name = "ZMxfmDeliveriesExt")
public class ZMxfmDeliveriesExt {

    @XmlElement(name = "Bapiret2", required = true)
    protected TableOfBapiret2 bapiret2;
    @XmlElement(name = "PConcepto", required = true)
    protected String pConcepto;
    @XmlElement(name = "PEntrega", required = true)
    protected String pEntrega;
    @XmlElement(name = "PFacturaNota", required = true)
    protected String pFacturaNota;
    @XmlElement(name = "PFechaEntrega", required = true)
    protected String pFechaEntrega;
    @XmlElement(name = "PImporte", required = true)
    protected BigDecimal pImporte;
    @XmlElement(name = "PNosapProv", required = true)
    protected String pNosapProv;
    @XmlElement(name = "POpcionFN")
    protected String pOpcionFN;
    @XmlElement(name = "PSociedad", required = true)
    protected String pSociedad;
    @XmlElement(name = "PMoneda", required = true)
    protected String pMoneda;

    /**
     * Obtiene el valor de la propiedad bapiret2.
     * 
     * @return
     *     possible object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public TableOfBapiret2 getBapiret2() {
        return bapiret2;
    }

    /**
     * Define el valor de la propiedad bapiret2.
     * 
     * @param value
     *     allowed object is
     *     {@link TableOfBapiret2 }
     *     
     */
    public void setBapiret2(TableOfBapiret2 value) {
        this.bapiret2 = value;
    }

    /**
     * Obtiene el valor de la propiedad pConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPConcepto() {
        return pConcepto;
    }

    /**
     * Define el valor de la propiedad pConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPConcepto(String value) {
        this.pConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad pEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPEntrega() {
        return pEntrega;
    }

    /**
     * Define el valor de la propiedad pEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPEntrega(String value) {
        this.pEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad pFacturaNota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFacturaNota() {
        return pFacturaNota;
    }

    /**
     * Define el valor de la propiedad pFacturaNota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFacturaNota(String value) {
        this.pFacturaNota = value;
    }

    /**
     * Obtiene el valor de la propiedad pFechaEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPFechaEntrega() {
        return pFechaEntrega;
    }

    /**
     * Define el valor de la propiedad pFechaEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPFechaEntrega(String value) {
        this.pFechaEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad pImporte.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPImporte() {
        return pImporte;
    }

    /**
     * Define el valor de la propiedad pImporte.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPImporte(BigDecimal value) {
        this.pImporte = value;
    }

    /**
     * Obtiene el valor de la propiedad pNosapProv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPNosapProv() {
        return pNosapProv;
    }

    /**
     * Define el valor de la propiedad pNosapProv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPNosapProv(String value) {
        this.pNosapProv = value;
    }

    /**
     * Obtiene el valor de la propiedad pOpcionFN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOpcionFN() {
        return pOpcionFN;
    }

    /**
     * Define el valor de la propiedad pOpcionFN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOpcionFN(String value) {
        this.pOpcionFN = value;
    }

    /**
     * Obtiene el valor de la propiedad pSociedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSociedad() {
        return pSociedad;
    }

    /**
     * Define el valor de la propiedad pSociedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSociedad(String value) {
        this.pSociedad = value;
    }

    /**
     * @return the pMoneda
     */
    public String getPMoneda() {
        return pMoneda;
    }

    /**
     * @param pMoneda the pMoneda to set
     */
    public void setPMoneda(String pMoneda) {
        this.pMoneda = pMoneda;
    }
    
    
    

}
