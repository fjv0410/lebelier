package com.lebelier.suppliers.web.controller;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.lebelier.cfdi.comprobante.xml.entities.Comprobante;
import com.lebelier.cfdi.comprobante.xml.entities.Comprobante.Conceptos.Concepto;
import com.lebelier.cfdi.comprobante.xml.entities.Comprobante.Impuestos.Retenciones.Retencion;
import com.lebelier.suppliers.db.cayenne.entities.CCompanies;
import com.lebelier.suppliers.db.cayenne.entities.TInvoicesInsert;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.utils.InvoiceValidator;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UploadFilesSession;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.CfdiXmlParsingException;
import com.lebelier.suppliers.web.exception.DB_InsertException;
import com.lebelier.suppliers.web.exception.FileSaveFileException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.ValidationDocumentTypeException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.ValidationWebServiceSatException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.web.exception.WebServiceSatException;
import com.lebelier.suppliers.ws.client.exec.ConsultaCFDIServiceClient;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.Bapiret2;
import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesCxp;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesCxpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmValidateDeliveries;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmValidateDeliveriesResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZttCfdiConcepto;
import com.lebelier.suppliers.ws.sap.client.types.ZttDeliveryNumb;
import com.lebelier.suppliers.ws.sap.client.types.ZttRetenciones;
import com.lebelier.suppliers.ws.sap.client.types.ZtyCfdiConcepto;
import com.lebelier.suppliers.ws.sap.client.types.ZtyDeliveryNumb;
import com.lebelier.suppliers.ws.sat.client.types.Acuse;
import com.lebelier.suppliers.ws.sat.client.types.ConsultaResponse;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;

@Controller
public class InvoicesWithDeliveriesController  extends WebServiceGatewaySupport{

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;

    @Autowired
	private ServerRuntime cayenneRuntime;

	@Autowired
	private UploadFilesSession filesSession;
	
	@GetMapping("/payableAccounts/InvoicesWithDeliveries")
	public String goInvoicesWithDeliveriesPA() {
		return this.goInvoicesWithDeliveriesMx();
	}

	@RequestMapping(value = "/payableAccounts/invoiceWithDeliveries")
	public String processInvoiceWithDeliveriesPA(
		@RequestParam("delivery") String[] deliveryNumber, 
		HttpServletResponse resp) {
		
		return this.processInvoiceWithDeliveries(deliveryNumber, resp, true);
		
	}
	
	@GetMapping("/invoice/InvoicesWithDeliveries")
	public String goInvoicesWithDeliveriesMx() {
		filesSession.setPdfFile(null);
		filesSession.setXmlFile(null);
		
		return "InvoicesWithDeliveries";
	}

	@RequestMapping(value = "/invoice/invoiceWithDeliveries")
	public String processInvoiceWithDeliveriesMX(
			@RequestParam("delivery") String[] deliveryNumber, 
			HttpServletResponse resp
		) {

		return this.processInvoiceWithDeliveries(deliveryNumber, resp, false);
		
	}
	
	public String processInvoiceWithDeliveries(
			String[] deliveryNumber, 
			HttpServletResponse resp,
			boolean isPayableAccount
		) {
		
		String sReturnTo = "InvoicesWithDeliveries";
		
		String sTypMsg = "Error";
		String sMsg = "";

    	int iCnt = 1;
    	
		try {

			String sNumProvSap = userSession.getSap_supplier_id();
			
			MultipartFile mpXml = filesSession.getXmlFile();
			MultipartFile mpPdf = filesSession.getPdfFile();
			
			//(new XsdValidator()).validateComprobanteUsingXsd(mpXml.getOriginalFilename(), mpXml.getInputStream());
			
			String sTasaOCuota = "";
			String sUuid = "";
			String sFechaCfdi;
			String sTipoComprobante;
			String sRfcReceptor;
			String sFolioCfdi;
			String sMoneda;
			
			String sFormaPago;
			String sMetodoPago;
			String sUsoCfdi;
			String sSerie;
			
			BigDecimal bdImporte = new BigDecimal(0);
			BigDecimal bdTotalRetenciones = new BigDecimal(0);
			
			BigDecimal bdTpoCambio = new BigDecimal(0);
			String sRfcEmisor;

	        List<Concepto> lConceptos;

			Comprobante comprobante = new Comprobante();
			
			try {
		        JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);
		        
		        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		        comprobante = (Comprobante) unmarshaller.unmarshal(mpXml.getInputStream());
		        
		        lConceptos = comprobante.getConceptos().getConcepto();
		        
				sTipoComprobante = comprobante.getTipoDeComprobante().toString();
				sRfcReceptor = comprobante.getReceptor().getRfc();
				sFolioCfdi = comprobante.getFolio();
				sMoneda = comprobante.getMoneda().name();
				
				sFormaPago = comprobante.getFormaPago();
				sMetodoPago = comprobante.getMetodoPago().value();
				sUsoCfdi = comprobante.getReceptor().getUsoCFDI().value();
				
				sSerie = comprobante.getSerie();
				
				bdImporte = comprobante.getTotal();
				
				sFechaCfdi = comprobante.getFecha().toString();
				sFechaCfdi = sFechaCfdi.substring(0, sFechaCfdi.indexOf("T"));
				
				List<Object> lCompl = comprobante.getComplemento().get(0).getAny();
				
				for (Object object : lCompl) {
					ElementNSImpl element = (ElementNSImpl) object;
					String sName = element.getLocalName();
					if(sName.equals("TimbreFiscalDigital")){
						sUuid = element.getAttribute("UUID");	
					}
				}
				
				sRfcEmisor = comprobante.getEmisor().getRfc();
				bdTpoCambio = comprobante.getTipoCambio();
				if(bdTpoCambio == null) {
					bdTpoCambio = new BigDecimal(1);
				}
				
				if(comprobante.getImpuestos() != null && comprobante.getImpuestos().getRetenciones() != null && 
						comprobante.getImpuestos().getRetenciones().getRetencion() != null) {

					List<Retencion> retenciones = comprobante.getImpuestos().getRetenciones().getRetencion();
					
					for (Retencion retencion : retenciones) {
						bdTotalRetenciones = bdTotalRetenciones.add(retencion.getImporte());
					}
					 
				}
				
			}catch(Exception e) {
				e.printStackTrace();

	        	throw new CfdiXmlParsingException(mpXml.getOriginalFilename());
			}
			
			if(sUuid.equals("")) {
				throw new ValidationEmptyValueException("UUID");
			}
			
			InvoiceValidator.existUuid(cayenneRuntime, sUuid);
			
			if(!sTipoComprobante.trim().equals("I") && !sTipoComprobante.trim().equals("E")) {
				
				throw new ValidationDocumentTypeException(mpXml.getOriginalFilename(), sTipoComprobante);
				
			}

            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
            ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
            
            ConsultaCFDIServiceClient satClient = context.getBean(ConsultaCFDIServiceClient.class);
            
            ConsultaResponse consultaResp;
            
            try{
            	consultaResp = satClient.executeConsulta(environment.getProperty(PropetiesConstants.WS_SAT_UUID_WSDL), 
																	  environment.getProperty(PropetiesConstants.WS_SAT_UUID_ACTION), 
																	  comprobante.getEmisor().getRfc(), 
																	  sRfcReceptor, 
																	  comprobante.getTotal() + "", 
																	  sUuid);
            }catch(Exception e){
            	e.printStackTrace();
            	context.close();
            	
            	throw new  WebServiceSatException(e);
            }
            
			Acuse acuse =  consultaResp.getConsultaResult().getValue();
			String codigoEstatus = acuse.getCodigoEstatus().getValue();

			if(codigoEstatus.startsWith("N")){
            	context.close();
				throw new ValidationWebServiceSatException(sUuid, codigoEstatus);
			}
			
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_ARCH_PATH) + File.separator + "INV_" + sUuid;
			
			try {
				mpXml.transferTo(new File(sFilePath + ".xml"));
			} catch (Exception e) {
				e.printStackTrace();
                                context.close();
            	
				throw new FileSaveFileException(mpXml.getOriginalFilename());
			}

			try {
				mpPdf.transferTo(new File(sFilePath + ".pdf"));
			} catch (Exception e) {
				e.printStackTrace();
            	context.close();
            	
				throw new FileSaveFileException(mpPdf.getOriginalFilename());
			}
			
			
			ZttCfdiConcepto ttConceptos = new ZttCfdiConcepto();
			ZttDeliveryNumb ttDeliveryNumbers = new ZttDeliveryNumb();

	        for(int i = 0; i <= lConceptos.size() - 1; i++) {
	        	ZtyCfdiConcepto tyConcepto = new ZtyCfdiConcepto();
	        	Concepto cfdiConcepto = lConceptos.get(i);
	        	
	        	tyConcepto.setQuantity(cfdiConcepto.getCantidad());
	        	tyConcepto.setUnitValue(cfdiConcepto.getValorUnitario());
	        	
	        	sTasaOCuota = cfdiConcepto.getImpuestos().getTraslados().getTraslado().get(0).getTasaOCuota().toString();
	        	
	        	ttConceptos.getItem().add(tyConcepto);
	        }
	        
        	for(int j = 0; j < deliveryNumber.length; j++) {
        		ZtyDeliveryNumb tyDeliveryNumber = new ZtyDeliveryNumb();
    			String[] asDelivery = deliveryNumber[j].split("@");

    			tyDeliveryNumber.setDeliveryNumber(asDelivery[0]);
    			tyDeliveryNumber.setDeliveryDate(asDelivery[1]);
    			
    			ttDeliveryNumbers.getItem().add(tyDeliveryNumber);
        	}
        	

        	TableOfBapiret2 tBapiret2 = new TableOfBapiret2();
        	String sStatus = "";
        	if(isPayableAccount) {

        		ZMxfmDeliveriesCxp req = new ZMxfmDeliveriesCxp();
	
				req.setPtEntregas(ttDeliveryNumbers);
				req.setPtConceptosCfdi(ttConceptos);
				
				req.setPRfcReceptor(sRfcReceptor);
				req.setPTipoCfdi(sTipoComprobante);
				req.setPFechaCfdi(sFechaCfdi);
				req.setPFolioCfdi(sFolioCfdi);
				req.setPMonedaCfdi(sMoneda);
				req.setPTasaOCuota(sTasaOCuota);
				req.setPExtranjero("");
				req.setPUuid(sUuid);
				req.setPImporte(bdImporte);
				
				req.setPFormapago(sFormaPago);
				req.setPMetodopago(sMetodoPago);
				req.setPUsocfdi(sUsoCfdi);
				req.setPNosapProv(sNumProvSap);

				req.setPTipoCambio(bdTpoCambio);
				req.setPRfcEmisor(sRfcEmisor);

				req.setPTotalImpRet(bdTotalRetenciones);
				
				req.setPSerie(sSerie != null ? sSerie : "");
				
				ZttRetenciones tRet = new ZttRetenciones();
				req.setTRetenciones(tRet);
				
				req.setBapiret2(new TableOfBapiret2());
				
				ZMxfmDeliveriesCxpResponse wsResp;
				
				try {
					wsResp = client.executeZMxfmDeliveriesCxp(
	        			req,
	        			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
	        		);

				}catch(Exception e) {
					e.printStackTrace();
	            	context.close();
					
					throw new WebServiceSapException(e);
				}
				
				tBapiret2 = wsResp.getBapiret2();
				sStatus = wsResp.getRStatus();
				
        	}else{
        	
				ZMxfmValidateDeliveries req = new ZMxfmValidateDeliveries();
	
				req.setPtEntregas(ttDeliveryNumbers);
				req.setPtConceptosCfdi(ttConceptos);
				
				req.setPRfcReceptor(sRfcReceptor);
				req.setPTipoCfdi(sTipoComprobante);
				req.setPFechaCfdi(sFechaCfdi);
				req.setPFolioCfdi(sFolioCfdi);
				req.setPMonedaCfdi(sMoneda);
				req.setPTasaOCuota(sTasaOCuota);
				req.setPExtranjero("");
				req.setPUuid(sUuid);
				req.setPImporte(bdImporte);
				
				req.setPFormapago(sFormaPago);
				req.setPMetodopago(sMetodoPago);
				req.setPUsocfdi(sUsoCfdi);
				req.setPNosapProv(sNumProvSap);
				
				req.setPTipoCambio(bdTpoCambio);
				req.setPRfcEmisor(sRfcEmisor);
				
				req.setPTotalImpRet(bdTotalRetenciones);
				
				req.setPSerie(sSerie != null ? sSerie : "");
				
				ZttRetenciones tRet = new ZttRetenciones();
                                
				req.setTRetenciones(tRet);
				
				req.setBapiret2(new TableOfBapiret2());
				
				ZMxfmValidateDeliveriesResponse wsResp;
				
				try {
					wsResp = client.executeZMxfmValidateDeliveries(
	        			req,
	        			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
	        		);
					
				}catch(Exception e) {
					e.printStackTrace();
                                        context.close();
					throw new WebServiceSapException(e);
				}

				tBapiret2 = wsResp.getBapiret2();
				sStatus = wsResp.getRStatus();
				
        	}
        	
        	List<Bapiret2> lBapiret2 = tBapiret2.getItem();

        	context.close();
            	
			sTypMsg = "Warning";
	
        	switch (sStatus) {
				case "00":
					sTypMsg = "Success";

					resp.setHeader("process_msg_lst_" + iCnt++, "Estimado Proveedor su factura ha ingresado a revision para su proceso de pago." );
					resp.setHeader("process_msg_lst_" + iCnt++, "Le invitamos a revisar el status en el apartado \"Resumen de Status de Facturas\"." );

					filesSession.setPdfFile(null);
					filesSession.setXmlFile(null);
					
					ObjectContext cayenneContext = cayenneRuntime.newContext(); 
			    	
					CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.TAXID.eq(sRfcReceptor)).selectOne(cayenneContext);
			    	Long iIdCompany = (Long)db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);
					
			    	TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
					Long iIdSupplier = (Long)db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);
					
					LocalDate lDate = LocalDate.of(Integer.parseInt(sFechaCfdi.substring(0, 4)), Integer.parseInt(sFechaCfdi.substring(5,7)), Integer.parseInt(sFechaCfdi.substring(8, 10)));
					
					TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
					db_insert.setReference(sFolioCfdi);
					db_insert.setAmount(bdImporte.doubleValue());
					db_insert.setCurrency(sMoneda);
					db_insert.setUuid(sUuid);
					db_insert.setDocType(sTipoComprobante);
					db_insert.setCompany_id(iIdCompany.intValue());
					db_insert.setSupplier_id(iIdSupplier.intValue());
					db_insert.setInvoiceDate(lDate);
					db_insert.setStatus_id(1);
					db_insert.setWithDelivery(true);
					db_insert.setCreatedOn(LocalDate.now());
					
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						if(bapiret2.getType().equals("S")) {
							String sBapiretId = bapiret2.getId();
							db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
							db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
							db_insert.setSapCompany(sBapiretId.substring(0, 4));
						}

					}
	            	
	            	try {
	            		cayenneContext.commitChanges();
	            	}catch(Exception e) {
	            		throw new DB_InsertException(e);
	            	}
	            	
					break;
				case "01":
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						
						resp.setHeader("process_msg_lst_" + iCnt++, "Estimado Proveedor, la órden de entrega no es correcta." + " [" + bapiret2.getId() + "]" );

					}
	            	
					resp.setHeader("process_msg_lst_" + iCnt++, "Por favor verifique e intente nuevamente.");
					
					break;
				case "04":
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						
						sMsg = "Estimado Proveedor, el RFC de la factura ingresada al portal, no corresponde a ninguna empresa de nuestro grupo."+ " [" + bapiret2.getId() + "]";
						resp.setHeader("process_msg_lst_" + iCnt++, sMsg );
						
					}

					sMsg = "Por favor verifique, e intente nuevamente." ;
					resp.setHeader("process_msg_lst_" + iCnt++, sMsg );
					
					break;
				case "05":
					resp.setHeader("process_msg_lst_" + iCnt++, "Estimado Proveedor, la Cantidad y/o el precio en la Factura no coinciden con los datos ingresados en el Almacén." );

					resp.setHeader("process_msg_lst_" + iCnt++, "Por favor verifique e intente nuevamente." );
					
					break;
				case "08":
					resp.setHeader("process_msg_lst_" + iCnt++, "Estimado Proveedor, No es posible procesar la factura ingresada, por favor verifique e intente nuevamente." );

	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();

						sMsg = bapiret2.getMessage().concat(" [").concat(bapiret2.getId().concat("]"));

						resp.setHeader("process_msg_lst" + iCnt++, sMsg );
						
					}
	            	
					break;
				default:
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						
						sMsg = bapiret2.getMessage().concat(" [").concat(bapiret2.getId().concat("]"));

						resp.setHeader("process_msg_lst" + iCnt++, sMsg );
						
					}
	            	
	            	break;
	            	
			}

    		resp.setHeader("process_result", sTypMsg);
    		
			return sReturnTo;
					
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			
    		resp.setHeader("process_result", e.getFrontResult());
    		resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
    		
        }catch (Exception e) {
			e.printStackTrace();
			
    		resp.setHeader("process_result", "Error");
    		resp.setHeader("process_msg_lst", "Sucedió un error.");
    		
		}
		
		return sReturnTo;
		
	}

	@PostMapping(value = "/payableAccounts/uploadXmlWithDeliveries")
	public String uploadXmlWithDeliveriesPA(@RequestParam("xmlFile") MultipartFile p_file) {
		
        return uploadXmlWithDeliveriesMX(p_file);
		
	}

	@PostMapping(value = "/invoice/uploadXmlWithDeliveries")
	public String uploadXmlWithDeliveriesMX(@RequestParam("xmlFile") MultipartFile p_file) {
		
		filesSession.setXmlFile(p_file);
		
        return "InvoicesWithDeliveries";
		
	}

	@PostMapping(value = "/payableAccounts/uploadPdfWithDeliveries")
	public String uploadPdfWithDeliveriesPA(@RequestParam("pdfFile") MultipartFile p_file) {
		
        return uploadPdfWithDeliveriesMX(p_file);
		
	}
	
	@PostMapping(value = "/invoice/uploadPdfWithDeliveries")
	public String uploadPdfWithDeliveriesMX(@RequestParam("pdfFile") MultipartFile p_file) {

		filesSession.setPdfFile(p_file);
		
        return "InvoicesWithDeliveries";
		
	}
	
	
}
