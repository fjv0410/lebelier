package com.lebelier.suppliers.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.SelectById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesInsert;
import com.lebelier.suppliers.db.cayenne.entities.CRolesId;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersContactsInsert;
import com.lebelier.suppliers.web.constants.RolesConstants;
import com.lebelier.suppliers.web.entity.UserSession;

@Controller
public class UpdateContactsController  extends WebServiceGatewaySupport{

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;

    @Autowired
	private ServerRuntime cayenneRuntime;
	
	@GetMapping("/contacts/updateInformation")
	public String goUpdateContactInformation() {
		return "UpdateContacts";
	}
	
	@RequestMapping(value = "/contacts/pUpdateContactInformation")
	public String pUpdateContactInformation(
			@RequestParam("commercialName") String p_commercialName, 
			@RequestParam("commercialPhone") String p_commercialPhone, 
			@RequestParam("commercialEmail") String p_commercialEmail, 
			@RequestParam("fiscalName") String p_fiscalName, 
			@RequestParam("fiscalPhone") String p_fiscalPhone, 
			@RequestParam("fiscalEmail") String p_fiscalEmail, 
			@RequestParam("accountsName") String p_accountsName, 
			@RequestParam("accountsPhone") String p_accountsPhone,
			@RequestParam("accountsEmail") String p_accountsEmail,
			HttpServletResponse resp
		) {
		
		System.out.println("pUpdateContactInformation ... ");
		
		String sReturnTo = "Home";

    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	    	
	    	TSuppliersContactsInsert commercial = context.newObject(TSuppliersContactsInsert.class);
	    	commercial.setId_contact_type(1); // commercial = 1
	    	commercial.setId_supplier(userSession.getSupplier_id());
	    	commercial.setName(p_commercialName);
	    	commercial.setPhone(p_commercialPhone);
	    	commercial.setEmail(p_commercialEmail);

	    	TSuppliersContactsInsert fiscal = context.newObject(TSuppliersContactsInsert.class);
	    	fiscal.setId_contact_type(2); // fiscal = 2
	    	fiscal.setId_supplier(userSession.getSupplier_id());
	    	fiscal.setName(p_fiscalName);
	    	fiscal.setPhone(p_fiscalPhone);
	    	fiscal.setEmail(p_fiscalEmail);

	    	TSuppliersContactsInsert accounts = context.newObject(TSuppliersContactsInsert.class);
	    	accounts.setId_contact_type(3); // accounts = 2
	    	accounts.setId_supplier(userSession.getSupplier_id());
	    	accounts.setName(p_accountsName);
	    	accounts.setPhone(p_accountsPhone);
	    	accounts.setEmail(p_accountsEmail);

	    	ObjectId oIdRole = new ObjectId("CRolesId", CRolesId.ID_PK_COLUMN, userSession.getRoleId());
	    	CRolesId role = SelectById.query(CRolesId.class, oIdRole).selectOne(context);

	    	Map<String, String> mIdAuth = new HashMap<String, String>();
	    	mIdAuth.put(AuthoritiesInsert.ID_USER_PK_COLUMN, userSession.getUser_id() + "");
	    	mIdAuth.put(AuthoritiesInsert.ID_ROLE_PK_COLUMN, userSession.getRoleId() + "");
	    	AuthoritiesInsert auth = SelectById.query(AuthoritiesInsert.class, mIdAuth).selectOne(context);
	    	auth.setAuthority(RolesConstants.ROLE_PREFIX + role.getName());
	    	
			context.commitChanges();
			
    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Información actualizada correctamente.");
			resp.setHeader("process_msg_lst_" + iCnt++, "Favor de terminar su sesión e ingresar nuevamente para poder acceder a las opciones del portal.");
			
		}catch(Exception e) {
    		resp.setHeader("process_result", "Error");
    		
			resp.setHeader("process_msg_lst_" + iCnt++, "Sucedió un error al actualizar la información, por favor intente nuevamente.");
			
    		resp.setHeader("process_msg_tech_" + iCnt++, "Exception: " + e.getCause().getMessage());
			
			sReturnTo = "UpdateContacts";
			
		}
		
		return sReturnTo;
		
	}
	
}
