package com.lebelier.suppliers.web.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.apache.cayenne.query.SelectQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lebelier.suppliers.db.cayenne.entities.CCountriesId;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersInsert;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmUpdateSupplierResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZtyProveedor;

@Component
@Controller
public class SynchronizeSuppliersController {

	@Resource
    private Environment environment;

    @Autowired
	private ServerRuntime cayenneRuntime;

	@GetMapping("/admin/SynchronizeSuppliers")
	public String goUsersAdministration() {
		return "SynchronizeSuppliers";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/synchronizeSuppliers")
	public String synchronizeSuppliers(
			HttpServletResponse httpResp
		) {
		
		String sReturnTo = "SynchronizeSuppliers";
		
		List<ZtyProveedor> suppliers;
		
		int cntMessages = 0;
		
		ArrayList<String> failMessages = new ArrayList<String>();
		ArrayList<String> successMessages = new ArrayList<String>();
			
		try {
			try {
				suppliers = this.getSuppliersFromSap();
			}catch(Exception e) {
				throw new WebServiceSapException(e);
			}
			
			ObjectContext context = cayenneRuntime.newContext();
			
			for (ZtyProveedor zSupplier : suppliers) {
				 
				String supplierId = zSupplier.getLifnr();
				 
				String dbSuppId = ObjectSelect.columnQuery(TSuppliersQuery.class, TSuppliersQuery.ID_SAP).where(TSuppliersQuery.ID_SAP.eq(supplierId)).selectOne(context);
				
				if(dbSuppId == null || dbSuppId.equals("")) {
					
					CCountriesId cCountry = new CCountriesId();
					CCountriesId db_country = SelectQuery.query(CCountriesId.class, CCountriesId.ID_SAP.eq("MX")).selectOne(context);
					Long idCountry = (Long)db_country.getObjectId().getIdSnapshot().get(CCountriesId.ID_PK_COLUMN);
					
					cCountry.setObjectId(db_country.getObjectId());
					
					TSuppliersInsert newSupplier = context.newObject(TSuppliersInsert.class);
					newSupplier.setName("");
					newSupplier.setIdSap(supplierId);
					newSupplier.setId_country(idCountry.intValue());
					newSupplier.setCreatedOn(LocalDateTime.now());
					
					try {
						context.commitChanges();
						successMessages.add("Proveedor registrado correctamente: " + supplierId);
					}catch(Exception e) {
						e.printStackTrace();
						failMessages.add("Error al registrar el proveedor: " + supplierId);
					}
					
				}
				
			}

			if(failMessages.size() <= 0) {
				httpResp.setHeader("process_result", "Success");
				if(successMessages.size() == 0) {
					httpResp.setHeader("process_msg_lst" + cntMessages++, "No se encontraron proveedores para sincronizar.");
				}
			}else {
				httpResp.setHeader("process_result", "Error");
			}
			
			if(successMessages.size() > 0){
				for (String message : successMessages) {
					httpResp.setHeader("process_msg_lst" + cntMessages++, message);
				}
			}
			
			if(failMessages.size() > 0) {
				for (String message : failMessages) {
					httpResp.setHeader("process_msg_lst" + cntMessages++, message);
				}
			}
			
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			httpResp.setHeader("process_result", e.getFrontResult());
			httpResp.setHeader("process_msg_lst" + cntMessages++, e.getFrontMessage());
		}
		return sReturnTo;
	}
	
	private List<ZtyProveedor> getSuppliersFromSap(){

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
        
        ZMxfmUpdateSupplierResponse wsResp = client.executeZMxfmUpdateSupplier(
    			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
    		);
    	
		List<ZtyProveedor> lSuppliers = wsResp.getTProveedor().getItem();
		
		context.close();
    	
		return lSuppliers;
		
	}
	
}
