package com.lebelier.suppliers.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.SelectById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.db.cayenne.entities.TInvoicesInsert;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.exception.ValidationDocumentTypeException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.ValidationWebServiceSatException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.web.exception.WebServiceSatException;
import com.lebelier.suppliers.ws.client.exec.ConsultaCFDIServiceClient;
import com.lebelier.suppliers.web.exception.DB_UpdateException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.web.utils.Utils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPrelmaryFv60Ext;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPrelmaryFv60ExtResponse;
import java.math.BigDecimal;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60Cxp;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60CxpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01Response;
import com.lebelier.suppliers.ws.sap.client.types.ZttRetenciones;
import com.lebelier.suppliers.ws.sat.client.types.Acuse;
import com.lebelier.suppliers.ws.sat.client.types.ConsultaResponse;
import javax.annotation.Resource;
import org.springframework.core.env.Environment;

@Component
@Controller
public class AuthorizeInvoicesController {

    @Autowired
    private ServerRuntime cayenneRuntime;
    @Resource
    private Environment environment;

    @GetMapping("/payableAccounts/AuthorizeInvoices")
    public String goReportInvoicesStatus() {
        return "AuthorizeInvoices";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/payableAccounts/authorizeInvoice")
    public String authorizeInvoice(
            @RequestParam(value = "id") String p_id,
            @RequestParam(value = "auth") String p_auth,
            @RequestParam(value = "reference") String p_reference,
            @RequestParam(value = "comments") String p_comments,
            HttpServletResponse resp
    ) {

        String sReturnTo = "AuthorizeInvoices";

        int iCnt = 1;
        String sStatus = "";
        try {
            ObjectContext ctxCayenne = cayenneRuntime.newContext();

            ObjectId oId = new ObjectId("TInvoicesInsert", TInvoicesInsert.ID_PK_COLUMN, p_id);

            TInvoicesInsert invoice = SelectById.query(TInvoicesInsert.class, oId).selectOne(ctxCayenne);

            // Es una factura de proveedor nacional sin numero de entrega 
            if (invoice.getRfcEmisor() != null && invoice.getRfcReceptor() != null) {
                TableOfBapiret2 tBapiret2 = new TableOfBapiret2();
                AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
                ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);

                ConsultaCFDIServiceClient satClient = context.getBean(ConsultaCFDIServiceClient.class);

                ConsultaResponse consultaResp;

                try {
                    consultaResp = satClient.executeConsulta(environment.getProperty(PropetiesConstants.WS_SAT_UUID_WSDL),
                            environment.getProperty(PropetiesConstants.WS_SAT_UUID_ACTION),
                            invoice.getRfcEmisor(),
                            invoice.getRfcReceptor(),
                            String.valueOf(invoice.getAmount()),
                            invoice.getUuid());
                } catch (Exception e) {
                    e.printStackTrace();
                    context.close();

                    throw new WebServiceSatException(e);
                }

                Acuse acuse = consultaResp.getConsultaResult().getValue();
                String codigoEstatus = acuse.getCodigoEstatus().getValue();

                if (codigoEstatus.startsWith("N")) {
                    context.close();
                    throw new ValidationWebServiceSatException(invoice.getUuid(), codigoEstatus);
                }

                System.out.println(" Es un nacional sin numero ");
                ZmxfmPreliminaryPostingFb01 wsReq = new ZmxfmPreliminaryPostingFb01();

                wsReq.setPUuid(invoice.getUuid());
                wsReq.setPRfcReceptor(invoice.getRfcReceptor());
                wsReq.setPTipoCfdi(invoice.getTipoCfdi());
                wsReq.setPFechaCfdi(Utils.dateToString(invoice.getFechaCfdi()));
                wsReq.setPRfcEmisor(invoice.getRfcEmisor());
                wsReq.setPFechaContab(Utils.dateToString(invoice.getFechaContab()));
                wsReq.setPFolioCfdi(invoice.getFolioCfdi());
                wsReq.setPTotal(new BigDecimal(invoice.getTotalImporte()));
                wsReq.setPMonedaCfdi(invoice.getCurrency());
                wsReq.setPTasaOCuota(invoice.getTasa());
                wsReq.setPUsocfdi(invoice.getUsoCfdi());
                wsReq.setPFormapago(invoice.getFormaPago());
                wsReq.setPNosapProv(invoice.getNoProvedorSap());

                wsReq.setPMetodopago(invoice.getMetodoPago());

                wsReq.setPTotalImpRet(new BigDecimal(invoice.getTotalImporte()));

                wsReq.setPTipocambio(new BigDecimal(invoice.getTipoCambio()));

                wsReq.setPSerie(invoice.getSerie());

                ZttRetenciones tRetenciones = new ZttRetenciones();
                wsReq.setTRetenciones(tRetenciones);

                wsReq.setBapiret2(new TableOfBapiret2());

                ZmxfmPreliminaryPostingFb01Response wsResp;
                try {
                    wsResp = client.executeZmxfmPreliminaryPostingFb01(
                            wsReq,
                            environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
                    );

                    tBapiret2 = wsResp.getBapiret2();
                    sStatus = wsResp.getRStatus();
                    String mensajeSap = obtenerMensajeSap(sStatus, invoice.getUuid());
                    resp.setHeader("process_msg_lst_", mensajeSap);
                    String sAction = "autorizada";

                    if (p_auth.equals("auth")) {
                        invoice.setStatus_id(2);
                    } else if (p_auth.equals("reject")) {
                        invoice.setStatus_id(3);
                        sAction = "rechazada";
                    }

                    invoice.setComments(p_comments);

                    try {
                        ctxCayenne.commitChanges();
                    } catch (Exception e) {
                        throw new DB_UpdateException(e);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    context.close();

                    throw new WebServiceSapException(e);
                }

                //tBapiret2 = wsResp.getBapiret2();
                //sStatus = wsResp.getRStatus();
            } //Es un extranjero sin numero 
            else {
                ZmxfmPrelmaryFv60Ext wsReq = new ZmxfmPrelmaryFv60Ext();

                wsReq.setPConcepto(invoice.getConcepto());
                BigDecimal bdImporte = new BigDecimal(invoice.getAmount().toString());
                String mes = "";
                if (invoice.getInvoiceDate().getMonthValue() < 9) {
                    mes = "0" + invoice.getInvoiceDate().getMonthValue();
                } else {
                    mes = String.valueOf(invoice.getInvoiceDate().getMonthValue());
                }

                wsReq.setPFechaFactura(invoice.getInvoiceDate().getYear() + "-" + mes + "-" + invoice.getInvoiceDate().getDayOfMonth());
                wsReq.setPImporte(bdImporte);
                wsReq.setPIsFactONc(invoice.getDocType());
                wsReq.setPMoneda(invoice.getCurrency());
                wsReq.setPNoFactONc(invoice.getNoFactura());
                wsReq.setPNoSapProv(invoice.getNoProvedorSap());
                wsReq.setPSociedad(invoice.getSociedad());

                wsReq.setBapiret2(new TableOfBapiret2());

                AnnotationConfigApplicationContext contextWs = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
                ZmxSuppliers_Client client = contextWs.getBean(ZmxSuppliers_Client.class);

                ZmxfmPrelmaryFv60ExtResponse wsResp = new ZmxfmPrelmaryFv60ExtResponse();

                try {
                    wsResp = client.executeZmxfmPrelmaryFv60Ext(
                            wsReq,
                            environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
                    );
                } catch (Exception e) {
                    contextWs.close();

                    throw new WebServiceSapException(e);

                }

                contextWs.close();

                String sAction = "autorizada";

                if (p_auth.equals("auth")) {
                    invoice.setStatus_id(2);
                } else if (p_auth.equals("reject")) {
                    invoice.setStatus_id(3);
                    sAction = "rechazada";
                }

                invoice.setComments(p_comments);

                try {
                    ctxCayenne.commitChanges();
                } catch (Exception e) {
                    throw new DB_UpdateException(e);
                }

                resp.setHeader("process_result", "Success");
                resp.setHeader("process_msg_lst_" + iCnt++, "Factura " + sAction + " correctamente: " + p_reference);

            }

        } catch (SuppliersGeneralException e) {
            e.printStackTrace();
            resp.setHeader("process_result", e.getFrontResult());
            resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
        } catch (Exception e) {
            e.printStackTrace();
            resp.setHeader("process_result", "Error");
            resp.setHeader("process_msg_lst", "Sucedió un error en la aplicación.");
        }

        return sReturnTo;

    }

    public String obtenerMensajeSap(String sStatus, String folioFiscal) {
        String mensaje = "";
        switch (sStatus) {
            case "00":
                mensaje = "Factura ingresada a SAP  correctamente";
                break;
            case "011":
                mensaje = "No es posible procesar facturas de años anteriores.";
                break;
            case "012":
                mensaje = "La fecha de emisión de la factura no puede ser anterior a 30 días";
                break;
            case "015":
                mensaje = "RFC Receptor no corresponde a alguna de nuestras empresas.";
                break;
            case "013":
                mensaje = "RFC Emisor no corresponde con proveedor actual.";
                break;
            case "014":
                mensaje = "El proveedor actual no se encuentra regitrado en SAP.";
                break;
        }

        return mensaje;
    }

}
