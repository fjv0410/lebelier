package com.lebelier.suppliers.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lebelier.suppliers.db.dto.InvoicesWithoutPaymentDto;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmFacturasSinCp;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmFacturasSinCpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZtyFactsincomp;

@Component
@RestController
public class InvoicesWithoutPaymentJSonController {

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;
	
	@GetMapping(path = "/payableAccounts/InvoicesWithoutPayments", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InvoicesWithoutPaymentDto>> getInvoicesWithoutPayments(){
		String suppId = userSession.getSap_supplier_id();
		
		List<InvoicesWithoutPaymentDto> lstInvoices = new ArrayList<InvoicesWithoutPaymentDto>();
		
		List<ZtyFactsincomp> lstSap = new ArrayList<ZtyFactsincomp>();
		
		try {
		
			try {
				lstSap = getInvoicesWithouPayment(suppId);
			}catch(Exception e) {
				throw new WebServiceSapException(e);
				
			}
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().header("process_result", e.getFrontResult()).header("process_msg", e.getFrontMessage()).body(lstInvoices);
		}
		
		for (Iterator<ZtyFactsincomp> itSap = lstSap.iterator(); itSap.hasNext();) {
			ZtyFactsincomp zSap = (ZtyFactsincomp) itSap.next();
			
			InvoicesWithoutPaymentDto dto = new InvoicesWithoutPaymentDto();
			
			dto.setAmount("");
			dto.setCompany("");
			dto.setReference(zSap.getXblnr());
			dto.setFolio(zSap.getBelnr());
			dto.setPaymentDate("");
			dto.setPaymentDoc("");
			dto.setSupplier(zSap.getLifnr());
			
			lstInvoices.add(dto);
			
		}
		
		return ResponseEntity.ok().body(lstInvoices);
		
	}
	
	private List<ZtyFactsincomp> getInvoicesWithouPayment(String p_supplierId){

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
        
		ZMxfmFacturasSinCp req = new ZMxfmFacturasSinCp();
		req.setPProveedor(p_supplierId);
		
		ZMxfmFacturasSinCpResponse wsResp = client.executeZMxfmFacturasSinCp(
    			req,
    			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
    		);
    	
		List<ZtyFactsincomp> respInvoices = wsResp.getTFactUuid().getItem();
		
		context.close();
    	
		return respInvoices;
		
	}	
}
