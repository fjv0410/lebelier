package com.lebelier.suppliers.web.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@Controller
public class ReportInvoicesStatusController {
	
	@GetMapping("/report/ReportInvoicesStatus")
	public String goReportInvoicesStatus() {
		return "ReportInvoicesStatus";
	}
	
	
}
