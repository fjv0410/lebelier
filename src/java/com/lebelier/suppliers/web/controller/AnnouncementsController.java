package com.lebelier.suppliers.web.controller;

import java.io.File;
import java.time.LocalDate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.CayenneRuntimeException;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.SelectById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lebelier.suppliers.db.cayenne.entities.TAnnouncementsInsert;
import com.lebelier.suppliers.web.constants.PropetiesConstants;

@Component
@Controller
public class AnnouncementsController {

	@Resource
    private Environment environment;

    @Autowired
	private ServerRuntime cayenneRuntime;

	@GetMapping("/admin/AnnouncementsAdmin")
	public String goAnnouncementsAdmin() {
		return "AnnouncementsAdmin";
	}

	@GetMapping("/announcements/Supplier")
	public String goAnnouncementsSupplier() {
		return "AnnouncementsSupplier";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/deleteAnnouncement")
	public String deleteAnnouncement(
			@RequestParam(value = "id") String p_id,
			@RequestParam(value = "subject") String p_subject, 
			HttpServletResponse resp
		) {
		
		System.out.println("deleteAnnouncement ... ");
		
		String sReturnTo = "AnnouncementsAdmin";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 

	    	ObjectId oId = new ObjectId("TAnnouncementsInsert", TAnnouncementsInsert.ID_PK_COLUMN, p_id);
	    	
	    	TAnnouncementsInsert announcementDb = SelectById.query(TAnnouncementsInsert.class, oId).selectOne(context);
	    	
	    	context.deleteObject(announcementDb);
	    	
			context.commitChanges();
			
    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Anuncio eliminado correctamente: " + p_subject);
			
			if(announcementDb.getFileName() != null && !announcementDb.getFileName().equals("")) {
				String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_ANNOUNCEMENTS_PATH) + announcementDb.getFileName();
				
				File fFile = new File(sFilePath);
				
				if(fFile.exists()) {
					fFile.delete();
				}
				
			}
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al eliminado el anuncio: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/updateAnnouncement")
	public String updateAnnouncement(
			@RequestParam(value = "id") String p_id,
			@RequestParam(value = "subject") String p_subject, 
			@RequestParam(value = "body") String p_body,
			HttpServletResponse resp
		) {
		
		System.out.println("updateAnnouncement ... ");
		
		String sReturnTo = "AnnouncementsAdmin";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 

	    	ObjectId oId = new ObjectId("TAnnouncementsInsert", TAnnouncementsInsert.ID_PK_COLUMN, p_id);
	    	
	    	TAnnouncementsInsert announcementDb = SelectById.query(TAnnouncementsInsert.class, oId).selectOne(context);
	    	
	    	announcementDb.setSubject(p_subject);
	    	announcementDb.setBody(p_body);

			context.commitChanges();
			
    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Anuncio actualizado correctamente: " + p_subject);
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al actualizar el anuncio: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/createAnnouncement")
	public String createAnnouncement(
			@RequestParam(value = "subject") String p_subject, 
			@RequestParam(value = "body") String p_body,
			@RequestParam(value = "file", required = false) String p_fileName,
			HttpServletResponse resp
		) {
		
		System.out.println("createAnnouncement ... ");
		
		String sReturnTo = "AnnouncementsAdmin";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	    	
	    	LocalDate ldate = LocalDate.now();
	    	
	    	TAnnouncementsInsert announcement = context.newObject(TAnnouncementsInsert.class);
	    	
	    	announcement.setDate(ldate);
	    	announcement.setSubject(p_subject);
	    	announcement.setBody(p_body);
	    	announcement.setFileName(p_fileName);
		announcement.setEstatus(1);
			context.commitChanges();
			
    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Anuncio creado correctamente: " + p_subject);
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al crear el anuncio: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}
	
	@PostMapping(value = "/admin/uploadAnnouncementFile")
	@ResponseBody
	public String uploadXmlWithoutDeliveries(
			@RequestParam(value = "file", required = true) MultipartFile mpFile,
			HttpServletResponse resp) {
		
		String sRedirectTo = "uploadAnnouncementFile";
		
		try {
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_ANNOUNCEMENTS_PATH) + mpFile.getOriginalFilename();
			
			File fFile = new File(sFilePath);
			
			/*if(fFile.exists()) {
				resp.setStatus(HttpServletResponse.SC_CONFLICT);
				
				resp.setHeader("upload_result", "Error");
				resp.setHeader("upload_msg", "El archivo " + mpFile.getOriginalFilename() + " ya existe en el servidor, por favor seleccione un archivo diferente.");
				
				return sRedirectTo;
			}*/
			
			mpFile.transferTo(fFile);
			
			resp.setStatus(HttpServletResponse.SC_OK);

			resp.setHeader("upload_result", "Success");
			resp.setHeader("upload_msg", "Archivo almacenado correctamente: " + sFilePath + ".");
			
		}catch(Exception e){
			
			e.printStackTrace();
			
			resp.setStatus(HttpServletResponse.SC_CONFLICT);

			resp.setHeader("upload_result", "Error");
			resp.setHeader("upload_msg", "Sucedió un error al almacenar el archivo: " + mpFile.getOriginalFilename() + ".");
			resp.setHeader("upload_msg_tech", e.getMessage() + ".");
			
		}
		
        return sRedirectTo;
        
	}


}
