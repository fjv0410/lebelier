package com.lebelier.suppliers.web.controller;

import java.security.Principal;
import java.time.LocalDate;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesBase;
import com.lebelier.suppliers.db.cayenne.entities.TContactMessagesInsert;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.cayenne.entities.UsersQuery;
import com.lebelier.suppliers.email.service.EmailSender;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.security.PasswordHandler;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"userSession"})
public class LoginController {

	@Resource
    private Environment environment;
	
	@Autowired
	private UserSession userSession;

        @Autowired
	private ServerRuntime cayenneRuntime;

        @Autowired
        private JavaMailSender javaMailSender;

	private final String PASSWORD_ENCODER_PREFIX = "{bcrypt}";
    
	@GetMapping("/home")
	public String goHome(Principal principal, Model model, HttpServletRequest request) {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	UsersQuery userDb = ObjectSelect.query(UsersQuery.class).where(UsersQuery.USERNAME.eq(principal.getName())).selectOne(context);

		Map<String, Object> mId = userDb.getObjectId().getIdSnapshot();
		Long iId = (Long)mId.get(UsersQuery.ID_PK_COLUMN);
		userSession.setUser_id(iId.intValue());
		
		mId = userDb.getTSuppliers().getObjectId().getIdSnapshot();
		iId = (Long)mId.get(TSuppliersQuery.ID_PK_COLUMN);
		userSession.setSupplier_id(iId.intValue());

		mId = userDb.getAuthoritieses().get(0).getObjectId().getIdSnapshot();
		iId = (Long)mId.get(AuthoritiesBase.ID_ROLE_PK_COLUMN);
		userSession.setRoleId(iId.intValue());
		userSession.setUser_name(userDb.getName());
                userSession.setUser_lastname(userDb.getLastname());
                userSession.setSupplier_name(userDb.getTSuppliers().getName());
                userSession.setSap_supplier_id(userDb.getTSuppliers().getIdSap());
                
                boolean tieneMensajes=false;
                Long cantidadMensajes=null; 
                //Si el usuario es administrador verificar si tiene mensajes 
                if(iId.intValue()==5){
               
                    cantidadMensajes = ObjectSelect.query(TContactMessagesInsert.class).where(TContactMessagesInsert.ESTATUS.eq("1")).selectCount(context);
                    System.out.println("Cantidad de mensajes "+cantidadMensajes);
                    if(cantidadMensajes>0)
                        tieneMensajes=true;
                }
                
		model.addAttribute("user_name", userSession.getUser_name());
		model.addAttribute("user_lastname", userSession.getUser_lastname());
		model.addAttribute("tieneMensajes",tieneMensajes);
                model.addAttribute("cantidadMensajes", cantidadMensajes);
                HttpSession session = request.getSession();
                session.setAttribute("user", userSession);
		return "Home";
	}
	
	@GetMapping("/")
	public String goLogin() {
		return "Login";
	}
/*	
	@GetMapping("/access-denied")
	public String showAccessDenied() {
		return "access-denied";
	}
*/
	@GetMapping("/ChangePassword")
	public String goChangePassword() {
		return "ChangePassword";
	}

	@PostMapping("/changePassword")
	public String processChangePassword(
			@RequestParam("id") String p_username,
			@RequestParam("code") String p_code,
			@RequestParam("password") String p_password,
			@RequestParam("confirmation") String p_confirmation,
			Model model
		) {
		
		ObjectContext context = cayenneRuntime.newContext(); 

		if(p_password.equals(p_confirmation)) {

			UsersQuery user = ObjectSelect.query(UsersQuery.class).where(UsersQuery.USERNAME.eq(p_username)).and(UsersQuery.RESET_PASS_CODE.eq(p_code)).selectOne(context);			
	    	
			if(user != null) {
				
				user.setPassword(this.PASSWORD_ENCODER_PREFIX + PasswordHandler.encodePassword(p_password));
				user.setResetPassCode(null);
				
				context.commitChanges();
				
				model.addAttribute("result_msg_success", "Estimado usuario su contraseña ha sido actualizada.");
				
			}else {
				model.addAttribute("result_msg_error", "Usuario inválido");
			}
			
		}else {
			model.addAttribute("result_msg_error", "Las contraseñas no coinciden, favor de intentar nuevamente.");
		}
		
		
		return "ChangePassword";
		
	}

	@GetMapping("/ValidateCode")
	public String goValidateCode() {
		return "ValidateCode";
	}

	@PostMapping("/validateCode")
	public String processValidateCode(
			@RequestParam("username") String p_username,
			@RequestParam("code") String p_code,
			Model model
		) {
		
		ObjectContext context = cayenneRuntime.newContext(); 
		
		UsersQuery user = ObjectSelect.query(UsersQuery.class).where(UsersQuery.USERNAME.eq(p_username)).and(UsersQuery.RESET_PASS_CODE.eq(p_code)).selectOne(context);
		
		if(user != null) {
			
			model.addAttribute("id", p_username);
			model.addAttribute("code", p_code);
			return "redirect:/ChangePassword";
			
		}else {
			model.addAttribute("username", p_username);
			model.addAttribute("code", p_code);
			model.addAttribute("result_msg_error", "Código inválido");

			return "ValidateCode";
		}
		
	}


	@GetMapping("/ResetPassword")
	public String goResetPassword() {
		return "ResetPassword";
	}
	
	@PostMapping("/resetPassword")
	public String processResetPassword(
			HttpServletRequest p_request,
			@RequestParam("username") String p_username,
			Model model
		) {
		
		ObjectContext context = cayenneRuntime.newContext(); 
		
		String sCode = PasswordHandler.generateResetPasswordCode(7);
		
		UsersQuery user = ObjectSelect.query(UsersQuery.class).where(UsersQuery.USERNAME.eq(p_username)).selectOne(context);
		
		model.addAttribute("username", p_username);
		
		if(user != null) {
			
			user.setResetPassCode(sCode);
			context.commitChanges();
			
			model.addAttribute("result_msg_success", "Proceso de restablecimiento de contraseña iniciado.");
			
			try {

		        String sUrl = environment.getProperty(PropetiesConstants.WEB_SUPP_PASS_RESET_URL);
		        EmailSender eSender = new EmailSender();
		        
		        String sBodyMail = "Estimado proveedor " + user.getName() + " " + user.getLastname() + " recibimos su solicitud para restablecer su contraseña.\n\n"
		        		+ "Para cambiar su contraseña por favor ingrese en la siguiente dirección y escriba el código proporcionado. \n\n"
		        		+ "\tDirección: " + sUrl + "\n\n"
		        		+ "\tCódigo: " + sCode + "\n\n"
		        		+ "LeBelier";
		        
		        eSender.sendSimpleMessage(javaMailSender, user.getEmail(), "LeBelier - Portal Proveedores - Restablecer Contraseña", sBodyMail);
		        
				model.addAttribute("result_msg_success", "Proceso de restablecimiento de contraseña iniciado correctamente, por favor revise el correo electrónico que le hemos envíado.");
				
			}catch(MailSendException e) {
				model.addAttribute("result_msg_error", "Sucedió un error al enviar el correo electrónico.");
			}
			
		}else {
			model.addAttribute("result_msg_error", "Usuario no registrado.");
		}
		
		return "ResetPassword";
		
	}

	@GetMapping("/ChangeEmail")
	public String goChangeEmail() {
		return "ChangeEmail";
	}

	@PostMapping("/changeEmail")
	public String processChangeEmail(
			@RequestParam("username") String p_username, 
			@RequestParam("email") String p_email, 
			@RequestParam("motive") String p_motive,
			Model model
		) {
		
		ObjectContext context = cayenneRuntime.newContext(); 

		UsersQuery user = ObjectSelect.query(UsersQuery.class).where(UsersQuery.USERNAME.eq(p_username)).selectOne(context);
		
		if(user != null) {
	        String sSubject = "Solicitud de Cambio de Correo Electrónico.";
	        
	        String sBody = "El usuario " + user.getName() + " " + user.getLastname() + " del proveedor " + user.getTSuppliers().getName() + " solicita se cambie su dirección de correo electrónico.\n\n"
	        		+ "Nuevo Correo: " + p_email + "\n\n"
	        		+ "Motivo: " + p_motive;
	        

			Map<String, Object> mId = user.getObjectId().getIdSnapshot();
			Long iId = (Long)mId.get(UsersQuery.ID_PK_COLUMN);
			
	    	TContactMessagesInsert message = context.newObject(TContactMessagesInsert.class);
	    	message.setDate(LocalDate.now());
	    	message.setSubject(sSubject);
	    	message.setBody(sBody);
	    	message.setSender_id(iId.intValue());
	    	
	    	context.commitChanges();

			model.addAttribute("result_msg_success", "Estimado usuario, su solicitud de cambio de correo está siendo validada.");
			
		}else {
			model.addAttribute("result_msg_error", "Usuario no registrado.");
		}
		
    	return "ChangeEmail";
		
	}
	
}









