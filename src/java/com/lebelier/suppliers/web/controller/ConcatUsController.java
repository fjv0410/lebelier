package com.lebelier.suppliers.web.controller;

import java.time.LocalDate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.CayenneRuntimeException;
import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesQuery;
import com.lebelier.suppliers.db.cayenne.entities.TContactMessagesInsert;
import com.lebelier.suppliers.email.service.EmailSender;
import com.lebelier.suppliers.web.constants.RolesConstants;
import com.lebelier.suppliers.web.entity.UserSession;

@Component
@Controller
public class ConcatUsController {

	@Resource
    private Environment environment;

    @Autowired
	private ServerRuntime cayenneRuntime;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private UserSession userSession;
    
	@GetMapping("/admin/ContactUs")
	public String goContactUsAdmin() {
		return "ContactUsAdmin";
	}

	@GetMapping("/contact_us/Messages")
	public String goContactUs() {
		return "ContactUs";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/contact_us/createMessage")
	public String createMessage(
			@RequestParam(value = "subject") String p_subject, 
			@RequestParam(value = "body") String p_body,
			HttpServletResponse resp
		) {
		
		System.out.println("createMessage ... ");
		
		String sReturnTo = "ContactUs";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	    	
	    	LocalDate lDate = LocalDate.now();
	    	
	    	TContactMessagesInsert message = context.newObject(TContactMessagesInsert.class);
	    	
	    	message.setDate(lDate);
	    	message.setSubject(p_subject);
	    	message.setBody(p_body);
	    	message.setSender_id(userSession.getUser_id());
                message.setEstatus("1");
			context.commitChanges();
			
			resp.setHeader("process_msg_lst_" + iCnt++, "Mensaje creado correctamente: " + p_subject);

			try {
		        EmailSender eSender = new EmailSender();
		        
		        AuthoritiesQuery authDb =  ObjectSelect.query(AuthoritiesQuery.class).where(AuthoritiesQuery.AUTHORITY.eq(RolesConstants.ROLE_PREFIX + RolesConstants.ADMIN)).selectOne(context);

		        eSender.sendSimpleMessage(javaMailSender, authDb.getUsers().getEmail(), p_subject, p_body);
	
	    		resp.setHeader("process_result", "Success");
	    		
			}catch(MailSendException e) {
	    		resp.setHeader("process_result", "Warning");
				resp.setHeader("process_msg_lst_" + iCnt++, "Pero sucedió un error al enviar el correo electrónico al administrador.");
			}
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al crear el mensaje: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}
	
}
