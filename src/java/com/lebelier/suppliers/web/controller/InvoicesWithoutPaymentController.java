package com.lebelier.suppliers.web.controller;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@Controller
public class InvoicesWithoutPaymentController {
	
	@GetMapping("/payableAccounts/InvoicesWithoutPayment")
	public String goReportInvoicesStatus() {
		return "InvoicesWithoutPayment";
	}
	
	
}
