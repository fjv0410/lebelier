package com.lebelier.suppliers.web.controller;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.apache.cayenne.query.SelectById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesQuery;
import com.lebelier.suppliers.db.cayenne.entities.CCountriesTrans;
import com.lebelier.suppliers.db.cayenne.entities.CRolesId;
import com.lebelier.suppliers.db.cayenne.entities.CRolesTrans;
import com.lebelier.suppliers.db.cayenne.entities.TAnnouncementsQuery;
import com.lebelier.suppliers.db.cayenne.entities.TContactMessagesQuery;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.cayenne.entities.UsersQuery;
import com.lebelier.suppliers.db.dto.AnnouncementDto;
import com.lebelier.suppliers.db.dto.ContactMessageDto;
import com.lebelier.suppliers.db.dto.RoleDto;
import com.lebelier.suppliers.db.dto.SupplierDto;
import com.lebelier.suppliers.db.dto.UserDto;
import com.lebelier.suppliers.db.dto.UserEditDto;
import com.lebelier.suppliers.web.entity.UserSession;

@RestController
public class JSonRestController {

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;
	
    @Autowired
	private ServerRuntime cayenneRuntime;

	@GetMapping(path = "/contact_us/getMessages", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ContactMessageDto> getMessagesByUser() {
		ObjectContext context = cayenneRuntime.newContext();
		
		int sessionUserId = userSession.getUser_id();

		ObjectId oSessionUserId = new ObjectId("UsersQuery", UsersQuery.ID_PK_COLUMN, sessionUserId);

		UsersQuery userQuery = new UsersQuery();
		userQuery.setObjectId(oSessionUserId);
		
    	List<TContactMessagesQuery> lMessagesDb = ObjectSelect.query(TContactMessagesQuery.class).where(TContactMessagesQuery.SENDER.eq(userQuery)).orderBy(TContactMessagesQuery.DATE.desc()).select(context);
    	
    	List<ContactMessageDto> lMessagesDto = new ArrayList<ContactMessageDto>();
    	
    	for (Iterator<TContactMessagesQuery> iterator = lMessagesDb.iterator(); iterator.hasNext();) {
    		TContactMessagesQuery messageDb = (TContactMessagesQuery) iterator.next();
			
    		ContactMessageDto messageDto = new ContactMessageDto();
			
			messageDto.setDate(messageDb.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			messageDto.setSubject(messageDb.getSubject());
			messageDto.setBody(messageDb.getBody());
			
			lMessagesDto.add(messageDto);
			
		}
    	
		return lMessagesDto;
	}
	
	@GetMapping(path = "/admin/getMessages", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ContactMessageDto> getMessages() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<TContactMessagesQuery> lMessagesDb = ObjectSelect.query(TContactMessagesQuery.class).orderBy(TContactMessagesQuery.DATE.desc()).select(context);
    	
    	List<ContactMessageDto> lMessagesDto = new ArrayList<ContactMessageDto>();
    	
    	for (Iterator<TContactMessagesQuery> iterator = lMessagesDb.iterator(); iterator.hasNext();) {
    		TContactMessagesQuery messageDb = (TContactMessagesQuery) iterator.next();
			
    		ContactMessageDto messageDto = new ContactMessageDto();
			
			//Map<String, Object> oId = messageDb.getObjectId().getIdSnapshot();
			//Long iId = (Long)oId.get(TAnnouncementsQuery.ID_PK_COLUMN);

			//messageDto.setId(iId.intValue());
			messageDto.setDate(messageDb.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			messageDto.setSubject(messageDb.getSubject());
			messageDto.setBody(messageDb.getBody());
			
			//Map<String, Object> oUserId = messageDb.getSender().getObjectId().getIdSnapshot();
			//Long iUserId = (Long)oUserId.get(UsersQuery.ID_PK_COLUMN);

			//messageDto.setSender_id(iUserId.intValue());
			messageDto.setSender_name(messageDb.getSender().getName() + " " + messageDb.getSender().getLastname());
			
			lMessagesDto.add(messageDto);
			
		}
    	
		return lMessagesDto;
	}
	
	@GetMapping(path = "/announcements/getAnnouncements", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AnnouncementDto> getSupplierAnnouncements() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<TAnnouncementsQuery> lAnnouncementsDb = ObjectSelect.query(TAnnouncementsQuery.class).orderBy(TAnnouncementsQuery.DATE.desc()).select(context);
    	
    	List<AnnouncementDto> lAnnouncementsDto = new ArrayList<AnnouncementDto>();
    	
    	for (Iterator<TAnnouncementsQuery> iterator = lAnnouncementsDb.iterator(); iterator.hasNext();) {
    		TAnnouncementsQuery announcementDb = (TAnnouncementsQuery) iterator.next();
			
    		AnnouncementDto announcentsDto = new AnnouncementDto();
			
			announcentsDto.setDate(announcementDb.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			announcentsDto.setSubject(announcementDb.getSubject());
			announcentsDto.setBody(announcementDb.getBody());
			announcentsDto.setEstatus(announcementDb.getEstatus());
			if(announcementDb.getFileName() != null && !announcementDb.getFileName().trim().equals("")) {
				announcentsDto.setFile_name(announcementDb.getFileName());
				announcentsDto.setHas_file(true);
			}else {
				announcentsDto.setHas_file(false);
			}
			
			lAnnouncementsDto.add(announcentsDto);
			
		}
    	
		return lAnnouncementsDto;
	}
	
	@GetMapping(path = "/admin/getAnnouncements", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<AnnouncementDto> getAnnouncements() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<TAnnouncementsQuery> lAnnouncementsDb = ObjectSelect.query(TAnnouncementsQuery.class).orderBy(TAnnouncementsQuery.DATE.desc()).select(context);
    	
    	List<AnnouncementDto> lAnnouncementsDto = new ArrayList<AnnouncementDto>();
    	
    	for (Iterator<TAnnouncementsQuery> iterator = lAnnouncementsDb.iterator(); iterator.hasNext();) {
    		TAnnouncementsQuery announcementDb = (TAnnouncementsQuery) iterator.next();
			
    		AnnouncementDto announcentsDto = new AnnouncementDto();
			
			Map<String, Object> mIdAnnouncement = announcementDb.getObjectId().getIdSnapshot();
			Long idAnnouncement = (Long)mIdAnnouncement.get(TAnnouncementsQuery.ID_PK_COLUMN);

			announcentsDto.setId(idAnnouncement.intValue());
			announcentsDto.setDate(announcementDb.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			announcentsDto.setSubject(announcementDb.getSubject());
			announcentsDto.setBody(announcementDb.getBody());
			
			if(announcementDb.getFileName() != null && !announcementDb.getFileName().trim().equals("")) {
				announcentsDto.setFile_name(announcementDb.getFileName());
				announcentsDto.setHas_file(true);
			}else {
				announcentsDto.setHas_file(false);
			}
			
			lAnnouncementsDto.add(announcentsDto);
			
		}
    	
		return lAnnouncementsDto;
	}

	@GetMapping(path = "/admin/getAnnouncement", produces=MediaType.APPLICATION_JSON_VALUE)
	public AnnouncementDto getAnnouncementById(
				@RequestParam("id") int p_id
			) {
		ObjectContext context = cayenneRuntime.newContext(); 

    	ObjectId oId = new ObjectId("TAnnouncementsQuery", TAnnouncementsQuery.ID_PK_COLUMN, p_id);
    	
    	TAnnouncementsQuery announcementDb = SelectById.query(TAnnouncementsQuery.class, oId).selectOne(context);
    	
    	AnnouncementDto announcementDto = new AnnouncementDto();
    	
		announcementDto.setId(p_id);
		announcementDto.setDate(announcementDb.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		announcementDto.setSubject(announcementDb.getSubject());
		announcementDto.setBody(announcementDb.getBody());
		
		announcementDto.setFile_name(announcementDb.getFileName());
		
		return announcementDto;
	}

	@GetMapping(path = "/admin/getUsersCatalog", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<UserDto> getUsersCatalog() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<UsersQuery> lUsersDb = ObjectSelect.query(UsersQuery.class).orderBy(UsersQuery.NAME.asc()).select(context);
    	
    	List<UserDto> lUsersDto = new ArrayList<UserDto>();
    	
    	for (Iterator<UsersQuery> iterator = lUsersDb.iterator(); iterator.hasNext();) {
    		UsersQuery userDb = (UsersQuery) iterator.next();
			
			UserDto userDto = new UserDto();
			
			Map<String, Object> mId = userDb.getObjectId().getIdSnapshot();
			Long iId = (Long)mId.get(UsersQuery.ID_PK_COLUMN);
			userDto.setId(iId.intValue());
			
			userDto.setName((userDb.getName() + " " + userDb.getLastname()).trim());
			userDto.setEmail(userDb.getEmail());
			userDto.setUsername(userDb.getUsername());
			userDto.setEnabled(userDb.isEnabled());

			if(userDb.getTSuppliers() != null) {
				userDto.setSupplierName(userDb.getTSuppliers().getName());
			}
			
			lUsersDto.add(userDto);
			
		}
    	
		return lUsersDto;
	}

	@GetMapping(path = "/admin/getRolesCatalog", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<RoleDto> getRolesCatalog() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<CRolesId> lRolesDb = ObjectSelect.query(CRolesId.class).orderBy(CRolesTrans.NAME.asc()).select(context);
    	
    	List<RoleDto> lRoleDto = new ArrayList<RoleDto>();

		RoleDto roleDto0 = new RoleDto();
		roleDto0.setId(0);
		roleDto0.setName("[Seleccione Role]");
    	lRoleDto.add(roleDto0);
    	
    	for (Iterator<CRolesId> iterator = lRolesDb.iterator(); iterator.hasNext();) {
    		CRolesId roleDb = (CRolesId) iterator.next();
			
			RoleDto roleDto = new RoleDto();
			
			Map<String, Object> mId = roleDb.getObjectId().getIdSnapshot();
			Long iId = (Long)mId.get(CRolesId.ID_PK_COLUMN);
			roleDto.setId(iId.intValue());

    		String sRoleName = "";
    		List<CRolesTrans> lRoleTrans = roleDb.getCRolesTranss();
    		for (CRolesTrans cRoleTrans : lRoleTrans) {
    			String sLangIdSap = cRoleTrans.getCLanguagesId().getIdSap();
    			if(sLangIdSap.equals("ES")) {
    				sRoleName = cRoleTrans.getName();
    			}
			}
    				
    		roleDto.setName(sRoleName);
    		
    		lRoleDto.add(roleDto);
			
		}
    	
		return lRoleDto;
	}

	@GetMapping(path = "/admin/getSuppliersCatalog", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<SupplierDto> getSuppliersCatalog() {
		ObjectContext context = cayenneRuntime.newContext(); 
    	
    	List<TSuppliersQuery> lSuppliersDb = ObjectSelect.query(TSuppliersQuery.class).orderBy(TSuppliersQuery.NAME.asc()).select(context);
    	
    	List<SupplierDto> lSuppliersDto = new ArrayList<SupplierDto>();
    	
    	for (Iterator<TSuppliersQuery> iterator = lSuppliersDb.iterator(); iterator.hasNext();) {
    		TSuppliersQuery supplierDb = (TSuppliersQuery) iterator.next();
			
    		SupplierDto supplierDto = new SupplierDto();

			Map<String, Object> mId = supplierDb.getObjectId().getIdSnapshot();
			Long iId = (Long)mId.get(TSuppliersQuery.ID_PK_COLUMN);
			supplierDto.setId(iId.intValue());
			
    		supplierDto.setIdSap(supplierDb.getIdSap());
    		supplierDto.setName(supplierDb.getName());
    		
    		String sCountryId = supplierDb.getCCountriesId().getIdSap();
    		if(sCountryId.equals("MX")) {
    			supplierDto.setTaxId(supplierDb.getTaxIdMx());
    		}else {
    			supplierDto.setTaxId(supplierDb.getTaxIdOth());
    		}

    		String sCountryName = "";
    		List<CCountriesTrans> lCountryTrans = supplierDb.getCCountriesId().getCCountriesTranss();
    		for (CCountriesTrans cCountriesTrans : lCountryTrans) {
    			String sLangIdSap = cCountriesTrans.getCLanguagesId().getIdSap();
    			if(sLangIdSap.equals("ES")) {
    				sCountryName = cCountriesTrans.getName();
    			}
			}
    				
    		supplierDto.setCountry(sCountryName);
    		
    		lSuppliersDto.add(supplierDto);
			
		}
    	
		return lSuppliersDto;
	}

	@GetMapping(path = "/admin/getUser", produces=MediaType.APPLICATION_JSON_VALUE)
	public UserEditDto getUserById(
			@RequestParam("id") int p_id
		) {
		ObjectContext context = cayenneRuntime.newContext(); 
		
		UserEditDto userDto = new UserEditDto();
    	
    	ObjectId objectId = new ObjectId("UsersQuery", UsersQuery.ID_PK_COLUMN, p_id);
    	
    	UsersQuery userDb = SelectById.query(UsersQuery.class, objectId).selectOne(context);
    	
    	userDto.setId(p_id);
    	userDto.setName(userDb.getName());
    	userDto.setLastname(userDb.getLastname());
    	userDto.setEmail(userDb.getEmail());
    	userDto.setUsername(userDb.getUsername());
    	userDto.setEnabled(userDb.isEnabled());

		if(userDb.getTSuppliers() != null) {
			Map<String, Object> mId = userDb.getTSuppliers().getObjectId().getIdSnapshot();
			Long iId = (Long)mId.get(TSuppliersQuery.ID_PK_COLUMN);
			userDto.setSupplierId(iId.intValue());
			
			userDto.setSupplierName(userDb.getTSuppliers().getName());
		}

		if(userDb.getAuthoritieses() != null) {
			
			for (Iterator<AuthoritiesQuery> iterator = userDb.getAuthoritieses().iterator(); iterator.hasNext();) {
				AuthoritiesQuery auth = iterator.next();
				Map<String, Object> mId = auth.getCRolesId().getObjectId().getIdSnapshot();
				Long iId = (Long)mId.get(CRolesId.ID_PK_COLUMN);
				userDto.setRoleId(iId.intValue());
			}
			
		}
		
		return userDto;
		
	}
	
}
