package com.lebelier.suppliers.web.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lebelier.suppliers.db.cayenne.entities.TInvoicesQuery;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.dto.InvoicesStatusReportByBqmDto;
import com.lebelier.suppliers.db.dto.InvoicesStatusReportByCompanyDto;
import com.lebelier.suppliers.db.dto.InvoicesStatusReportByLbqDto;
import com.lebelier.suppliers.db.dto.InvoicesStatusReportDto;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.DB_QueryException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmInvoiceEstatus;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmInvoiceEstatusResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZtyInoviceStatus;

@Component
@RestController
public class ReportInvoicesStatusJSonController {

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;
	
    @Autowired
	private ServerRuntime cayenneRuntime;

	@GetMapping(path = "/report/InvoicesStatus", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InvoicesStatusReportByCompanyDto>> getInvoicesStatus(){
		String suppId = userSession.getSap_supplier_id();
		
		List<InvoicesStatusReportByCompanyDto> lstInvoices = new ArrayList<InvoicesStatusReportByCompanyDto>();

		List<InvoicesStatusReportDto> lstDb = new ArrayList<InvoicesStatusReportDto>();
		List<ZtyInoviceStatus> lstSap = new ArrayList<ZtyInoviceStatus>();

		List<InvoicesStatusReportDto> lstZmx1 = new ArrayList<InvoicesStatusReportDto>();
		List<InvoicesStatusReportDto> lstZmx2 = new ArrayList<InvoicesStatusReportDto>();
		
		try {
		
			try {
				lstDb = getInvoicesFromDb(suppId);
			}catch(Exception e) {
				throw new DB_QueryException(e);
			}
			
			try {
				lstSap = getInvoicesFromSap(suppId);
			}catch(Exception e) {
				throw new WebServiceSapException(e);
				
			}
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().header("process_result", e.getFrontResult()).header("process_msg", e.getFrontMessage()).body(lstInvoices);
		}
		
		
		for (Iterator<ZtyInoviceStatus> itSap = lstSap.iterator(); itSap.hasNext();) {
			ZtyInoviceStatus zSap = (ZtyInoviceStatus) itSap.next();
			
			InvoicesStatusReportDto iDto = new InvoicesStatusReportDto();
			
			for (InvoicesStatusReportDto iDb : lstDb) {
				
				if(zSap.getBukrs().equals(iDb.getsComp()) && 
						zSap.getEjercicio().equals(iDb.getsYear()) &&
						zSap.getNoSap().equals(iDb.getsDoc())) {
					
					iDto.setStatus(iDb.getStatus());
					
					continue;
					
				}
                                iDto.setStatus(iDb.getStatus());
				
			}
			
			iDto.setAmount(zSap.getImporte() + "");
			iDto.setRecDate(zSap.getFechaRecepcion());
                        iDto.setPayDate(zSap.getFechaPago());
			iDto.setCurrency(zSap.getMoneda());
			iDto.setReference(zSap.getReferencia());
                        iDto.setStatus(iDto.getStatus());
			//iDto.setStatus(zSap.getStatus());
			//iDto.setUuid(zSap.getgetUuid());
			iDto.setCompany(zSap.getBukrs().equals("ZMX1") ? "LBQ" : zSap.getBukrs().equals("ZMX2") ? "BQM" : "");
			
			iDto.setsYear(zSap.getEjercicio());
			iDto.setsDoc(zSap.getNoSap());
			iDto.setsComp(zSap.getBukrs());

			if(iDto.getCompany().equals("LBQ")) {
				lstZmx1.add(iDto);
			}else if(iDto.getCompany().equals("BQM")) {
				lstZmx2.add(iDto);
			}
			
			
		}
		
		InvoicesStatusReportByLbqDto dtoZmx1 = new InvoicesStatusReportByLbqDto();
		dtoZmx1.setLbq(lstZmx1);

		InvoicesStatusReportByBqmDto dtoZmx2 = new InvoicesStatusReportByBqmDto();
		dtoZmx2.setBqm(lstZmx2);
		
		lstInvoices.add(dtoZmx1);
		lstInvoices.add(dtoZmx2);
		
		return ResponseEntity.ok().body(lstInvoices);
		
	}
	
	private List<InvoicesStatusReportDto> getInvoicesFromDb(String p_supplierId){
		
        List<InvoicesStatusReportDto> lstDto = new ArrayList<InvoicesStatusReportDto>();

        ObjectContext ctxCayenne = cayenneRuntime.newContext(); 

        TSuppliersQuery tSupp = ObjectSelect.query(TSuppliersQuery.class, TSuppliersQuery.ID_SAP.eq(p_supplierId)).selectOne(ctxCayenne);
    	
    	List<TInvoicesQuery> lInvoices = ObjectSelect.query(TInvoicesQuery.class, TInvoicesQuery.SUPPLIER.eq(tSupp)).orderBy(TInvoicesQuery.CREATED_ON.asc()).select(ctxCayenne);
    	
    	for (Iterator<TInvoicesQuery> iterator = lInvoices.iterator(); iterator.hasNext();) {
    		TInvoicesQuery oDb = (TInvoicesQuery) iterator.next();
			
    		InvoicesStatusReportDto oDto = new InvoicesStatusReportDto();
		
    		oDto.setAmount(oDb.getAmount() + "");
    		oDto.setRecDate(oDb.getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    		//oDto.setPayDate(oDb.get);
    		oDto.setCurrency(oDb.getCurrency());
			oDto.setReference(oDb.getReference());
                        String estatus=""; 
                       
                        
                        System.out.println(" "+oDb.getStatus());
			oDto.setStatus(oDb.getStatus().getName());
                        
			oDto.setUuid(oDb.getUuid());
			oDto.setCompany(oDb.getCompany().getIdSap());
			
			oDto.setsYear(oDb.getSapFiscalYear());
			oDto.setsDoc(oDb.getSapPrelimDocNum());
			oDto.setsComp(oDb.getSapCompany());
			
			lstDto.add(oDto);
			
		}
    	
		return lstDto;
		
	}

	private List<ZtyInoviceStatus> getInvoicesFromSap(String p_supplierId){

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
        
		ZMxfmInvoiceEstatus req = new ZMxfmInvoiceEstatus();
		req.setGjahr(LocalDate.now().getYear() + "");
		req.setLifnr(p_supplierId);
		
		ZMxfmInvoiceEstatusResponse wsResp = client.executeZMxfmInvoiceEstatus(
    			req,
    			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
    		);
    	
		List<ZtyInoviceStatus> respInvoices = wsResp.getItFacturas().getItem();
		
		context.close();
    	
		return respInvoices;
		
	}
	
	
}
