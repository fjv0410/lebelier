package com.lebelier.suppliers.web.controller;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lebelier.cfdi.comprobante.xml.entities.Comprobante;
import com.lebelier.cfdi.comprobante.xml.entities.Comprobante.Impuestos.Retenciones.Retencion;
import com.lebelier.suppliers.db.cayenne.entities.CCompanies;
import com.lebelier.suppliers.db.cayenne.entities.TInvoicesInsert;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.utils.InvoiceValidator;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UploadFilesArraySession;
import com.lebelier.suppliers.web.entity.UploadFilesSession;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.CfdiXmlParsingException;
import com.lebelier.suppliers.web.exception.DB_InsertException;
import com.lebelier.suppliers.web.exception.FileSaveFileException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.ValidationDocumentTypeException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.ValidationWebServiceSatException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.web.exception.WebServiceSatException;
import com.lebelier.suppliers.ws.client.exec.ConsultaCFDIServiceClient;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.Bapiret2;
import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60Cxp;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryFv60CxpResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmPreliminaryPostingFb01Response;
import com.lebelier.suppliers.ws.sap.client.types.ZttRetenciones;
import com.lebelier.suppliers.ws.sat.client.types.Acuse;
import com.lebelier.suppliers.ws.sat.client.types.ConsultaResponse;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
@Controller
public class InvoicesWithoutDeliveriesController {

    @Resource
    private Environment environment;

    @Autowired
    private UserSession userSession;

    @Autowired
    private ServerRuntime cayenneRuntime;

    @Autowired
    private UploadFilesArraySession filesSession;

    @GetMapping("/payableAccounts/InvoicesWithoutDeliveries")
    public String goInvoicesWithoutDeliveriesPA() {
        return this.goInvoicesWithoutDeliveriesMx();
    }

    @RequestMapping(value = "/payableAccounts/invoiceWithoutDelivery")
    public String processInvoiceWithoutNumberPA(
            HttpServletResponse resp,HttpServletRequest request
    ) {

        return this.processInvoiceWithoutNumber(resp, true, request);
    }

    @GetMapping("/invoice/InvoicesWithoutDeliveries")
    public String goInvoicesWithoutDeliveriesMx() {
        filesSession.initialize();
        return "InvoicesWithoutDeliveries";
    }

    @RequestMapping(value = "/invoice/invoiceWithoutDelivery")
    public String processInvoiceWithoutNumberMX(
            HttpServletResponse resp, HttpServletRequest request
    ) {

        return this.processInvoiceWithoutNumber(resp, false, request);

    }

    private String processInvoiceWithoutNumber(
            HttpServletResponse resp,
            boolean isPayableAccount,HttpServletRequest request
    ) {
        HttpSession miSession= (HttpSession) request.getSession();
        UserSession usuario=(UserSession) miSession.getAttribute("scopedTarget.userSession");
        userSession.getRoleId();
        String sReturnTo = "InvoicesWithoutDeliveries";
        if(usuario.getRoleId()==3){
            this.enviaSapDirectoCuentas(resp, isPayableAccount);
        }
        else {
        

        UploadFilesSession[] aFiles = filesSession.getUploadFilesSession();

        boolean hasErrors = true;
        int iCnt = 0;

        for (UploadFilesSession files : aFiles) {

            if (files == null || files.getPdfFile() == null || files.getXmlFile() == null) {
                continue;
            }

            MultipartFile mpXml = files.getXmlFile();
            MultipartFile mpPdf = files.getPdfFile();

            String sTasaOCuota = "";
            String sUuid = "";
            String sFechaCfdi = "";
            String sTipoComprobante = "";
            String sRfcReceptor = "";
            String sFolioCfdi = "";
            String sMonedaCfdi = "";
            String sUsoCfdi = "";
            String sRfcEmisor = "";
            BigDecimal bdTotal = new BigDecimal(0);
            BigDecimal bdTotalRetenciones = new BigDecimal(0);
            String sFormaPago = "";
            String sMetodoPago = "";

            String sSerie = "";

            String sNumProSAP = userSession.getSap_supplier_id();

            String sFechaContab = "";

            BigDecimal bdTpoCambio = new BigDecimal(0);

            try {
                //(new XsdValidator()).validateComprobanteUsingXsd(mpXml.getOriginalFilename(), mpXml.getInputStream());

                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);

                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    Comprobante comprobante = (Comprobante) unmarshaller.unmarshal(mpXml.getInputStream());

                    sRfcReceptor = comprobante.getReceptor().getRfc();
                    sFolioCfdi = comprobante.getFolio();
                    sMonedaCfdi = comprobante.getMoneda().name();

                    sUsoCfdi = comprobante.getReceptor().getUsoCFDI().value();

                    sRfcEmisor = comprobante.getEmisor().getRfc();

                    bdTotal = comprobante.getTotal();

                    sFormaPago = comprobante.getFormaPago();

                    sFechaCfdi = comprobante.getFecha().toString();
                    sFechaCfdi = sFechaCfdi.substring(0, sFechaCfdi.indexOf("T"));

                    GregorianCalendar cal = new GregorianCalendar();
                    sFechaContab = cal.get(Calendar.YEAR) + "-" + String.format("%02d", cal.get(Calendar.MONTH)) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));

                    sTipoComprobante = comprobante.getTipoDeComprobante().toString();

                    List<Object> lCompl = comprobante.getComplemento().get(0).getAny();

                    for (Object object : lCompl) {
                        ElementNSImpl element = (ElementNSImpl) object;
                        String sName = element.getLocalName();
                        if (sName.equals("TimbreFiscalDigital")) {
                            sUuid = element.getAttribute("UUID");
                        }
                    }
                    
                        if (comprobante.getConceptos().getConcepto().get(0).getImpuestos() != null) {
                            sTasaOCuota = comprobante.getConceptos().getConcepto().get(0).getImpuestos().getTraslados().getTraslado().get(0).getTasaOCuota().toString();
                        }

                        if (comprobante.getMetodoPago() != null) {
                            sMetodoPago = comprobante.getMetodoPago().value();
                        }

                        if (comprobante.getImpuestos() != null && comprobante.getImpuestos().getRetenciones() != null
                                && comprobante.getImpuestos().getRetenciones().getRetencion() != null) {

                            List<Retencion> retenciones = comprobante.getImpuestos().getRetenciones().getRetencion();

                            for (Retencion retencion : retenciones) {
                                bdTotalRetenciones = bdTotalRetenciones.add(retencion.getImporte());
                            }

                        }

                        bdTpoCambio = comprobante.getTipoCambio();
                        if (bdTpoCambio == null) {
                            bdTpoCambio = new BigDecimal(1);
                        }

                        sSerie = comprobante.getSerie();
                    
                } catch (Exception e) {
                    e.printStackTrace();

                    throw new CfdiXmlParsingException(mpXml.getOriginalFilename());
                }

                if (sUuid.equals("")) {
                    throw new ValidationEmptyValueException("UUID");
                }

                InvoiceValidator.existUuid(cayenneRuntime, sUuid);

                if (!sTipoComprobante.equals("P")) {
                    if (!sTipoComprobante.trim().equals("I") && !sTipoComprobante.trim().equals("E")) {
                        throw new ValidationDocumentTypeException(mpXml.getOriginalFilename(), sTipoComprobante);
                    }
                }

                String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_ARCH_PATH) + File.separator + "INV_" + sUuid;

                try {
                    mpPdf.transferTo(new File(sFilePath + ".pdf"));
                } catch (Exception e) {
                    e.printStackTrace();

                    throw new FileSaveFileException(mpPdf.getOriginalFilename());
                }

                TableOfBapiret2 tBapiret2 = new TableOfBapiret2();
                String sStatus = "";
                Date dateCfdi=new SimpleDateFormat("yyyy-MM-dd").parse(sFechaCfdi); 
                Date dateContab=new SimpleDateFormat("yyyy-MM-dd").parse(sFechaContab);
                
                List<Bapiret2> lBapiret2 = tBapiret2.getItem();

                resp.setHeader("process_msg_lst_" + iCnt++, "Factura ingresada a revision para proceso de pago: " + mpXml.getOriginalFilename() );
                ObjectContext cayenneContext = cayenneRuntime.newContext();

                CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.TAXID.eq(sRfcReceptor)).selectOne(cayenneContext);
                Long iIdCompany = (Long) db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);

                TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
                Long iIdSupplier = (Long) db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);

                LocalDate lDate = LocalDate.of(Integer.parseInt(sFechaCfdi.substring(0, 4)), Integer.parseInt(sFechaCfdi.substring(5, 7)), Integer.parseInt(sFechaCfdi.substring(8, 10)));
              
                TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
                db_insert.setReference(sFolioCfdi);
                db_insert.setAmount(bdTotal.doubleValue());
                db_insert.setCurrency(sMonedaCfdi);
                db_insert.setUuid(sUuid);
                db_insert.setDocType(sTipoComprobante);
                db_insert.setCompany_id(iIdCompany.intValue());
                db_insert.setSupplier_id(iIdSupplier.intValue());
                db_insert.setInvoiceDate(lDate);
                db_insert.setStatus_id(1);
                db_insert.setCreatedOn(LocalDate.now());
                db_insert.setWithDelivery(false);
                db_insert.setSapPrelimDocNum("");
                db_insert.setSapFiscalYear("");
                db_insert.setSapCompany("");
                db_insert.setRfcEmisor(sRfcEmisor);
                db_insert.setRfcReceptor(sRfcReceptor);
                db_insert.setTipoFactura("2");
                db_insert.setTipoProveedor("1");
                db_insert.setTipoCfdi(sTipoComprobante);
                  
                db_insert.setFechaCfdi(dateCfdi);
                db_insert.setFechaContab(dateContab);
                db_insert.setFolioCfdi(sFolioCfdi);
                db_insert.setTasa(sTasaOCuota);
                db_insert.setUsoCfdi(sUsoCfdi);
                db_insert.setFormaPago(sFormaPago);
                db_insert.setMetodoPago(sMetodoPago);
                db_insert.setTotalImporte(bdTotalRetenciones.doubleValue());
                db_insert.setTipoCambio(bdTpoCambio.toString());
                db_insert.setSerie(sSerie);
                db_insert.setNoProvedorSap(sNumProSAP);
                db_insert.setNoProveedorSap(sNumProSAP);
                
               /* for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext();) {
                    Bapiret2 bapiret2 = iterator.next();
                    if (bapiret2.getType().equals("S")) {
                        String sBapiretId = bapiret2.getId();
                        db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
                        db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
                        db_insert.setSapCompany(sBapiretId.substring(0, 4));
                    }

                }*/

                try {
                    cayenneContext.commitChanges();
                } catch (Exception e) {
                    throw new DB_InsertException(e);
                }

                filesSession.initialize();

                hasErrors = false;

            } catch (SuppliersGeneralException e) {
                e.printStackTrace();

                resp.setHeader("process_result", e.getFrontResult());
                resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());

            } catch (Exception e) {
                e.printStackTrace();

                resp.setHeader("process_result", "Error");
                resp.setHeader("process_msg_lst" + iCnt++, "Sucedió un error.");

            }

        }
        }

        return sReturnTo;

    }

    @PostMapping(value = "/payableAccounts/uploadXmlWithoutDeliveries")
    @ResponseBody
    public String uploadXmlWithoutDeliveriesPA(
            @RequestParam(required = true) MultipartFile xmlFile,
            @RequestParam(value = "indXml", required = true) int index,
            HttpServletResponse resp
    ) {

        return uploadXmlWithoutDeliveries(xmlFile, index, resp);

    }

    @PostMapping(value = "/payableAccounts/uploadPdfWithoutDeliveries")
    @ResponseBody
    public String uploadPdfWithoutDeliveriesPA(
            @RequestParam(required = true) MultipartFile pdfFile,
            @RequestParam(value = "indPdf", required = true) int index,
            HttpServletResponse resp
    ) {

        return uploadPdfWithoutDeliveries(pdfFile, index, resp);

    }

    @PostMapping(value = "/invoice/uploadXmlWithoutDeliveries")
    @ResponseBody
    public String uploadXmlWithoutDeliveries(
            @RequestParam(required = true) MultipartFile xmlFile,
            @RequestParam(value = "indXml", required = true) int index,
            HttpServletResponse resp
    ) {

        filesSession.getUploadFilesSession()[index].setXmlFile(xmlFile);

        return "InvoicesWithoutDeliveries";

    }

    @PostMapping(value = "/invoice/uploadPdfWithoutDeliveries")
    @ResponseBody
    public String uploadPdfWithoutDeliveries(
            @RequestParam(required = true) MultipartFile pdfFile,
            @RequestParam(value = "indPdf", required = true) int index,
            HttpServletResponse resp
    ) {

        filesSession.getUploadFilesSession()[index].setPdfFile(pdfFile);

        return "InvoicesWithoutDeliveries";

    }

    @PostMapping(value = "/payableAccounts/removeFile")
    @ResponseBody
    public String removeFilePA(
            @RequestParam(value = "ind", required = true) int index,
            HttpServletResponse resp
    ) {

        return removeFileMX(index, resp);

    }

    @PostMapping(value = "/invoice/removeFile")
    @ResponseBody
    public String removeFileMX(
            @RequestParam(value = "ind", required = true) int index,
            HttpServletResponse resp
    ) {

        filesSession.getUploadFilesSession()[index].setPdfFile(null);
        filesSession.getUploadFilesSession()[index].setXmlFile(null);

        return "OK.";

    }
    
    public String enviaSapDirectoCuentas(HttpServletResponse resp,
            boolean isPayableAccount){
        String sReturnTo = "InvoicesWithoutDeliveries";

        UploadFilesSession[] aFiles = filesSession.getUploadFilesSession();

        boolean hasErrors = true;
        int iCnt = 0;

        for (UploadFilesSession files : aFiles) {

            if (files == null || files.getPdfFile() == null || files.getXmlFile() == null) {
                continue;
            }

            MultipartFile mpXml = files.getXmlFile();
            MultipartFile mpPdf = files.getPdfFile();

            String sTasaOCuota = "";
            String sUuid = "";
            String sFechaCfdi = "";
            String sTipoComprobante = "";
            String sRfcReceptor = "";
            String sFolioCfdi = "";
            String sMonedaCfdi = "";
            String sUsoCfdi = "";
            String sRfcEmisor = "";
            BigDecimal bdTotal = new BigDecimal(0);
            BigDecimal bdTotalRetenciones = new BigDecimal(0);
            String sFormaPago = "";
            String sMetodoPago = "";

            String sSerie = "";

            String sNumProSAP = userSession.getSap_supplier_id();

            String sFechaContab = "";

            BigDecimal bdTpoCambio = new BigDecimal(0);

            try {
                //(new XsdValidator()).validateComprobanteUsingXsd(mpXml.getOriginalFilename(), mpXml.getInputStream());

                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);

                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    Comprobante comprobante = (Comprobante) unmarshaller.unmarshal(mpXml.getInputStream());

                    sRfcReceptor = comprobante.getReceptor().getRfc();
                    sFolioCfdi = comprobante.getFolio();
                    sMonedaCfdi = comprobante.getMoneda().name();

                    sUsoCfdi = comprobante.getReceptor().getUsoCFDI().value();

                    sRfcEmisor = comprobante.getEmisor().getRfc();

                    bdTotal = comprobante.getTotal();

                    sFormaPago = comprobante.getFormaPago();

                    sFechaCfdi = comprobante.getFecha().toString();
                    sFechaCfdi = sFechaCfdi.substring(0, sFechaCfdi.indexOf("T"));

                    GregorianCalendar cal = new GregorianCalendar();
                    sFechaContab = cal.get(Calendar.YEAR) + "-" + String.format("%02d", cal.get(Calendar.MONTH)) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));

                    sTipoComprobante = comprobante.getTipoDeComprobante().toString();

                    List<Object> lCompl = comprobante.getComplemento().get(0).getAny();

                    for (Object object : lCompl) {
                        ElementNSImpl element = (ElementNSImpl) object;
                        String sName = element.getLocalName();
                        if (sName.equals("TimbreFiscalDigital")) {
                            sUuid = element.getAttribute("UUID");
                        }
                    }
                    
                        if (comprobante.getConceptos().getConcepto().get(0).getImpuestos() != null) {
                            sTasaOCuota = comprobante.getConceptos().getConcepto().get(0).getImpuestos().getTraslados().getTraslado().get(0).getTasaOCuota().toString();
                        }

                        if (comprobante.getMetodoPago() != null) {
                            sMetodoPago = comprobante.getMetodoPago().value();
                        }

                        if (comprobante.getImpuestos() != null && comprobante.getImpuestos().getRetenciones() != null
                                && comprobante.getImpuestos().getRetenciones().getRetencion() != null) {

                            List<Retencion> retenciones = comprobante.getImpuestos().getRetenciones().getRetencion();

                            for (Retencion retencion : retenciones) {
                                bdTotalRetenciones = bdTotalRetenciones.add(retencion.getImporte());
                            }

                        }

                        bdTpoCambio = comprobante.getTipoCambio();
                        if (bdTpoCambio == null) {
                            bdTpoCambio = new BigDecimal(1);
                        }

                        sSerie = comprobante.getSerie();
                    
                } catch (Exception e) {
                    e.printStackTrace();

                    throw new CfdiXmlParsingException(mpXml.getOriginalFilename());
                }

                if (sUuid.equals("")) {
                    throw new ValidationEmptyValueException("UUID");
                }

                InvoiceValidator.existUuid(cayenneRuntime, sUuid);

                if (!sTipoComprobante.equals("P")) {
                    if (!sTipoComprobante.trim().equals("I") && !sTipoComprobante.trim().equals("E")) {
                        throw new ValidationDocumentTypeException(mpXml.getOriginalFilename(), sTipoComprobante);
                    }
                }

                AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
                ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);

                ConsultaCFDIServiceClient satClient = context.getBean(ConsultaCFDIServiceClient.class);

                ConsultaResponse consultaResp;

                try {
                    consultaResp = satClient.executeConsulta(environment.getProperty(PropetiesConstants.WS_SAT_UUID_WSDL),
                            environment.getProperty(PropetiesConstants.WS_SAT_UUID_ACTION),
                            sRfcEmisor,
                            sRfcReceptor,
                            bdTotal + "",
                            sUuid);
                } catch (WebServiceSatException e) {
                    e.printStackTrace();
                    context.close();

                    throw new WebServiceSatException(e);
                }

                Acuse acuse = consultaResp.getConsultaResult().getValue();
                String codigoEstatus = acuse.getCodigoEstatus().getValue();

                if (codigoEstatus.startsWith("N")) {
                    context.close();
                    throw new ValidationWebServiceSatException(sUuid, codigoEstatus);
                }

                String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_ARCH_PATH) + File.separator + "INV_" + sUuid;

                try {
                    mpXml.transferTo(new File(sFilePath + ".xml"));
                } catch (Exception e) {
                    e.printStackTrace();
                    context.close();

                    throw new FileSaveFileException(mpXml.getOriginalFilename());
                }

                try {
                    mpPdf.transferTo(new File(sFilePath + ".pdf"));
                } catch (Exception e) {
                    e.printStackTrace();
                    context.close();

                    throw new FileSaveFileException(mpPdf.getOriginalFilename());
                }

                TableOfBapiret2 tBapiret2 = new TableOfBapiret2();
                String sStatus = "";
                if (isPayableAccount) {
                    ZmxfmPreliminaryFv60Cxp wsReq = new ZmxfmPreliminaryFv60Cxp();

                    wsReq.setPUuid(sUuid);
                    wsReq.setPRfcReceptor(sRfcReceptor);
                    wsReq.setPTipoCfdi(sTipoComprobante);
                    wsReq.setPFechaCfdi(sFechaCfdi);
                    wsReq.setPRfcEmisor(sRfcEmisor);
                    wsReq.setPFechaContab(sFechaContab);
                    wsReq.setPFolioCfdi(sFolioCfdi);
                    wsReq.setPTotal(bdTotal);
                    wsReq.setPMonedaCfdi(sMonedaCfdi);
                    wsReq.setPTasaOCuota(sTasaOCuota);
                    wsReq.setPUsocfdi(sUsoCfdi);
                    wsReq.setPFormapago(sFormaPago);
                    wsReq.setPNosapProv(sNumProSAP);

                    wsReq.setPMetodopago(sMetodoPago);

                    wsReq.setPTotalImpRet(bdTotalRetenciones);
                    wsReq.setPTipocambio(bdTpoCambio);

                    wsReq.setPSerie(sSerie);

                    ZttRetenciones tRetenciones = new ZttRetenciones();
                    wsReq.setTRetencion(tRetenciones);

                    wsReq.setBapiret2(new TableOfBapiret2());

                    ZmxfmPreliminaryFv60CxpResponse wsResp;
                    try {
                        wsResp = client.executeZmxfmPreliminaryFv60Cxp(
                                wsReq,
                                environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
                        );

                    } catch (Exception e) {
                        e.printStackTrace();
                        context.close();

                        throw new WebServiceSapException(e);
                    }

                    tBapiret2 = wsResp.getBapiret2();
                    sStatus = wsResp.getRStatus();

                } else {

                    ZmxfmPreliminaryPostingFb01 wsReq = new ZmxfmPreliminaryPostingFb01();

                    wsReq.setPUuid(sUuid);
                    wsReq.setPRfcReceptor(sRfcReceptor);
                    wsReq.setPTipoCfdi(sTipoComprobante);
                    wsReq.setPFechaCfdi(sFechaCfdi);
                    wsReq.setPRfcEmisor(sRfcEmisor);
                    wsReq.setPFechaContab(sFechaContab);
                    wsReq.setPFolioCfdi(sFolioCfdi);
                    wsReq.setPTotal(bdTotal);
                    wsReq.setPMonedaCfdi(sMonedaCfdi);
                    wsReq.setPTasaOCuota(sTasaOCuota);
                    wsReq.setPUsocfdi(sUsoCfdi);
                    wsReq.setPFormapago(sFormaPago);
                    wsReq.setPNosapProv(sNumProSAP);

                    wsReq.setPMetodopago(sMetodoPago);

                    wsReq.setPTotalImpRet(bdTotalRetenciones);

                    wsReq.setPTipocambio(bdTpoCambio);

                    wsReq.setPSerie(sSerie);

                    ZttRetenciones tRetenciones = new ZttRetenciones();
                    wsReq.setTRetenciones(tRetenciones);

                    wsReq.setBapiret2(new TableOfBapiret2());

                    ZmxfmPreliminaryPostingFb01Response wsResp;
                    try {
                        wsResp = client.executeZmxfmPreliminaryPostingFb01(
                                wsReq,
                                environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
                        );

                    } catch (Exception e) {
                        e.printStackTrace();
                        context.close();

                        throw new WebServiceSapException(e);
                    }

                    tBapiret2 = wsResp.getBapiret2();
                    sStatus = wsResp.getRStatus();

                }

                List<Bapiret2> lBapiret2 = tBapiret2.getItem();

                context.close();

                switch (sStatus) {
                    case "00":

                        resp.setHeader("process_msg_lst_" + iCnt++, "Factura ingresada a revision para proceso de pago: " + mpXml.getOriginalFilename());

                        ObjectContext cayenneContext = cayenneRuntime.newContext();

                        CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.TAXID.eq(sRfcReceptor)).selectOne(cayenneContext);
                        Long iIdCompany = (Long) db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);

                        TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
                        Long iIdSupplier = (Long) db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);

                        LocalDate lDate = LocalDate.of(Integer.parseInt(sFechaCfdi.substring(0, 4)), Integer.parseInt(sFechaCfdi.substring(5, 7)), Integer.parseInt(sFechaCfdi.substring(8, 10)));

                        TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
                        db_insert.setReference(sFolioCfdi);
                        db_insert.setAmount(bdTotal.doubleValue());
                        db_insert.setCurrency(sMonedaCfdi);
                        db_insert.setUuid(sUuid);
                        db_insert.setDocType(sTipoComprobante);
                        db_insert.setCompany_id(iIdCompany.intValue());
                        db_insert.setSupplier_id(iIdSupplier.intValue());
                        db_insert.setInvoiceDate(lDate);
                        db_insert.setStatus_id(1);
                        db_insert.setCreatedOn(LocalDate.now());
                        db_insert.setWithDelivery(false);

                        for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext();) {
                            Bapiret2 bapiret2 = iterator.next();
                            if (bapiret2.getType().equals("S")) {
                                String sBapiretId = bapiret2.getId();
                                db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
                                db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
                                db_insert.setSapCompany(sBapiretId.substring(0, 4));
                            }

                        }

                        try {
                            cayenneContext.commitChanges();
                        } catch (Exception e) {
                            throw new DB_InsertException(e);
                        }

                        filesSession.initialize();

                        hasErrors = false;

                        break;
                    case "11":
                        resp.setHeader("process_msg_lst_" + iCnt++, "No es posible procesar facturas de años anteriores: " + mpXml.getOriginalFilename());
                        break;
                    case "12":
                        resp.setHeader("process_msg_lst_" + iCnt++, "La fecha de emisión de la factura no puede ser anterior a 30 días: " + mpXml.getOriginalFilename());
                        break;
                    case "15":
                        resp.setHeader("process_msg_lst_" + iCnt++, "RFC Receptor no corresponde a alguna de nuestras empresas: " + mpXml.getOriginalFilename());
                        break;
                    case "13":
                        resp.setHeader("process_msg_lst_" + iCnt++, "RFC Emisor no corresponde con proveedor actual: " + mpXml.getOriginalFilename());
                        break;
                    case "14":
                        resp.setHeader("process_msg_lst_" + iCnt++, "El proveedor actualo no se encuentra regitrado en SAP: " + mpXml.getOriginalFilename());
                        break;
                    default:
                        for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext();) {
                            Bapiret2 bapiret2 = iterator.next();

                            resp.setHeader("process_msg_lst_" + iCnt++, bapiret2.getMessage().concat(" [").concat(bapiret2.getId().concat("]")));

                        }

                        break;

                }
            } catch (SuppliersGeneralException e) {
                e.printStackTrace();

                resp.setHeader("process_result", e.getFrontResult());
                resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());

            } catch (Exception e) {
                e.printStackTrace();

                resp.setHeader("process_result", "Error");
                resp.setHeader("process_msg_lst" + iCnt++, "Sucedió un error.");

            }

        }

        if (hasErrors) {
            resp.setHeader("process_result", "Error");
        } else {
            resp.setHeader("process_result", "Success");
        }

        return sReturnTo;
    }

}
