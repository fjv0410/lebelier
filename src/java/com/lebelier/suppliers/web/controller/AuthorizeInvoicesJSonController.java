package com.lebelier.suppliers.web.controller;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lebelier.suppliers.db.cayenne.entities.CInvoiceStatus;
import com.lebelier.suppliers.db.cayenne.entities.TInvoicesQuery;
import com.lebelier.suppliers.db.dto.InvoicesStatusReportDto;

@Component
@RestController
public class AuthorizeInvoicesJSonController {
	
    @Autowired
	private ServerRuntime cayenneRuntime;

	@GetMapping(path = "/payableAccounts/InvoicesToAuthorize", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<InvoicesStatusReportDto> getMessagesByUser() {

		List<InvoicesStatusReportDto> lstDto = new ArrayList<InvoicesStatusReportDto>();
		
		ObjectContext ctxCayenne = cayenneRuntime.newContext(); 
		
		ObjectId oInvoiceId = new ObjectId("CInvoiceStatus", CInvoiceStatus.ID_PK_COLUMN, 1);
		
		CInvoiceStatus invoiceStatus = new CInvoiceStatus();
		invoiceStatus.setObjectId(oInvoiceId);

		List<TInvoicesQuery> lstInvoices = ObjectSelect.query(TInvoicesQuery.class, TInvoicesQuery.STATUS.eq(invoiceStatus).andExp(TInvoicesQuery.WITH_DELIVERY.eq(false))).select(ctxCayenne);
    	
                for (Iterator<TInvoicesQuery> iterator = lstInvoices.iterator(); iterator.hasNext();) {
                    TInvoicesQuery oDb = (TInvoicesQuery) iterator.next();


                    Long oDtoId = (Long)oDb.getObjectId().getIdSnapshot().get(CInvoiceStatus.ID_PK_COLUMN);


                    InvoicesStatusReportDto oDto = new InvoicesStatusReportDto();

                    oDto.setId(oDtoId.intValue());
                    oDto.setCompany(oDb.getCompany().getIdSap());
                    oDto.setReference(oDb.getReference());
                    oDto.setRecDate(oDb.getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                    oDto.setAmount(oDb.getAmount() + "");
                    oDto.setCurrency(oDb.getCurrency());
                    oDto.setDocSap(oDb.getSapPrelimDocNum());
                    oDto.setNumProveedor(oDb.getSupplier().getIdSap());
                            lstDto.add(oDto);
                 
                 
                
                          
		}
    	
		return lstDto;
		
	}
	
}
