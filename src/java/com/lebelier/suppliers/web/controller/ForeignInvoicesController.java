package com.lebelier.suppliers.web.controller;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.ObjectContext;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.ObjectSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.lebelier.suppliers.db.cayenne.entities.CCompanies;
import com.lebelier.suppliers.db.cayenne.entities.TInvoicesInsert;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UploadFilesSession;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.DB_InsertException;
import com.lebelier.suppliers.web.exception.FileSaveFileException;
import com.lebelier.suppliers.web.exception.SuppliersCustomException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.ValidationAmountException;
import com.lebelier.suppliers.web.exception.ValidationDateException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.Bapiret2;
import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesExt;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmDeliveriesExtResponse;
import javax.annotation.PostConstruct;

@Controller
public class ForeignInvoicesController  extends WebServiceGatewaySupport{

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;
	
	@Autowired
	private UploadFilesSession filesSession;

        @Autowired
	private ServerRuntime cayenneRuntime;
    
        @PostConstruct
        public void revisaMensajes(){
            
        }

	@GetMapping("/foreign/ForeignInvoices")
	public String goForeignInvoices() {
		filesSession.setPdfFile(null);
		return "ForeignInvoices";
	}

	@GetMapping("/foreign/ForeignInvoicesWoDelivery")
	public String goForeignInvoicesWoDelivery() {
		filesSession.setPdfFile(null);
		return "ForeignInvoicesWoDelivery";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/foreign/foreignInvoices")
	public String processForeignInvoices(
			@RequestParam("documentType") String p_documentType, 
			@RequestParam("companyCode") String p_companyCode, 
			@RequestParam("deliveryNumber") String p_deliveryNumber, 
			@RequestParam("invoiceNumber") String p_invoiceNumber, 
			@RequestParam("invoiceDate") String p_invoiceDate, 
			@RequestParam("documentDate") String p_documentDate, 
			@RequestParam("invoiceAmount") String p_invoiceAmount, 
			@RequestParam("invoiceDescription") String p_invoiceDescr, 
                        @RequestParam("coinType") String p_coinType,
			HttpServletResponse resp
		) {
		
		String sReturnTo = "ForeignInvoices";
		int iCnt = 0;
		
		try {
			                 System.out.println("Conin "+p_coinType);
			validateValues(p_documentType, p_companyCode, p_deliveryNumber, p_invoiceNumber, 
						   p_invoiceDate, p_documentDate, p_invoiceAmount, p_invoiceDescr);
			
			BigDecimal bdImporte = new BigDecimal(p_invoiceAmount);
	
			MultipartFile mpPdf = filesSession.getPdfFile();
			String sNewFileName = userSession.getSap_supplier_id() + "_" + p_documentType + "_" + p_invoiceNumber + "_" + p_deliveryNumber + ".pdf";
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_FOREIGN) + File.separator + sNewFileName;

            ZMxfmDeliveriesExt wsReq = new ZMxfmDeliveriesExt();
            
            wsReq.setPEntrega(p_deliveryNumber);
            wsReq.setPSociedad(p_companyCode);
            wsReq.setPFechaEntrega(p_invoiceDate);
            wsReq.setPImporte(bdImporte);
            wsReq.setPConcepto(p_invoiceDescr);
            wsReq.setPFacturaNota(p_invoiceNumber);
            wsReq.setPOpcionFN(p_documentType.equals("F") ? "X" : "");
            wsReq.setPNosapProv(userSession.getSap_supplier_id());
            wsReq.setPMoneda(p_coinType);
            wsReq.setBapiret2(new TableOfBapiret2());
    		
            AnnotationConfigApplicationContext contextWs = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
            ZmxSuppliers_Client client = contextWs.getBean(ZmxSuppliers_Client.class);
            
            ZMxfmDeliveriesExtResponse wsResp = new ZMxfmDeliveriesExtResponse();
	        try {
	            wsResp = client.executeZMxfmDeliveriesExt(
	        			wsReq,
	        			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
	        		);
	        }catch(Exception e){
				contextWs.close();
	        	throw new WebServiceSapException(e);
	        }
                
                if(wsResp.getBapiret2().getItem().get(0).getType().equals("E")){
                    String mensaje=wsResp.getBapiret2().getItem().get(0).getMessage();
                    if(mensaje !=null &&
                            wsResp.getBapiret2().getItem().get(0).getMessageV1().equals("MONEDA")){
                         resp.setHeader("process_result", "Error");
                        resp.setHeader("process_msg_lst" + iCnt++, mensaje);
                    }
                    else  if(mensaje !=null &&
                            wsResp.getBapiret2().getItem().get(0).getMessageV1().equals("IMPORTE")){
                        resp.setHeader("process_result", "Error");
                        resp.setHeader("process_msg_lst" + iCnt++, mensaje);
                }
                    contextWs.close();
                }
            
        	
        	
        	
                
                else{	
                        TableOfBapiret2 tBapiret2 = wsResp.getBapiret2();
        	List<Bapiret2> lBapiret2 = tBapiret2.getItem();
                String importeWs=wsResp.getBapiret2().getItem().get(0).getMessageV1();
                String coinOut=wsResp.getBapiret2().getItem().get(0).getMessageV2();
                    switch (wsResp.getRStatus()) {
				case "00":
                                        resp.setHeader("process_result", "Success");
					resp.setHeader("process_msg_lst" + iCnt++, "Factura ingresada a revision para proceso de pago.");

					try {
						mpPdf.transferTo(new File(sFilePath));
					} catch (Exception e) {
						e.printStackTrace();
						throw new FileSaveFileException(mpPdf.getOriginalFilename());
					}
					
					ObjectContext cayenneContext = cayenneRuntime.newContext(); 
			    	
					CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.ID_SAP.eq(p_companyCode)).selectOne(cayenneContext);
			    	Long iIdCompany = (Long)db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);
					
					TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
					Long iIdSupplier = (Long)db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);
					
					LocalDate invDate = LocalDate.of(Integer.parseInt(p_documentDate.substring(0, 4)), Integer.parseInt(p_documentDate.substring(5,7)), Integer.parseInt(p_documentDate.substring(8, 10)));
					
					TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
					db_insert.setReference("");
					db_insert.setAmount(bdImporte.doubleValue());
					db_insert.setCurrency("");
					db_insert.setUuid("");
					db_insert.setDocType(p_documentType.equals("F") ? "I" : "E");
					db_insert.setCompany_id(iIdCompany.intValue());
					db_insert.setSupplier_id(iIdSupplier.intValue());
					db_insert.setInvoiceDate(invDate);
					db_insert.setStatus_id(1);
					db_insert.setWithDelivery(true);
					db_insert.setCreatedOn(LocalDate.now());
					
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						if(bapiret2.getType().equals("S")) {
							String sBapiretId = bapiret2.getId();
							db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
							db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
							db_insert.setSapCompany(sBapiretId.substring(0, 4));
                                                        
						}

					}
	            	
	            	try {
	            		cayenneContext.commitChanges();
	            	}catch(Exception e) {
	            		throw new DB_InsertException(e);
	            	}
	            	
					break;
				default:
					throw new SuppliersCustomException(lBapiret2.get(0).getMessage());
					
			}
                    contextWs.close();
                }
        	
        }catch (SuppliersGeneralException e) {
			e.printStackTrace();
    		resp.setHeader("process_result", e.getFrontResult());
			resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
		}catch(Exception e){
			e.printStackTrace();
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst", "Sucedió un error en la aplicación." );
        }
		
		return sReturnTo;
		
	}

	@RequestMapping(method = RequestMethod.POST, value = "/foreign/foreignInvoicesWoDeliveries")
	public String processForeignInvoicesWoDeliveries(
			@RequestParam("documentType") String p_documentType, 
			@RequestParam("companyCode") String p_companyCode,  
			@RequestParam("invoiceNumber") String p_invoiceNumber, 
			@RequestParam("invoiceDate") String p_invoiceDate, 
			@RequestParam("invoiceAmount") String p_invoiceAmount, 
			@RequestParam("invoiceCurrency") String p_invoiceCurrency, 
			@RequestParam("invoiceDescription") String p_invoiceDescr, 
			HttpServletResponse resp
		) {
		
		String sReturnTo = "ForeignInvoicesWoDelivery";
		
		int iCnt = 0;
		
		try {
			
			validateValuesWoDelivery(p_documentType, p_companyCode, p_invoiceNumber, p_invoiceDate, 
									 p_invoiceAmount, p_invoiceCurrency, p_invoiceDescr);
			
			MultipartFile mpPdf = filesSession.getPdfFile();
			String sNewFileName = userSession.getSap_supplier_id() + "_" + p_documentType + "_" + p_invoiceNumber + ".pdf";
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_FOREIGN) + File.separator + sNewFileName;

			
			BigDecimal bdImporte = new BigDecimal(p_invoiceAmount);
			
            
        	
                        resp.setHeader("process_result", "Success");
                        resp.setHeader("process_msg_lst" + iCnt++, "Factura ingresada a revision para proceso de pago.");

                        try {
                            if(mpPdf!=null)
                                mpPdf.transferTo(new File(sFilePath));
                        } catch (Exception e) {
                                e.printStackTrace();
                                throw new FileSaveFileException(mpPdf.getOriginalFilename());
                        }
					
                        ObjectContext cayenneContext = cayenneRuntime.newContext(); 

                                CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.ID_SAP.eq(p_companyCode)).selectOne(cayenneContext);
                        Long iIdCompany = (Long)db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);

                        TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
                                Long iIdSupplier = (Long)db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);

                                LocalDate invDate = LocalDate.of(Integer.parseInt(p_invoiceDate.substring(0, 4)), Integer.parseInt(p_invoiceDate.substring(5,7)), Integer.parseInt(p_invoiceDate.substring(8, 10)));

                                TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
                                db_insert.setReference("");
                                db_insert.setAmount(bdImporte.doubleValue());
                                db_insert.setCurrency(p_invoiceCurrency);
                                db_insert.setUuid("");
                                db_insert.setDocType(p_documentType.equals("F") ? "I" : "E");
                                db_insert.setCompany_id(iIdCompany.intValue());
                                db_insert.setSupplier_id(iIdSupplier.intValue());
                                db_insert.setInvoiceDate(invDate);
                                db_insert.setStatus_id(1);
                                db_insert.setWithDelivery(false);
                                db_insert.setCreatedOn(LocalDate.now());
                                db_insert.setSapPrelimDocNum("");
                                db_insert.setSapFiscalYear("");
                                db_insert.setSapCompany(p_companyCode);
                                db_insert.setConcepto(p_invoiceDescr);
                                db_insert.setNoFactura(p_invoiceNumber);
                                db_insert.setNoProveedorSap(userSession.getSap_supplier_id());
                                        
	            	
	            	try {
	            		cayenneContext.commitChanges();
	            	}catch(Exception e) {
	            		throw new DB_InsertException(e);
	            	}
	            	
				
        	
        }catch (SuppliersGeneralException e) {
			e.printStackTrace();
    		resp.setHeader("process_result", e.getFrontResult());
			resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
		}catch(Exception e){
			e.printStackTrace();
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst", "Sucedió un error en la aplicación." );
        }
		
		return sReturnTo;
		
	}
       /* 
        @RequestMapping(method = RequestMethod.POST, value = "/foreign/foreignInvoicesWoDeliveries")
	public String processForeignInvoicesWoDeliveries(
			@RequestParam("documentType") String p_documentType, 
			@RequestParam("companyCode") String p_companyCode,  
			@RequestParam("invoiceNumber") String p_invoiceNumber, 
			@RequestParam("invoiceDate") String p_invoiceDate, 
			@RequestParam("invoiceAmount") String p_invoiceAmount, 
			@RequestParam("invoiceCurrency") String p_invoiceCurrency, 
			@RequestParam("invoiceDescription") String p_invoiceDescr, 
			HttpServletResponse resp
		) {
		
		String sReturnTo = "ForeignInvoicesWoDelivery";
		
		int iCnt = 0;
		
		try {
			
			validateValuesWoDelivery(p_documentType, p_companyCode, p_invoiceNumber, p_invoiceDate, 
									 p_invoiceAmount, p_invoiceCurrency, p_invoiceDescr);
			
			MultipartFile mpPdf = filesSession.getPdfFile();
			String sNewFileName = userSession.getSap_supplier_id() + "_" + p_documentType + "_" + p_invoiceNumber + ".pdf";
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_FOREIGN) + File.separator + sNewFileName;

			
			BigDecimal bdImporte = new BigDecimal(p_invoiceAmount);
			
            ZmxfmPrelmaryFv60Ext wsReq = new ZmxfmPrelmaryFv60Ext();

    		wsReq.setPConcepto(p_invoiceDescr);
            wsReq.setPFechaFactura(p_invoiceDate);
    		wsReq.setPImporte(bdImporte);
            wsReq.setPIsFactONc(p_documentType.equals("F") ? "I" : "E");
    		wsReq.setPMoneda(p_invoiceCurrency);
    		wsReq.setPNoFactONc(p_invoiceNumber);
    		wsReq.setPNoSapProv(userSession.getSap_supplier_id());
            wsReq.setPSociedad(p_companyCode);
    		
    		wsReq.setBapiret2(new TableOfBapiret2());
    		
            AnnotationConfigApplicationContext contextWs = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
            ZmxSuppliers_Client client = contextWs.getBean(ZmxSuppliers_Client.class);
            
            ZmxfmPrelmaryFv60ExtResponse wsResp = new ZmxfmPrelmaryFv60ExtResponse();
            
	        try {
	            wsResp = client.executeZmxfmPrelmaryFv60Ext(
	        			wsReq,
	        			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
	        		);
	        }catch(Exception e){
				contextWs.close();
				
	        	throw new WebServiceSapException(e);
	        	
	        }
            
        	contextWs.close();
        	
        	TableOfBapiret2 tBapiret2 = wsResp.getBapiret2();
        	List<Bapiret2> lBapiret2 = tBapiret2.getItem();
        	
        	switch (wsResp.getRStatus()) {
				case "00":
		    		resp.setHeader("process_result", "Success");
					resp.setHeader("process_msg_lst" + iCnt++, "Factura ingresada a revision para proceso de pago.");
					
					try {
						mpPdf.transferTo(new File(sFilePath));
					} catch (Exception e) {
						e.printStackTrace();
						throw new FileSaveFileException(mpPdf.getOriginalFilename());
					}
					
					ObjectContext cayenneContext = cayenneRuntime.newContext(); 
			    	
					CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.ID_SAP.eq(p_companyCode)).selectOne(cayenneContext);
			    	Long iIdCompany = (Long)db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);
					
			    	TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
					Long iIdSupplier = (Long)db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);
					
					LocalDate invDate = LocalDate.of(Integer.parseInt(p_invoiceDate.substring(0, 4)), Integer.parseInt(p_invoiceDate.substring(5,7)), Integer.parseInt(p_invoiceDate.substring(8, 10)));
					
					TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
					db_insert.setReference("");
					db_insert.setAmount(bdImporte.doubleValue());
					db_insert.setCurrency(p_invoiceCurrency);
					db_insert.setUuid("");
					db_insert.setDocType(p_documentType.equals("F") ? "I" : "E");
					db_insert.setCompany_id(iIdCompany.intValue());
					db_insert.setSupplier_id(iIdSupplier.intValue());
					db_insert.setInvoiceDate(invDate);
					db_insert.setStatus_id(1);
					db_insert.setWithDelivery(false);
					db_insert.setCreatedOn(LocalDate.now());
					
	            	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
						Bapiret2 bapiret2 = iterator.next();
						if(bapiret2.getType().equals("S")) {
							String sBapiretId = bapiret2.getId();
							db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
							db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
							db_insert.setSapCompany(sBapiretId.substring(0, 4));
						}
					}
	            	
	            	try {
	            		cayenneContext.commitChanges();
	            	}catch(Exception e) {
	            		throw new DB_InsertException(e);
	            	}
	            	
					break;
				default:
					throw new SuppliersCustomException(lBapiret2.get(0).getMessage());
					
		        	
			}
        	
        }catch (SuppliersGeneralException e) {
			e.printStackTrace();
    		resp.setHeader("process_result", e.getFrontResult());
			resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
		}catch(Exception e){
			e.printStackTrace();
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst", "Sucedió un error en la aplicación." );
        }
		
		return sReturnTo;
		
	}*/


	private void validateValuesWoDelivery(String p_documentType, String p_companyCode, String p_invoiceNumber, String p_invoiceDate, 
										  String p_invoiceAmount, String p_invoiceCurrency, String p_invoiceDescr) throws SuppliersGeneralException {
		validateEmpty(p_documentType, "Tipo de Documento");
		validateEmpty(p_companyCode, "Sociedad");
		validateEmpty(p_invoiceNumber, "Número de Factura");
		validateEmpty(p_invoiceDate, "Fecha de Factura");
		validateDate(p_invoiceDate);
		validateEmpty(p_invoiceAmount, "Importe de Factura");
		validateAmount(p_invoiceAmount);
		validateEmpty(p_invoiceCurrency, "Moneda de Factura");
		validateEmpty(p_invoiceDescr, "Concepto de Factura");
	}
	
	private void validateValues(String p_documentType, String p_companyCode, String p_deliveryNumber, String p_invoiceNumber, 
								String p_invoiceDate, String p_documentDate, String p_invoiceAmount, String p_invoiceDescr) throws SuppliersGeneralException {
		validateEmpty(p_documentType, "Tipo de Documento");
		validateEmpty(p_companyCode, "Sociedad");
		validateEmpty(p_deliveryNumber, "Orden de Entrega");
		validateEmpty(p_invoiceNumber, "Número de Factura");
		validateEmpty(p_invoiceDate, "Fecha de Entrada de Mercancía");		
		validateDate(p_invoiceDate);
		validateEmpty(p_documentDate, "Fecha Factura Física");
		validateDate(p_documentDate);
		validateEmpty(p_invoiceAmount, "Importe de Factura");
		validateAmount(p_invoiceAmount);
		validateEmpty(p_invoiceDescr, "Concepto de Factura");
	}
	
	private void validateAmount(String p_Amount) throws ValidationAmountException {
		try {
			new BigDecimal(p_Amount);
		}catch(Exception e) {
			e.printStackTrace();
			throw new ValidationAmountException(p_Amount);
		}
	}
	
	private void validateDate(String p_date) throws ValidationDateException {
		try {
			LocalDate.of(Integer.parseInt(p_date.substring(0, 4)), Integer.parseInt(p_date.substring(5,7)), Integer.parseInt(p_date.substring(8, 10)));
		}catch(Exception e) {
			e.printStackTrace();
			throw new ValidationDateException(p_date);
		}
		
	}
	
	private void validateEmpty(String p_value, String p_fieldName) throws ValidationEmptyValueException {
		if(p_value == null || p_value.trim().equals("")) {
			throw new ValidationEmptyValueException(p_fieldName);
		}
	}
	
	@PostMapping(value = "/foreign/uploadPdfForeign")
	public String uploadPdfPayments(@RequestParam("uploaderPdf") MultipartFile mpFile, HttpServletResponse resp) {
		try {
			if(mpFile == null || mpFile.getSize() <= 0 || mpFile.getOriginalFilename().trim().equals("")) {
				throw new ValidationEmptyValueException("Archivo PDF");
			}else {
				filesSession.setPdfFile(mpFile);
			}
		}catch(ValidationEmptyValueException e) {
    		resp.setHeader("process_result", e.getFrontResult());
			resp.setHeader("process_msg_lst", e.getFrontMessage());
		}
			
        return "ForeignInvoices";
		
	}
        
     private String validaDatos(BigDecimal importeIn, String importeOut,String monedaIn, String monedaOut){
         String mensaje=null;
         if(importeOut!=null){
             if(!String.valueOf(importeIn).equals(importeOut)){
                 mensaje="Importe introducido no concuerda con la Entrega";
             }
             if(!monedaIn.equals(monedaOut)){
                 mensaje="Moneda introducida no es igual a la orden de compra";
             }
         }
         else {
             mensaje="Ocurrio un error";
         }
        return mensaje; 
     }   
	
}
