package com.lebelier.suppliers.web.controller;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.cayenne.configuration.server.ServerRuntime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.lebelier.cfdi.comprobante.xml.entities.Comprobante;
import com.lebelier.suppliers.db.utils.InvoiceValidator;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UploadFilesArrayCutomBrokerSession;
import com.lebelier.suppliers.web.entity.UploadFilesCustomBrokerSession;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.CfdiXmlParsingException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.ValidationDocumentTypeException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.ValidationWebServiceSatException;
import com.lebelier.suppliers.web.exception.WebServiceSatException;
import com.lebelier.suppliers.ws.client.exec.ConsultaCFDIServiceClient;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmFactAduana;
import com.lebelier.suppliers.ws.sap.client.types.ZmxfmFactAduanaResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZstXml;
import com.lebelier.suppliers.ws.sap.client.types.ZttXml;
import com.lebelier.suppliers.ws.sat.client.types.Acuse;
import com.lebelier.suppliers.ws.sat.client.types.ConsultaResponse;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;

@Controller
public class InvoicesCustomBroker {

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;

    @Autowired
	private ServerRuntime cayenneRuntime;

	@Autowired
	private UploadFilesArrayCutomBrokerSession filesSession;

	@GetMapping("/payableAccounts/InvoicesTravelExpenses")
	public String goTravelExpenses() {
		filesSession.initialize(); 
		return "InvoicesTravelExpenses";
	}

	@GetMapping("/customBroker/InvoicesCustomBroker")
	public String goInvoicesCustomBroker() {
		filesSession.initialize(); 
		return "InvoicesCustomBroker";
	}

	@RequestMapping(value = "/customBroker/invoiceCustomBroker")
	public String processInvoiceCB(
			HttpServletResponse resp
		) {
		
		return processInvoice("A", resp);
		
	}

	@RequestMapping(value = "/payableAccounts/invoiceTravelExpenses")
	public String processInvoiceTE(
			HttpServletResponse resp
		) {
		return processInvoice("V", resp);
		
	}
	
	private String processInvoice(
			String p_process,
			HttpServletResponse resp
		) {
		
		String sReturnTo = "InvoicesCustomBroker";
		
		UploadFilesCustomBrokerSession[] aFiles = filesSession.getUploadFilesSession();

		boolean hasErrors = true;
		int iCnt = 0;
		
		ZttXml ztt = new ZttXml();
		
		List<ZstXml> tXml = ztt.getItem();

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
        
    	for(UploadFilesCustomBrokerSession file : aFiles) {
    		
    		if(file == null || file.getPdfFile() == null || file.getXmlFile() == null) {
    			continue;
    		}
    		
			MultipartFile mpXml = file.getXmlFile();
			MultipartFile mpPdf = file.getPdfFile();
			
			String sUuid = "";
			String sTipoComprobante = "";
			String sRfcReceptor = "";
			String sRfcEmisor = "";
			BigDecimal bdTotal = new BigDecimal(0);
			
			try {
				//(new XsdValidator()).validateComprobanteUsingXsd(mpXml.getOriginalFilename(), mpXml.getInputStream());
				
				try {
			        JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);
			        
			        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			        Comprobante comprobante = (Comprobante) unmarshaller.unmarshal(mpXml.getInputStream());
			        
					sRfcReceptor = comprobante.getReceptor().getRfc();
					sRfcEmisor = comprobante.getEmisor().getRfc();
					bdTotal = comprobante.getTotal();
					sTipoComprobante = comprobante.getTipoDeComprobante().toString();
					
					List<Object> lCompl = comprobante.getComplemento().get(0).getAny();
					
					for (Object object : lCompl) {
						ElementNSImpl element = (ElementNSImpl) object;
						String sName = element.getLocalName();
						if(sName.equals("TimbreFiscalDigital")){
							sUuid = element.getAttribute("UUID");	
						}
					}
					
				}catch(Exception e) {
					e.printStackTrace();
	
		        	throw new CfdiXmlParsingException(mpXml.getOriginalFilename());
				}

				if(sUuid.equals("")) {
					throw new ValidationEmptyValueException("UUID");
				}
				
				InvoiceValidator.existUuid(cayenneRuntime, sUuid);
				
				if(!sTipoComprobante.trim().equals("I") && !sTipoComprobante.trim().equals("E")) {
					throw new ValidationDocumentTypeException(mpXml.getOriginalFilename(), sTipoComprobante);
				}
				
	            ConsultaCFDIServiceClient satClient = context.getBean(ConsultaCFDIServiceClient.class);
		        
	            ConsultaResponse consultaResp;
	            
				try {
					consultaResp = satClient.executeConsulta(environment.getProperty(PropetiesConstants.WS_SAT_UUID_WSDL), 
																			  environment.getProperty(PropetiesConstants.WS_SAT_UUID_ACTION), 
																			  sRfcEmisor, 
																			  sRfcReceptor, 
																			  bdTotal + "", 
																			  sUuid);
				} catch (WebServiceSatException e) {
	            	e.printStackTrace();
	            	
	            	throw new  WebServiceSatException(e);
				}
	
				Acuse acuse =  consultaResp.getConsultaResult().getValue();
				String codigoEstatus = acuse.getCodigoEstatus().getValue();
	
				if(codigoEstatus.startsWith("N")){
					throw new ValidationWebServiceSatException(sUuid, codigoEstatus);
				}
				
				String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_ARCH_PATH) + File.separator + "INV_" + sUuid;
	
				/*try {
					mpXml.transferTo(new File(sFilePath + ".xml"));
				} catch (Exception e) {
					e.printStackTrace();
	            	
					throw new FileSaveFileException(mpXml.getOriginalFilename());
				}
	
				try {
					mpPdf.transferTo(new File(sFilePath + ".pdf"));
				} catch (Exception e) {
					e.printStackTrace();
	            	
					throw new FileSaveFileException(mpPdf.getOriginalFilename());
				}*/
				
				ZstXml zXml = new ZstXml();
				zXml.setXml(new String(mpXml.getBytes()));
				zXml.setFactprincipal(file.isOwnIvoice() ? "X" : "");
				
				tXml.add(zXml);
				
			}catch(SuppliersGeneralException e) {
				e.printStackTrace();
				
	    		resp.setHeader("process_result", e.getFrontResult());
	    		resp.setHeader("process_msg_lst" + iCnt++, e.getFrontMessage());
	    		
	        }catch (Exception e) {
				e.printStackTrace();
				
	    		resp.setHeader("process_result", "Error");
	    		resp.setHeader("process_msg_lst" + iCnt++, "Sucedió un error.");
	    		
			}
        		
    	}
    	
		String sNumProSAP = userSession.getSap_supplier_id();

        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
    	String sStatus = "";
    	
		ZmxfmFactAduana  wsReq = new ZmxfmFactAduana();

        wsReq.setPLifnr(sNumProSAP);
        wsReq.setPXmlfiles(ztt);
        wsReq.setIProceso(p_process);
        
        ZmxfmFactAduanaResponse wsResp;
		try {
            wsResp = client.executeZmxfmFactAduana(
        			wsReq,
        			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
        		);
            
    		sStatus = wsResp.getRStatus();
    			
        	context.close();

    		filesSession.initialize(); 

        	switch (wsResp.getRBapiret2().getItem().get(0).getNumber()) {
    			case "000":
    				

    /*				ObjectContext cayenneContext = cayenneRuntime.newContext(); 
    		    	
    				CCompanies db_company = ObjectSelect.query(CCompanies.class).where(CCompanies.TAXID.eq(sRfcReceptor)).selectOne(cayenneContext);
    		    	Long iIdCompany = (Long)db_company.getObjectId().getIdSnapshot().get(CCompanies.ID_PK_COLUMN);
    				
    		    	TSuppliersQuery db_supplier = ObjectSelect.query(TSuppliersQuery.class).where(TSuppliersQuery.ID_SAP.eq(userSession.getSap_supplier_id())).selectOne(cayenneContext);
    				Long iIdSupplier = (Long)db_supplier.getObjectId().getIdSnapshot().get(TSuppliersQuery.ID_PK_COLUMN);

    				LocalDate lDate = LocalDate.of(Integer.parseInt(sFechaCfdi.substring(0, 4)), Integer.parseInt(sFechaCfdi.substring(5,7)), Integer.parseInt(sFechaCfdi.substring(8, 10)));
    				
    				TInvoicesInsert db_insert = cayenneContext.newObject(TInvoicesInsert.class);
    				db_insert.setReference(sFolioCfdi);
    				db_insert.setAmount(bdTotal.doubleValue());
    				db_insert.setCurrency(sMonedaCfdi);
    				db_insert.setUuid(sUuid);
    				db_insert.setDocType(sTipoComprobante);
    				db_insert.setCompany_id(iIdCompany.intValue());
    				db_insert.setSupplier_id(iIdSupplier.intValue());
    				db_insert.setInvoiceDate(lDate);
    				db_insert.setStatus_id(1);
    				db_insert.setCreatedOn(LocalDate.now());
    				db_insert.setWithDelivery(false);

                	for (Iterator<Bapiret2> iterator = lBapiret2.iterator(); iterator.hasNext(); ) {
    					Bapiret2 bapiret2 = iterator.next();
    					if(bapiret2.getType().equals("S")) {
    						String sBapiretId = bapiret2.getId();
    						db_insert.setSapPrelimDocNum(sBapiretId.substring(10));
    						db_insert.setSapFiscalYear(sBapiretId.substring(5, 9));
    						db_insert.setSapCompany(sBapiretId.substring(0, 4));
    					}

    				}

                	try {
                		cayenneContext.commitChanges();
                	}catch(Exception e) {
                		throw new DB_InsertException(e);
                	}
    */
    				hasErrors = false;
    				
    				break;
    			default:
    				
    				hasErrors = true;
    				
                	break;
                	
    		}

			resp.setHeader("process_msg_lst_" + iCnt++, wsResp.getRBapiret2().getItem().get(0).getMessage() );
			
			
		}catch(Exception e) {
			e.printStackTrace();

			hasErrors = true;
			
			resp.setHeader("process_msg_lst_" + iCnt++, e.getMessage() );
		}
		
    	if(hasErrors) {
    		resp.setHeader("process_result", "Error");
    	}else {
    		resp.setHeader("process_result", "Success");
    	}
		
		return sReturnTo;
		
	}
	
	@PostMapping({"/customBroker/uploadXmlCustomBroker", "/payableAccounts/uploadXmlCustomBroker"})
	@ResponseBody
	public String uploadXmlCustomBroker(
			@RequestParam(required = true) MultipartFile xmlFile,
			@RequestParam(value = "indXml", required = true) int index,
			HttpServletResponse resp
		) {
		
			filesSession.getUploadFilesSession()[index].setXmlFile(xmlFile);
			
			return "InvoicesCustomBroker";
        
	}

	@PostMapping({"/customBroker/uploadPdfCustomBroker", "/payableAccounts/uploadPdfCustomBroker"})
	@ResponseBody
	public String uploadPdfCustomBroker(
			@RequestParam(required = true) MultipartFile pdfFile,
			@RequestParam(value = "indPdf", required = true) int index,
			HttpServletResponse resp
		) {
		
			filesSession.getUploadFilesSession()[index].setPdfFile(pdfFile);
			
			return "InvoicesCustomBroker";
		
	}

	@PostMapping({"/customBroker/selectOwn", "/payableAccounts/selectOwn"})
	@ResponseBody
	public String selectInvoiceTypeOwn(
			@RequestParam(value = "ind", required = true) int index,
			HttpServletResponse resp
		) {
		
			filesSession.getUploadFilesSession()[index].setOwnIvoice(true);
			
			return "OK.";
		
	}

	@PostMapping({"/customBroker/selectRel", "/payableAccounts/selectRel"})
	@ResponseBody
	public String selectInvoiceTypeRel(
			@RequestParam(value = "ind", required = true) int index,
			HttpServletResponse resp
		) {
		
			filesSession.getUploadFilesSession()[index].setOwnIvoice(false);
			
			return "OK.";
		
	}

	@PostMapping({"/customBroker/removeFile", "/payableAccounts/removeFile"})
	@ResponseBody
	public String removeFile(
			@RequestParam(value = "ind", required = true) int index,
			HttpServletResponse resp
		) {
			
			filesSession.getUploadFilesSession()[index].setOwnIvoice(false);
			filesSession.getUploadFilesSession()[index].setPdfFile(null);
			filesSession.getUploadFilesSession()[index].setXmlFile(null);
			
			return "OK.";
		
	}

	
}
