package com.lebelier.suppliers.web.controller;

import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.cayenne.CayenneRuntimeException;
import org.apache.cayenne.ObjectContext;
import  org.apache.cayenne.ObjectId;
import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.query.SelectById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesInsert;
import com.lebelier.suppliers.db.cayenne.entities.AuthoritiesQuery;
import com.lebelier.suppliers.db.cayenne.entities.CRolesId;
import com.lebelier.suppliers.db.cayenne.entities.TContactMessagesQuery;
import com.lebelier.suppliers.db.cayenne.entities.TSuppliersQuery;
import com.lebelier.suppliers.db.cayenne.entities.UsersInsert;
import com.lebelier.suppliers.db.cayenne.entities.UsersQuery;
import com.lebelier.suppliers.email.service.EmailSender;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.constants.RolesConstants;
import com.lebelier.suppliers.web.exception.DB_DeleteException;
import com.lebelier.suppliers.web.exception.DB_QueryException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.security.PasswordHandler;

@Component
@Controller
public class UsersAdminController {

	@Resource
    private Environment environment;

    @Autowired
	private ServerRuntime cayenneRuntime;

    @Autowired
    private JavaMailSender javaMailSender;
    
	private final String PASSWORD_ENCODER_PREFIX = "{bcrypt}";
    
	@GetMapping("/admin/UsersAdministration")
	public String goUsersAdministration() {
		return "UsersAdministration";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/enableUser")
	public String enableUser(
			@RequestParam("id") int p_id, 
			@RequestParam("enable") boolean p_enable,
			HttpServletResponse resp
		) {
		
		String sReturnTo = "UsersAdministration";
    	int iCnt = 1;
    	
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	
	    	ObjectId oId = new ObjectId("UsersQuery", UsersQuery.ID_PK_COLUMN, p_id);
	    	UsersQuery user = SelectById.query(UsersQuery.class, oId).selectOne(context);
	    	
	    	user.setEnabled(p_enable);

			context.commitChanges();

    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Usuario " + (p_enable ? "habilitado" : "deshabilitado") + " correctamente. ");// + p_username);
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al actualizar el usuario: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/admin/deleteUser")
	public String deleteUser(
			@RequestParam("id") int p_id,
			HttpServletResponse resp
		) {
		
		String sReturnTo = "UsersAdministration";
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 

	    	ObjectId oIdMesg = new ObjectId("TContactMessagesQuery", TContactMessagesQuery.SENDER.getName(), p_id);
	    	TContactMessagesQuery userMesg;
	    	try {
	    		userMesg = SelectById.query(TContactMessagesQuery.class, oIdMesg).selectOne(context);
	    	}catch(Exception e) {
	    		throw new DB_QueryException(e); 
	    	}
	    	
	    	if(userMesg != null) {
		    	try {
		    		context.deleteObject(userMesg);
		    	}catch(Exception e) {
		    		throw new DB_DeleteException(e); 
		    	}
	    	}
	    	
	    	ObjectId oIdAuth = new ObjectId("AuthoritiesQuery", AuthoritiesQuery.ID_USER_PK_COLUMN, p_id);
	    	AuthoritiesQuery auth;
	    	
	    	try {
	    		auth = SelectById.query(AuthoritiesQuery.class, oIdAuth).selectOne(context);
	    	}catch(Exception e) {
	    		throw new DB_QueryException(e); 
	    	}

	    	try {
	    		context.deleteObject(auth);
	    	}catch(Exception e) {
	    		throw new DB_DeleteException(e); 
	    	}
	    	
	    	ObjectId oIdUser = new ObjectId("UsersQuery", UsersQuery.ID_PK_COLUMN, p_id);
	    	UsersQuery user;

	    	try {
	    		user = SelectById.query(UsersQuery.class, oIdUser).selectOne(context);
	    	}catch(Exception e) {
	    		throw new DB_QueryException(e); 
	    	}

	    	try {
	    		context.deleteObject(user);
	    	}catch(Exception e) {
	    		throw new DB_DeleteException(e); 
	    	}

	    	try {
				context.commitChanges();
	    	}catch(Exception e) {
	    		throw new DB_DeleteException(e); 
	    	}
			
    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst", "Usuario eliminado correctamente. ");
			
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			
    		resp.setHeader("process_result", e.getFrontResult());
			resp.setHeader("process_msg_lst", e.getFrontMessage());
			
		}catch(Exception e) {
			e.printStackTrace();

    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst", "Sucedió un error en la aplicación.");
			
		}
		
		return sReturnTo;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/admin/createUser")
	public String createUser(
			@RequestParam("name") String p_name, 
			@RequestParam("lastname") String p_lastname, 
			@RequestParam("email") String p_email,
			@RequestParam("supplier") int p_supplier, 
			@RequestParam("role") int p_role, 
			HttpServletResponse resp
		) {
		
		System.out.println("createUser ... ");
		
		String sReturnTo = "UsersAdministration";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	    	
	    	String sUsername = "";
	    	
	    	String sPassword = PasswordHandler.generateRandomPassword();
	    	
	    	//AutoPkSupport autoPk = ObjectSelect.query(AutoPkSupport.class).where(AutoPkSupport.TABLE_NAME.eq("users")).selectOne(context);
	    	
			ObjectId roleId = new ObjectId("CRolesId", CRolesId.ID_PK_COLUMN, p_role);
			CRolesId cRole = SelectById.query(CRolesId.class, roleId).selectFirst(context);
			
	    	UsersInsert user = context.newObject(UsersInsert.class);
			user.setName(p_name);
			user.setLastname(p_lastname);
			user.setEmail(p_email);
			
			if(cRole.getName().equals(RolesConstants.SUPMX) || cRole.getName().equals(RolesConstants.SUPFR) || cRole.getName().equals(RolesConstants.CUSTB)) {
				ObjectId oSuppId = new ObjectId("TSuppliersQuery", TSuppliersQuery.ID_PK_COLUMN, p_supplier);
				TSuppliersQuery tSupp = SelectById.query(TSuppliersQuery.class, oSuppId).selectFirst(context);
				
				sUsername = tSupp.getIdSap();
			}else {
				sUsername = p_email;
			}
			user.setUsername(sUsername);
			
			user.setPassword(this.PASSWORD_ENCODER_PREFIX + PasswordHandler.encodePassword(sPassword));
			user.setId_suppliers(p_supplier);
			user.setId_language(2); // id_language = ES
			user.setCreatedOn(LocalDateTime.now());
			user.setEnabled(true);

			context.commitChanges();

			Long userId = (Long)user.getObjectId().getIdSnapshot().get(UsersInsert.ID_PK_COLUMN);
			
	    	AuthoritiesInsert authority = context.newObject(AuthoritiesInsert.class);
	    	authority.setId_user(userId.intValue());
	    	authority.setId_role(p_role);
	    	authority.setUsername(sUsername);
	    	
	    	// Para roles Proveedor Nacional (1) y Proveedor Extranjero (2) se asigna el role UPDATE_CONTACTS
	    	if(p_role == 1 || p_role == 2) {
	    		authority.setAuthority(RolesConstants.ROLE_PREFIX + RolesConstants.UPDCONT);
	    	}else {
	    		authority.setAuthority(RolesConstants.ROLE_PREFIX + cRole.getName());
	    	}
	    	
			context.commitChanges();
			
			resp.setHeader("process_msg_lst_" + iCnt++, "Usuario creado correctamente.");
			
			try {
		        EmailSender eSender = new EmailSender();
		        
		        String sUrl = environment.getProperty(PropetiesConstants.WEB_SUPP_MAIN_URL);
		        
		        String sBodyMail = "Estimado proveedor " + p_name + " " + p_lastname + "\n\n"
		        		+ "Se ha creado su usuario para acceder a nuestro portal de proveedores \n\n"
		        		+ "Por favor validar su acceso en la siguiente dirección:\n"
		        		+ "\t"  + sUrl + "\n\n"
		        		+ "Utilizando los siguientes datos: \n"
		        		+ "\tUsuario: " + sUsername + "\n"
		        		+ "\tContraseña: " + sPassword + "\n\n"
		        		+ "LeBelier" 
		        		+ "\n.\n";
		        
		        eSender.sendSimpleMessage(javaMailSender, p_email, "LeBelier - Portal Proveedores - Creación de Usuario", sBodyMail);
	
	    		resp.setHeader("process_result", "Success");
	    		
			}catch(MailSendException e) {
				e.printStackTrace();
				
	    		resp.setHeader("process_result", "Warning");
				resp.setHeader("process_msg_lst_" + iCnt++, "Sucedió un error al enviar el correo electrónico al usuario");
			}
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al crear el usuario: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}

	@RequestMapping(method = RequestMethod.POST, value = "/admin/updateUser")
	public String updateUser(
			@RequestParam("id") int p_id,
//			@RequestParam("username") String p_username,
			@RequestParam("name") String p_name, 
			@RequestParam("lastname") String p_lastname, 
			@RequestParam("email") String p_email, 
			HttpServletResponse resp
		) {
		
		System.out.println("updateUser ... ");
		
		String sReturnTo = "UsersAdministration";
		
    	int iCnt = 1;
		
		try {
			ObjectContext context = cayenneRuntime.newContext(); 
	    	
	    	ObjectId oId = new ObjectId("UsersInsert", UsersInsert.ID_PK_COLUMN, p_id);
	    	UsersInsert user = SelectById.query(UsersInsert.class, oId).selectOne(context);
	    	
			user.setName(p_name);
			user.setLastname(p_lastname);
			user.setEmail(p_email);
			
			context.commitChanges();

    		resp.setHeader("process_result", "Success");
			resp.setHeader("process_msg_lst_" + iCnt++, "Usuario actualizado correctamente.");
			
		}catch(CayenneRuntimeException e) {
			String sMessage = "Sucedió un error al actualizar el usuario: ";
			
			if(e.getCause() != null && e.getCause().getMessage() != null && !e.getCause().getMessage().equals("")) {
				sMessage += e.getCause().getMessage();
			}else {
				sMessage += e.getMessage();
			}
			
    		resp.setHeader("process_result", "Error");
			resp.setHeader("process_msg_lst_" + iCnt++, sMessage);
			
			e.printStackTrace();
			
		}catch(Exception e) {
			
			String[] sMsg = e.getMessage().split("\n");

    		resp.setHeader("process_result", "Error");
    		if(sMsg.length > 0) {
    			String sMsg0 = sMsg[0];
    			sMsg = sMsg0.split(";");

    			for (int i = 0; i < sMsg.length; i++) {
    				resp.setHeader("process_msg_lst_" + iCnt++, sMsg[i]);
    			}
    			
    		}
    		
			e.printStackTrace();
			
		}
		
		return sReturnTo;
		
	}

}
