package com.lebelier.suppliers.web.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lebelier.suppliers.db.dto.PaymentComplementDto;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentComplment;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentComplmentResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZtyPaymentComp;

@Component
@RestController
public class PaymentComplementJSonController {

	@Resource
    private Environment environment;

	@Autowired
	private UserSession userSession;
	
	@GetMapping(path = "/payment/listPaymentComplement", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PaymentComplementDto>> getPaymentsComplement() {
		List<PaymentComplementDto> lstDto = new ArrayList<PaymentComplementDto>();
		
		String suppId = userSession.getSap_supplier_id();

		List<ZtyPaymentComp> lstSap;

		try {
			try {
				lstSap = getInvoicesFromSap(suppId);
			}catch(Exception e) {
				throw new WebServiceSapException(e);
			}
		}catch(SuppliersGeneralException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().header("process_result", e.getFrontResult()).header("process_msg", e.getFrontMessage()).body(lstDto);
		}
		
		lstSap.sort(new SortByAugbl());
		
		String belnrAux = "";
		
		for (ZtyPaymentComp zSap : lstSap) {

			PaymentComplementDto iDto = new PaymentComplementDto();
			
			if(!belnrAux.equals(zSap.getAugbl())){
				belnrAux = zSap.getAugbl();
				iDto.setVisible(true);
			}else {
				iDto.setVisible(false);
			}
			
                iDto.setReferenceDoc(zSap.getBelnr());
                iDto.setReferenceFolio(zSap.getXblnr());
    		iDto.setAmount(zSap.getWrbtr() + "");
    		iDto.setPaymentDoc(zSap.getAugbl());
    		
    		iDto.setCompany(zSap.getBukrs());
    		iDto.setYear(zSap.getGjahr());
    		
			lstDto.add(iDto);
			
		}
		
		return ResponseEntity.ok().body(lstDto);
	}
	
	private List<ZtyPaymentComp> getInvoicesFromSap(String p_supplierId){

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
        
        ZMxfmPaymentComplment req = new ZMxfmPaymentComplment();
		req.setPProveedor(p_supplierId);
		
		ZMxfmPaymentComplmentResponse wsResp = client.executeZMxfmPaymentComplment(
    			req,
    			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
    		);
    	
		List<ZtyPaymentComp> respInvoices = wsResp.getTtPaymentComp().getItem();
		
		context.close();
		
		return respInvoices;
		
	}
	
	private class SortByAugbl implements Comparator<ZtyPaymentComp> 
	{ 
		
		@Override
		public int compare(ZtyPaymentComp o1, ZtyPaymentComp o2) {
			return o1.getAugbl().compareTo(o2.getAugbl());
		}
		
	} 
	 
	
}
