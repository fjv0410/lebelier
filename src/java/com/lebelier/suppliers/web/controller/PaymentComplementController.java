package com.lebelier.suppliers.web.controller;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.NodeList;

import com.lebelier.cfdi.comprobante.xml.entities.Comprobante;
import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UploadFilesSession;
import com.lebelier.suppliers.web.entity.UserSession;
import com.lebelier.suppliers.web.exception.CfdiXmlParsingException;
import com.lebelier.suppliers.web.exception.FileSaveFileException;
import com.lebelier.suppliers.web.exception.SuppliersGeneralException;
import com.lebelier.suppliers.web.exception.ValidationEmptyValueException;
import com.lebelier.suppliers.web.exception.WebServiceSapException;
import com.lebelier.suppliers.ws.client.exec.ZmxSuppliers_Client;
import com.lebelier.suppliers.ws.config.ZmxSuppliers_Config;
import com.lebelier.suppliers.ws.sap.client.types.TableOfBapiret2;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentUuid;
import com.lebelier.suppliers.ws.sap.client.types.ZMxfmPaymentUuidResponse;
import com.lebelier.suppliers.ws.sap.client.types.ZttFactUuid;
import com.lebelier.suppliers.ws.sap.client.types.ZtyFactUuid;
import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;

@Component
@Controller
public class PaymentComplementController {

	@Resource
    private Environment environment;

	@Autowired
	private UploadFilesSession filesSession;

	@Autowired
	private UserSession userSession;
	
	@GetMapping("/payment/PaymentComplements")
	public String goReportInvoicesStatus() {
		filesSession.setXmlFile(null);
		filesSession.setPdfFile(null);
		return "PaymentComplements";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/payment/paymentComplement")
	public String paymentComplement(
			@RequestParam("payment") String p_payment, 
			@RequestParam("company") String p_company, 
			@RequestParam("year") String p_year,
			HttpServletResponse httpResp
		) {
		
		String sReturnTo = "PaymentComplements";

		String sSuppId = userSession.getSap_supplier_id();
		
		String sTypMsg = "Error";
		String sMsg = "";
		
		try {
			MultipartFile mpXml = filesSession.getXmlFile();
			MultipartFile mpPdf = filesSession.getPdfFile();
	
			Comprobante cfdi = new Comprobante();
			
			String sUuid = "";
			String sRfcEmisor = "";
			String sRfcReceptor = "";
			String sTipoCfdi = "";
			
	        ZttFactUuid zttFactUuid = new ZttFactUuid();
	        
			try {
		        JAXBContext jaxbContext = JAXBContext.newInstance(Comprobante.class);
		        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		        
		        cfdi = (Comprobante) unmarshaller.unmarshal(mpXml.getInputStream());
		        
				ElementNSImpl ePagos = (ElementNSImpl) cfdi.getComplemento().get(0).getAny().get(0);
				ElementNSImpl eTimbre = (ElementNSImpl) cfdi.getComplemento().get(0).getAny().get(1);
				
				sUuid = eTimbre.getAttribute("UUID");
				
				NodeList nlRelacionados = ePagos.getChildNodes().item(0).getChildNodes();
				
				for (int i = 0; i < nlRelacionados.getLength(); i++) {
					ElementNSImpl eDocRel = (ElementNSImpl)nlRelacionados.item(i);
					
			        ZtyFactUuid ztyFactUuid = new ZtyFactUuid();
			        ztyFactUuid.setBelnr(p_payment);
			        ztyFactUuid.setBukrs(p_company);
			        ztyFactUuid.setGjahr(p_year);
			        ztyFactUuid.setUuidFact(eDocRel.getAttribute("IdDocumento"));
			        
			        zttFactUuid.getItem().add(ztyFactUuid);
			        
				}
				
				
				sRfcEmisor = cfdi.getEmisor().getRfc();
				sRfcReceptor = cfdi.getReceptor().getRfc();
				sTipoCfdi = cfdi.getTipoDeComprobante().value();
				
			}catch(Exception e) {
				e.printStackTrace();
	        	throw new CfdiXmlParsingException(mpXml.getOriginalFilename());
			}
			
			if(sUuid.equals("")) {
				throw new ValidationEmptyValueException("UUID");
			}
			
			if(sUuid.equals("")) {
				throw new ValidationEmptyValueException("UUID Relacionado");
			}
			
			String sFilePath = environment.getProperty(PropetiesConstants.FILES_UPLOAD_CFDI_ARCH_PATH) + File.separator + "CP_" + sUuid;
			
			try {
				mpXml.transferTo(new File(sFilePath + ".xml"));
			} catch (Exception e) {
				e.printStackTrace();
				throw new FileSaveFileException(mpXml.getOriginalFilename());
			}

			try {
				mpPdf.transferTo(new File(sFilePath + ".pdf"));
			} catch (Exception e) {
				e.printStackTrace();
				throw new FileSaveFileException(mpPdf.getOriginalFilename());
			}
			
	        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ZmxSuppliers_Config.class);
	        ZmxSuppliers_Client client = context.getBean(ZmxSuppliers_Client.class);
	        /*
	        ZttFactUuid zttFactUuid = new ZttFactUuid();
	        
	        ZtyFactUuid ztyFactUuid = new ZtyFactUuid();
	        ztyFactUuid.setBelnr(p_payment);
	        ztyFactUuid.setBukrs(p_company);
	        ztyFactUuid.setGjahr(p_year);
	        ztyFactUuid.setUuidFact(sUuidRel);
	        
	        zttFactUuid.getItem().add(ztyFactUuid);
	        */
	        
	        ZMxfmPaymentUuid wsReq = new ZMxfmPaymentUuid();
	        wsReq.setTUuid(zttFactUuid);
	        
	        wsReq.setPNosapProv(sSuppId);
	        wsReq.setPRfcEmisor(sRfcEmisor);
	        wsReq.setPRfcReceptor(sRfcReceptor);
	        wsReq.setPTipoCfdi(sTipoCfdi);
	        wsReq.setPUuid(sUuid);
			
	        ZMxfmPaymentUuidResponse wsResp = new ZMxfmPaymentUuidResponse();
	        try {
	        	wsResp = client.executeZMxfmPaymentUuid(
	        			wsReq,
		    			environment.getProperty(PropetiesConstants.WS_SAP_SUPPLIERS_WSDL)
		    		);
				context.close();
				
	        }catch(Exception e){
				context.close();
				e.printStackTrace();
	        	throw new WebServiceSapException(e);
	        }

			TableOfBapiret2 respInvoices = wsResp.getBapiret2();
			
			if(respInvoices != null && 
					respInvoices.getItem() != null &&
					respInvoices.getItem().size() > 0 &&
					respInvoices.getItem().get(0) != null && 
					respInvoices.getItem().get(0).getId().equals("00")) {
				
				filesSession.setXmlFile(null);
				filesSession.setPdfFile(null);
				
				sTypMsg = "Success";
				sMsg = "Complemento de Pago procesado correctamente: " + sUuid;
				
			}else if(respInvoices != null && 
					respInvoices.getItem() != null && 
					respInvoices.getItem().size() > 0 &&
					respInvoices.getItem().get(0) != null){
				
				sTypMsg = "Error";
				sMsg = respInvoices.getItem().get(0).getMessage();
				
			}else {

				sTypMsg = "Error";
				sMsg = "Sucedió un error al procesar el Complemento de Pago: " + sUuid;
				
			}
			
		} catch (SuppliersGeneralException e) {
			e.printStackTrace();
			
			sTypMsg = e.getFrontResult();
			sMsg = e.getFrontMessage();
		}

		httpResp.setHeader("process_result", sTypMsg);
		httpResp.setHeader("process_msg_lst", sMsg);
			
		return sReturnTo;
		
	}

	@PostMapping(value = "/payment/uploadXmlPayment")
	public String uploadXmlPayment(@RequestParam("xmlFile") MultipartFile mpFile) {
		filesSession.setXmlFile(mpFile);
		
        return "PaymentComplements";
		
	}

	@PostMapping(value = "/payment/uploadPdfPayments")
	public String uploadPdfPayments(@RequestParam("pdfFile") MultipartFile mpFile, HttpServletResponse resp) {
		filesSession.setPdfFile(mpFile);
		
        return "PaymentComplements";
		
	}
	
}
