package com.lebelier.suppliers.web.security;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordHandler {
	
	public static String encodePassword(String p_password) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16);
		return encoder.encode(p_password);
	}
	
	public static String generateRandomPassword() {
		return RandomStringUtils.random(20, true, true);
	}

	public static String generateResetPasswordCode(int p_lenght) {

		return RandomStringUtils.randomNumeric(p_lenght);
		
	}
	
}
