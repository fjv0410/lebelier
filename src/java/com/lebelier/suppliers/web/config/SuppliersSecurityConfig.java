package com.lebelier.suppliers.web.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.lebelier.suppliers.web.constants.RolesConstants;

@Configuration
@EnableWebSecurity
public class SuppliersSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource securityDataSource;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(securityDataSource);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		String[] aAnyRole = {
			RolesConstants.ADMIN,
			RolesConstants.PAYAB,
			RolesConstants.SUPMX,
			RolesConstants.SUPFR,
			RolesConstants.CUSTB,
			RolesConstants.UPDCONT
		};
		
		http.headers().frameOptions().sameOrigin().
			and().headers().contentTypeOptions().disable();
		
		http.authorizeRequests().
			antMatchers("/home").hasAnyRole(aAnyRole).
			antMatchers("/admin/**").hasRole(RolesConstants.ADMIN).
			antMatchers("/invoice/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.PAYAB, RolesConstants.CUSTB).
			antMatchers("/customBroker/**").hasAnyRole(RolesConstants.CUSTB).
			antMatchers("/foreign/**").hasAnyRole(RolesConstants.SUPFR).
			antMatchers("/contacts/**").hasAnyRole(RolesConstants.UPDCONT).
			antMatchers("/files/announcements/**").hasAnyRole(RolesConstants.ADMIN, RolesConstants.SUPMX, RolesConstants.SUPFR).
			antMatchers("/announcements/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.SUPFR).
			antMatchers("/contact_us/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.SUPFR, RolesConstants.CUSTB).
			antMatchers("/report/ReportInvoicesStatus/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.SUPFR, RolesConstants.CUSTB).
			antMatchers("/report/InvoicesStatus/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.SUPFR, RolesConstants.CUSTB).
			antMatchers("/payableAccounts/**").hasAnyRole(RolesConstants.PAYAB).
			antMatchers("/payment/**").hasAnyRole(RolesConstants.SUPMX, RolesConstants.PAYAB).
			antMatchers("/ResetPassword/**").permitAll().
			antMatchers("/resetPassword/**").permitAll().
			antMatchers("/ValidateCode/**").permitAll().
			antMatchers("/validateCode/**").permitAll().
			antMatchers("/ChangePassword/**").permitAll().
			antMatchers("/changePassword/**").permitAll().
			antMatchers("/ChangeEmail/**").permitAll().
			antMatchers("/changeEmail/**").permitAll().
			anyRequest().authenticated().
			and().
			formLogin().
				loginPage("/").
				loginProcessingUrl("/login").
				defaultSuccessUrl("/home",true).
			    failureUrl("/?error").
				permitAll().
			and().
			logout().permitAll();
//        .logoutUrl("/perform_logout")
//        .deleteCookies("JSESSIONID")
//			.and()
//			.exceptionHandling().accessDeniedPage("/access-denied");
		
	}
		
}






