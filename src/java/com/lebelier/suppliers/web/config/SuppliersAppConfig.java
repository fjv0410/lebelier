package com.lebelier.suppliers.web.config;

import java.beans.PropertyVetoException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.cayenne.configuration.server.ServerRuntime;
import org.apache.cayenne.datasource.DataSourceBuilder;
import org.apache.cayenne.datasource.PoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.lebelier.suppliers.web.constants.PropetiesConstants;
import com.lebelier.suppliers.web.entity.UserSession;
import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="com.lebelier.suppliers.web")
@PropertySource({"classpath:/com/lebelier/suppliers/web/properties/suppliers_app.properties"})
public class SuppliersAppConfig implements WebMvcConfigurer{
    
	@Autowired
    private Environment environment;

	private Logger logger = Logger.getLogger(getClass().getName());
	
    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
    
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		registry.jsp("/WEB-INF/view/", ".jsp");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/WEB-INF/resources/openui5/");
		registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/resources/css/");
		registry.addResourceHandler("/img/**").addResourceLocations("/WEB-INF/resources/img/");
		registry.addResourceHandler("/js/common/**").addResourceLocations("/WEB-INF/resources/js/common/");
		registry.addResourceHandler("/foreign/js/**").addResourceLocations("/WEB-INF/resources/js/foreign/");
		registry.addResourceHandler("/files/announcements/**").addResourceLocations("file:///" + environment.getProperty(PropetiesConstants.FILES_UPLOAD_ANNOUNCEMENTS_PATH));
	}
	
	@Bean
	public DataSource securityDataSource() {
		
		ComboPooledDataSource securityDataSource = new ComboPooledDataSource();
		
		try {
			securityDataSource.setDriverClass(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_DRIVER));
		} catch (PropertyVetoException e) {
			logger.severe("PropertyVetoException: " + e.getMessage());
			throw new RuntimeException(e);
		}
		
		securityDataSource.setJdbcUrl(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_URL));
		securityDataSource.setUser(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_USERNAME));
		securityDataSource.setPassword(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_PASSWORD));
		
		securityDataSource.setInitialPoolSize(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_POOL_SIZE_INIT));
		securityDataSource.setMinPoolSize(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_POOL_SIZE_MIN));
		securityDataSource.setMaxPoolSize(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_POOL_SIZE_MAX));
		securityDataSource.setMaxIdleTime(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_TIME_IDLE_MAX));
		
		return securityDataSource;
	}
	
	private int getPropertyAsInt(String sName) {
		String sVal = environment.getProperty(sName);
		
		int iVal = Integer.parseInt(sVal);
		
		return iVal;
	}
	
	@Bean
	public JavaMailSender getJavaMailSender() {
	    
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    mailSender.setHost(environment.getProperty(PropetiesConstants.MAIL_SMTP_HOST));
	    mailSender.setPort(Integer.parseInt(environment.getProperty(PropetiesConstants.MAIL_SMTP_PORT)));
	     
	    mailSender.setUsername(environment.getProperty(PropetiesConstants.MAIL_SMTP_USERNAME));
	    mailSender.setPassword(environment.getProperty(PropetiesConstants.MAIL_SMTP_PASSWORD));
	    
	    mailSender.setDefaultEncoding("UTF-8");
	     
	    Properties props = mailSender.getJavaMailProperties();
	    props.put("mail.transport.protocol", environment.getProperty(PropetiesConstants.MAIL_SMTP_PROTOCOL));
	    props.put("mail.smtp.auth", environment.getProperty(PropetiesConstants.MAIL_SMTP_AUTH));
	    props.put("mail.smtp.starttls.enable", environment.getProperty(PropetiesConstants.MAIL_SMTP_TLS_ENABLE));
	    props.put("mail.smtp.from", environment.getProperty(PropetiesConstants.MAIL_SMTP_FROM));
	    props.put("mail.debug", environment.getProperty(PropetiesConstants.MAIL_SMTP_DEBUG));
            
            
	    
	    return mailSender;
	}
	
/*
	@Bean
	public JavaMailSender getJavaMailSender() {
	    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    mailSender.setHost("smtp.lebelier.hu");
	    mailSender.setPort(25);
	     
	    mailSender.setUsername("lbajka\\svc_lbq_support_port");
	    mailSender.setPassword("pWTugtgXu3EMxq0eTyw1");
	     
	    Properties props = mailSender.getJavaMailProperties( );
	    props.put("mail.transport.protocol", "smtp");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.from", "LBQM_SupplierPortal@lebelier.com");
	    props.put("mail.debug", "true");
	    
	    return mailSender;
	}
*/	
	
	@Bean
	public ServerRuntime getServerRuntime() {
		
		PoolingDataSource pds = DataSourceBuilder.url(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_URL)).
				driver(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_DRIVER)).
				userName(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_USERNAME)).
				password(environment.getProperty(PropetiesConstants.DB_SUPPLIERS_JDBC_PASSWORD)).
				pool(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_POOL_SIZE_MIN), getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_POOL_SIZE_MAX)).
				maxQueueWaitTime(getPropertyAsInt(PropetiesConstants.DB_SUPPLIERS_TIME_IDLE_MAX)).
				build();
		
		ServerRuntime cayenneRuntime = ServerRuntime.
									   builder().
									   addConfig("/cayenne-dd_Suppliers.xml").
									   dataSource(pds).
									   build();
		
		return cayenneRuntime;
	}
	
	
	@Bean
	@SessionScope
	public UserSession getUserSession() {
		return new UserSession();
	}
	
}
















