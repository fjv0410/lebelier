package com.lebelier.suppliers.web.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SuppliersSecurityWebApplicationInitializer 
						extends AbstractSecurityWebApplicationInitializer {
	
}
