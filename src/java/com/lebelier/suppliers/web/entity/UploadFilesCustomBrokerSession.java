package com.lebelier.suppliers.web.entity;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.multipart.MultipartFile;

@Component
@SessionScope
public class UploadFilesCustomBrokerSession {
	
	private MultipartFile xmlFile;
	private MultipartFile pdfFile;
	
	private boolean isOwnIvoice;
	
	public MultipartFile getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(MultipartFile xmlFile) {
		this.xmlFile = xmlFile;
	}
	public MultipartFile getPdfFile() {
		return pdfFile;
	}
	public void setPdfFile(MultipartFile pdfFile) {
		this.pdfFile = pdfFile;
	}
	public boolean isOwnIvoice() {
		return isOwnIvoice;
	}
	public void setOwnIvoice(boolean isOwnIvoice) {
		this.isOwnIvoice = isOwnIvoice;
	}
	
}
