package com.lebelier.suppliers.web.entity;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class UploadFilesArrayCutomBrokerSession {
	
	UploadFilesCustomBrokerSession uploadFilesSession[] = new UploadFilesCustomBrokerSession[10];
	
	public void initialize() {
		
		for (int i = 0; i < uploadFilesSession.length; i++) {
			UploadFilesCustomBrokerSession file = uploadFilesSession[i];
		
			if(file == null) {
				uploadFilesSession[i] = new UploadFilesCustomBrokerSession();
			}else {
				file.setPdfFile(null);
				file.setXmlFile(null);
				file.setOwnIvoice(true);
			}
			
		}
		
	}

	public UploadFilesCustomBrokerSession[] getUploadFilesSession() {
		return uploadFilesSession;
	}

	public void setUploadFilesSession(UploadFilesCustomBrokerSession[] uploadFilesSession) {
		this.uploadFilesSession = uploadFilesSession;
	}

}
