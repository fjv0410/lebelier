package com.lebelier.suppliers.web.entity;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.multipart.MultipartFile;

@Component
@SessionScope
public class UploadFilesSession {
	
	private MultipartFile xmlFile;
	private MultipartFile pdfFile;
	
	public MultipartFile getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(MultipartFile xmlFile) {
		this.xmlFile = xmlFile;
	}
	public MultipartFile getPdfFile() {
		return pdfFile;
	}
	public void setPdfFile(MultipartFile pdfFile) {
		this.pdfFile = pdfFile;
	}
	
}
