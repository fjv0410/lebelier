package com.lebelier.suppliers.web.entity;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class UploadFilesArraySession {
	
	UploadFilesSession uploadFilesSession[] = new UploadFilesSession[10];

	public UploadFilesSession[] getUploadFilesSession() {
		return uploadFilesSession;
	}

	public void setUploadFilesSession(UploadFilesSession[] uploadFilesSession) {
		this.uploadFilesSession = uploadFilesSession;
	}
	
	public void initialize() {
		
		for (int i = 0; i < uploadFilesSession.length; i++) {
			UploadFilesSession file = uploadFilesSession[i];
		
			if(file == null) {
				uploadFilesSession[i] = new UploadFilesSession();
			}else {
				file.setPdfFile(null);
				file.setXmlFile(null);
			}
			
		}
		
	}

}
