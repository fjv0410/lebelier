package com.lebelier.suppliers.web.exception;

public class SuppliersCustomException extends SuppliersGeneralException {
	
	protected static final long serialVersionUID = -530130418082592884L;

	protected String frontMessage = "";
	
	public SuppliersCustomException(String p_frontMessage){
		this.setFrontMessage(p_frontMessage); 
	}

	public String getFrontMessage() {
		return this.frontMessage;
	}
	
	protected void setFrontMessage(String frontMessage) {
		this.frontMessage = frontMessage;
	}
	
}
