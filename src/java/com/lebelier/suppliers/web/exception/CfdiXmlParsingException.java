package com.lebelier.suppliers.web.exception;

public class CfdiXmlParsingException extends FileGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "Sucedió un error al leer la información del archivo: {filename}.";
	
	public CfdiXmlParsingException(String p_filename) {
		super(p_filename);
		super.setFrontMessage(frontMessage);
	}

}
