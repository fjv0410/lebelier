package com.lebelier.suppliers.web.exception;

public class ValidationEmptyValueException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "No se ha proporcionado un valor para {field}.";

	protected String field;

	public ValidationEmptyValueException(String p_field){
		this.setField(p_field);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{field}", this.getField());
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
	
}
