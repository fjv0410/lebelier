package com.lebelier.suppliers.web.exception;

public class DB_DeleteException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "Sucedió un error al eliminar la información.";

	public DB_DeleteException(Throwable p_cause){
		super(p_cause);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage;
	}

}
