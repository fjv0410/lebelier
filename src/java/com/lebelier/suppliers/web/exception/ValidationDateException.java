package com.lebelier.suppliers.web.exception;

public class ValidationDateException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "La fecha {date} no es válida.";

	protected String date;

	public ValidationDateException(String p_amount){
		this.setDate(p_amount);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{date}", this.getDate());
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
