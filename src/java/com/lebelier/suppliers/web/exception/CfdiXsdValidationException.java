package com.lebelier.suppliers.web.exception;

public class CfdiXsdValidationException extends FileGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "Sucedió un error al validar estructura de archivo: {filename}.";
	
	public CfdiXsdValidationException(String p_filename, Throwable p_cause) {
		super(p_filename, p_cause);
		super.setFrontMessage(frontMessage);
	}

}
