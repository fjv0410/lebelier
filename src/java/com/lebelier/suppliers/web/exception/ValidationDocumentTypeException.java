package com.lebelier.suppliers.web.exception;

public class ValidationDocumentTypeException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "El archivo {file} tiene un tipo de Comprobante NO válido: {type}";

	protected String file;
	protected String type;
	
	public ValidationDocumentTypeException(String p_file, String p_type){
		this.setFile(p_file);
		this.setType(p_type);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{file}", this.getFile()).replace("{type}", this.getType());
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
