package com.lebelier.suppliers.web.exception;

public class SuppliersGeneralException extends Exception {
	
	protected static final long serialVersionUID = -530130418082592884L;
	
	protected String frontMessage = "Sucedió un error en la aplicación.";
	protected String frontResult = "Error";

	protected SuppliersGeneralException(){
		super();
	}
	
	protected SuppliersGeneralException(Throwable cause){
		super(cause);
	}

	public String getFrontMessage() {
		return frontMessage;
	}
	
	public String getFrontResult() {
		return frontResult;
	}

	protected void setFrontMessage(String frontMessage) {
		this.frontMessage = frontMessage;
	}
	
}
