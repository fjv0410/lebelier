package com.lebelier.suppliers.web.exception;

public class FileGeneralException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;
	
	protected String filename;

	protected FileGeneralException(String p_filename){
		this.setFilename(p_filename);
	}

	protected FileGeneralException(String p_filename, Throwable p_cause){
		super(p_cause);
		this.setFilename(p_filename);
	}
	
	protected String getFilename() {
		return this.filename;
	}

	protected void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{filename}", this.getFilename());
	}

	
}
