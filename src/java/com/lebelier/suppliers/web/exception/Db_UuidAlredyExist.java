package com.lebelier.suppliers.web.exception;

public class Db_UuidAlredyExist extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "El UUID ya se había registrado con anterioridad {UUID}.";
	
	private final String uuid;
	
	public Db_UuidAlredyExist(String sUuid) {
		this.uuid = sUuid;
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{UUID}", this.uuid);
	}
	
}
