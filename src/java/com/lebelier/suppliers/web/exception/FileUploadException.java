package com.lebelier.suppliers.web.exception;

public class FileUploadException extends FileGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "Sucedió un error al subir el archivo: {filename}.";
	
	public FileUploadException(String p_filename, Throwable p_cause) {
		super(p_filename, p_cause);
		super.setFrontMessage(frontMessage);
	}

	public FileUploadException(String p_filename) {
		super(p_filename);
		super.setFrontMessage(frontMessage);
	}
	
}
