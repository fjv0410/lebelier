package com.lebelier.suppliers.web.exception;

public class DB_InsertException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "Sucedió un error al almacenar la información.";

	public DB_InsertException(Throwable p_cause){
		super(p_cause);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage;
	}

}
