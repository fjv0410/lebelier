package com.lebelier.suppliers.web.exception;

public class WebServiceSatException extends SuppliersGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "Error interno de comunicación [WSSATE].";
	
	public WebServiceSatException(Throwable p_cause) {
		super(p_cause);
		super.setFrontMessage(frontMessage);
	}
	
}
