package com.lebelier.suppliers.web.exception;

public class WebServiceSapException extends SuppliersGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "Error interno de comunicación [WSSAPE]";
	
	public WebServiceSapException(Throwable e) {
		super(e);
		super.setFrontMessage(frontMessage);
	}
	
}
