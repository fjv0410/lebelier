package com.lebelier.suppliers.web.exception;

public class DB_QueryException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "Sucedió un error al recuperar la información.";

	public DB_QueryException(Throwable p_cause){
		super(p_cause);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage;
	}

}
