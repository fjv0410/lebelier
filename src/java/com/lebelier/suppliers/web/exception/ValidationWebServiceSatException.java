package com.lebelier.suppliers.web.exception;

public class ValidationWebServiceSatException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "{uuid} No válido: {codigoEstatus}";

	protected String uuid;
	protected String codigoEstatus;
	
	public ValidationWebServiceSatException(String p_uuid, String p_codigoEstatus){
		this.setUuid(p_uuid);
		this.setCodigoEstatus(p_codigoEstatus);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{uuid}", this.getUuid()).replace("{codigoEstatus}", this.getCodigoEstatus());
	}

	public String getCodigoEstatus() {
		return codigoEstatus;
	}

	public void setCodigoEstatus(String codigoEstatus) {
		this.codigoEstatus = codigoEstatus;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
