package com.lebelier.suppliers.web.exception;

public class ValidationAmountException extends SuppliersGeneralException{
	
	private static final long serialVersionUID = 3402133611844913307L;

	protected final String frontMessage = "El monto {amount} no es válido.";

	protected String amount;

	public ValidationAmountException(String p_amount){
		this.setAmount(p_amount);
	}
	
	@Override
	public String getFrontMessage() {
		return frontMessage.replace("{amount}", this.getAmount());
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	
}
