package com.lebelier.suppliers.web.exception;

public class CfdiXsdInvalidException extends FileGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	protected final String frontMessage = "El archivo {filename} NO es un CFDI válido.";

	public CfdiXsdInvalidException(String p_filename) {
		super(p_filename);
		super.setFrontMessage(frontMessage);
	}

}
