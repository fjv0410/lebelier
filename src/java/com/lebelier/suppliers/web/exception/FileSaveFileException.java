package com.lebelier.suppliers.web.exception;

public class FileSaveFileException extends FileGeneralException {

	private static final long serialVersionUID = -2058406185948194114L;
	
	//protected final String frontMessage = "Sucedió un error almacenar el archivo: {filename}.";
        protected final String frontMessage   = "La estructura del xml {filename} no es valida.";

	public FileSaveFileException(String p_filename) {
		super(p_filename);
		super.setFrontMessage(frontMessage);
	}

}
