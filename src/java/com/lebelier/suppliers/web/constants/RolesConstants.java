package com.lebelier.suppliers.web.constants;

public class RolesConstants {
	public static final String ADMIN = "ADMINISTRATOR";
	public static final String PAYAB = "PAYABLE_ACCOUNTS";
	public static final String SUPMX = "SUPPLIER_MX";
	public static final String SUPFR = "SUPPLIER_FOREIGN";
	public static final String CUSTB = "CUSTOM_BROKER";
	
	public static final String UPDCONT = "UPDATE_CONTACTS";
	
	public static final String ROLE_PREFIX = "ROLE_";
}
