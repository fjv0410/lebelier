package com.lebelier.suppliers.web.constants;

public class PropetiesConstants {
	
	public static final String FILES_UPLOAD_CFDI_ARCH_PATH	= "files.uploads.cfdi.arch";
	public static final String FILES_UPLOAD_CFDI_TEMP_PATH	= "files.uploads.cfdi.temp";
	public static final String FILES_UPLOAD_CFDI_FOREIGN	= "files.uploads.cfdi.foreign";
	public static final String FILES_UPLOAD_ANNOUNCEMENTS_PATH = "files.uploads.announcements";
	
	public static final String WS_SAP_SUPPLIERS_WSDL	 = "ws.sap.suppliers.wsdl";
	public static final String WS_SAP_SUPPLIERS_USERNAME = "ws.sap.suppliers.username";
	public static final String WS_SAP_SUPPLIERS_PASSWORD = "ws.sap.suppliers.password";

	public static final String WS_SAT_UUID_WSDL	  = "ws.sat.uuid.wsdl";
	public static final String WS_SAT_UUID_ACTION = "ws.sat.uuid.action";
	
	public static final String DB_SUPPLIERS_JDBC_DRIVER   = "db.suppliers.jdbc.driver";
	public static final String DB_SUPPLIERS_JDBC_URL 	  = "db.suppliers.jdbc.url";
	public static final String DB_SUPPLIERS_JDBC_USERNAME = "db.suppliers.jdbc.username";
	public static final String DB_SUPPLIERS_JDBC_PASSWORD = "db.suppliers.jdbc.password";
	
	public static final String DB_SUPPLIERS_POOL_SIZE_INIT = "db.suppliers.pool.size.init";
	public static final String DB_SUPPLIERS_POOL_SIZE_MIN  = "db.suppliers.pool.size.min";
	public static final String DB_SUPPLIERS_POOL_SIZE_MAX  = "db.suppliers.pool.size.max";
	public static final String DB_SUPPLIERS_TIME_IDLE_MAX  = "db.suppliers.time.idle.max";
	
	public static final String MAIL_SMTP_HOST		= "mail.suppliers.smtp.host";
	public static final String MAIL_SMTP_PORT 		= "mail.suppliers.smtp.port";
	public static final String MAIL_SMTP_USERNAME	= "mail.suppliers.smtp.username";
	public static final String MAIL_SMTP_PASSWORD	= "mail.suppliers.smtp.password";
	public static final String MAIL_SMTP_PROTOCOL	= "mail.suppliers.smtp.protocol";
	public static final String MAIL_SMTP_AUTH		= "mail.suppliers.smtp.auth";
	public static final String MAIL_SMTP_TLS_ENABLE = "mail.suppliers.smtp.tls.enable";
	public static final String MAIL_SMTP_FROM 		= "mail.suppliers.smtp.from";
	public static final String MAIL_SMTP_DEBUG 		= "mail.suppliers.smtp.debug";
	
	public static final String WEB_SUPP_MAIN_URL 	= "web.suppliers.main.url";
	public static final String WEB_SUPP_PASS_RESET_URL 	= "web.suppliers.password.reset.url";
	
	
}
