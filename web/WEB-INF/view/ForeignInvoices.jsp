<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		
		<title>Facturas de Proveedores Extranjeros</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='<c:url value="/resources/sap-ui-core.js" />'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	          
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	          
	   	<script id='foreign-js' src='<c:url value='/foreign/js/ForeignsUtils.js'/>'></script> 
	          
		<script type="text/javascript">

			var oUploaderPdf = new sap.ui.unified.FileUploader(
				"uploaderPdf",
				{
					fileType: "pdf",
					placeholder: "Selecciona PDF",
					uploadUrl: "uploadPdfForeign",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,
					
					iconOnly: true,
					icon: "sap-icon://pdf-attachment",
					
					width: "100%",
					
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					),
					
					uploadComplete: function(oEvent){
						ogBusyInd.close();
					},
					
					
					uploadStart: function(oEvent){
						ogBusyInd.open();
					}
				}
			);
			
			function validateForm(){
				
				oDocumentTypeSelect.setValueState(sap.ui.core.ValueState.None);
				oCompanySelect.setValueState(sap.ui.core.ValueState.None);
				oDeliveryNumber.setValueState(sap.ui.core.ValueState.None);
				oInvoiceNumber.setValueState(sap.ui.core.ValueState.None);
				oInvoiceDate.setValueState(sap.ui.core.ValueState.None);
				oDocumentDate.setValueState(sap.ui.core.ValueState.None);
				oInvoiceAmount.setValueState(sap.ui.core.ValueState.None);
				oInvoiceDescription.setValueState(sap.ui.core.ValueState.None);
				oUploaderPdf.setValueState(sap.ui.core.ValueState.None);
                                oCoinTypeSelect.setValueState(sap.ui.core.ValueState.None);
				
				if(!validateSelectedKey(oDocumentTypeSelect)){
					return false;
				}

				if(!validateSelectedKey(oCompanySelect)){
					return false;
				}

				if(!validateDeliveryNumber(oDeliveryNumber)){
					return false;
				}

				if(!validateEmptyText(oInvoiceNumber)){
					return false;
				}
				
				if(!validateDate(oInvoiceDate)){
					return false;
				}

				if(!validateDate(oDocumentDate)){
					return false;
				}
				
				if(!validateAmount(oInvoiceAmount)){
					return false;
				}
				
				if(!validateEmptyText(oInvoiceDescription)){
					return false;
				}

				if(!validateEmptyText(oUploaderPdf)){
					return false;
				}
                                if(!validateSelectedKey(oCoinTypeSelect)){
                                        return false; 
                                }
                                    
				
				return true;
				
			}
			
			function saveData(){
	
				if(validateForm()){
					
					var sParams = "documentType=" + oDocumentTypeSelect.getSelectedKey() + 
								  "&companyCode=" + oCompanySelect.getSelectedKey() + 
								  "&deliveryNumber=" + oDeliveryNumber.getValue() + 
								  "&invoiceNumber=" + oInvoiceNumber.getValue() + 
								  "&invoiceDate=" + oInvoiceDate.getValue() + 
								  "&documentDate=" + oDocumentDate.getValue() + 
								  "&invoiceAmount=" + oInvoiceAmount.getValue() + 
								  "&invoiceDescription=" + oInvoiceDescription.getValue()+
                                                                  "&coinType="+oCoinTypeSelect.getSelectedKey();

					sParams +=  "&" + "_csrf=" + "${_csrf.token}";

					var sTarget = "foreignInvoices";

					executePostSaveData(
							sTarget, 
							sParams, 
							function(){
								oDocumentTypeSelect.setSelectedKey("");
								oCompanySelect.setSelectedKey("");
								oDeliveryNumber.setValue("");
								oInvoiceNumber.setValue("");
								oInvoiceDate.setValue("");
								oDocumentDate.setValue("");
								oInvoiceAmount.setValue("");
								oInvoiceDescription.setValue("");
								oUploaderPdf.setValue(null);
							}
					);
					
					
				}
				
			}

			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 2,
					columnsL: 7,
					emptySpanL: 3,

					labelSpanM: 3,
					columnsM: 7,
					emptySpanM: 2,

					labelSpanS: 12,
					emptySpanS: 0,
					
					content: [
						
						new sap.m.Label(
							"lblTpoDoc", 
							{
								text:"Tipo de Documento",
								labelFor: "documentTypeSelect",
								required: true
							}
						),
						
						oDocumentTypeSelect,

						new sap.m.Label(
							"lblCompany", 
							{
								text:"Sociedad",
								labelFor: "companySelect",
								required: true
							}
						),
						
						oCompanySelect,
						
						new sap.m.Label(
							"lblDelivery", 
							{
								text:"Orden de Entrega",
								labelFor: "deliveryTxt",
								required: true
							}
						),
						
						oDeliveryNumber,

						new sap.m.Label(
							"lblInvoiceNumber", 
							{
								text:"Número de Factura",
								labelFor: "deliveryTxt",
								required: true
							}
						),
						
						oInvoiceNumber,
						
						new sap.m.Label(
							"lblInvoiceDate", 
							{
								text:"Fecha de Entrada de Mercancía",
								labelFor: "invoiceDate",
								required: true
							}
						),
						
						oInvoiceDate,

						new sap.m.Label(
							"lblDocumentDate", 
							{
								text:"Fecha Factura Física",
								labelFor: "invoiceDate",
								required: true
							}
						),
						
						oDocumentDate,
						
						new sap.m.Label(
							"lblInvoiceAmount", 
							{
								text:"Importe de Factura",
								labelFor: "invoiceAmount",
								required: true
							}
						),
						
						oInvoiceAmount,

						new sap.m.Label(
							"lblDescription", 
							{
								text:"Concepto",
								labelFor: "invoiceDescription",
								required: true
							}
						),
						
						oInvoiceDescription,

						new sap.m.Label(
							"lblPdf", 
							{
								text:"Archivo PDF",
								labelFor: "uploaderPdf",
								required: true
							}
						),
						
						oUploaderPdf,
                                                new sap.m.Label(
							"lblCoin", 
							{
								text:"Moneda",
								labelFor: "coinTypeSelect",
								required: true
							}
						),
						
						oCoinTypeSelect

					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Facturas con Orden de Entrega",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

						new sap.m.Button(
							"btnSubmit",
							{
								type: sap.m.ButtonType.Accept,
								text: "Guardar",
								press: function(){
									saveData();
								}
							}
						)
					]
				}
			);
			
			simpleForm.setToolbar(toolbar);

			simpleForm.placeAt("divForm");
			
		</script>
		
	</head>
	
	<body>
		
		<div id="divForm"></div>
		
	</body>
</html>