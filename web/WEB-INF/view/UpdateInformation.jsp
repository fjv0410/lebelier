<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<!DOCTYPE html>

<html>
  <head>
	<meta charset="UTF-8">
	<title>Facturas sin número de entrega</title>
	
	   	<script id='sap-ui-bootstrap' 
	          src='<c:url value="/resources/sap-ui-core.js" />'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.tnt, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	          
		<script type="text/javascript">
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "<c:url value='/img/puce.png'/>",
				}
			);
			
			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								dialog.close();
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}
			
			function verifyUploadedFile(oEvent){
				
				var sStatusResp = oEvent.getParameter("status");
				
				if (sStatusResp == "200") {
					return true;
				} else {

					var sMsgType = "Error";
					
					var sMsgType = oEvent.getParameters().headers["upload_result"];
					var sMsg = oEvent.getParameters().headers["upload_msg"];
					
			   		var lstValues = new sap.m.List();
		       	  	var item = new sap.m.StandardListItem();
		       	  	
		       	  	var techInfo = oEvent.getParameters().headers["upload_msg_tech"];
		       	  	if(techInfo != null){
			       	   	item.setDescription(techInfo);
		       	  	}
		        	
		        	item.setTitle(sMsg);
		       	  
		           	lstValues.addItem(item);

					createMessage(sMsgType, "Sucedió un error.", lstValues);
					
					return false;
				}
				
			}
			
			var oUploaderAccomplishment = new sap.ui.unified.FileUploader(
				"uploaderAccomplishment",
				{
					fileType: "pdf",
					placeholder: "Selecciona Archivo",
					uploadUrl: "uploadAccomplishment",
					uploadOnChange: false,
					multiple: false,
//					sendXHR: true,

					iconOnly: true,
					icon: "sap-icon://order-status",
					
					width: "100%",
					
					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					),
					
					layoutData: new sap.ui.layout.GridData(
						{span: "L4 M4 S4"}
					),
					
					uploadComplete: function(oEvent){
						var bUploaded = verifyUploadedFile(oEvent);
						if(!bUploaded){
							this.setValue("");
							this.valueState = sap.ui.core.ValueState.Error;
						}
					}
					
				}
			);
			
			var oUploaderBankAccount =  new sap.ui.unified.FileUploader(
				"uploaderBankAccount",
				{
					fileType: "pdf",
					placeholder: "Selecciona Archivo",
					uploadUrl: "uploadBankAccount",
					uploadOnChange: false,
					multiple: false,
//					sendXHR: true,
					
					iconOnly: true,
					icon: "sap-icon://excel-attachment",
					
					width: "100%",
					
					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					),
					
					layoutData: new sap.ui.layout.GridData(
						{span: "L4 M4 S4"}
					),

					uploadComplete: function(oEvent){
						var bUploaded = verifyUploadedFile(oEvent);
						if(!bUploaded){
							this.setValue("");
							this.valueState = sap.ui.core.ValueState.Error;
							
						}
					}
					
				}
			);
			
			function validateForm(){

				oUploaderAccomplishment.setValueState(sap.ui.core.ValueState.None);
				oUploaderBankAccount.setValueState(sap.ui.core.ValueState.None);

				if(oUploaderAccomplishment != null && oUploaderAccomplishment.getValue() == "" ){
					oUploaderAccomplishment.setValueState(sap.ui.core.ValueState.Error);
					oUploaderAccomplishment.setValueStateText("Valor incorrecto.");
					
					return false;
				}

				if(oUploaderBankAccount != null && oUploaderBankAccount.getValue() == "" ){
					oUploaderBankAccount.setValueState(sap.ui.core.ValueState.Error);
					oUploaderBankAccount.setValueStateText("Valor incorrecto.");
					
					return false;
				}
				
				
				return true;
				
			}
			
			function saveData(){

				oBusyInd.open();
				
				if(validateForm()){
					
					var sParams = "accomplishment=" + oUploaderAccomplishment.getValue() + "&" + 
								  "bankAccount=" + oUploaderBankAccount.getValue() ;

					sParams +=  "&" + "_csrf=" + "${_csrf.token}";
					
					var xhr = new XMLHttpRequest();
					xhr.open('POST', "updateInformation", true);
					
					xhr.onreadystatechange = function() {
						
					    if (xhr.readyState == XMLHttpRequest.DONE) {
					    	
					    	if (xhr.status == "200") {
								
					    		try {
						    		
							    	var result = xhr.getResponseHeader("process_result");
							    	var message = xhr.getResponseHeader("process_msg");
							    	
							    	var headers = xhr.getAllResponseHeaders();
							    	var arr = headers.trim().split(/[\r\n]+/);
							    	
							    	var lstValues = new sap.m.List();
							    	
							        arr.forEach(
							       	  function (line) {
								         var parts = line.split(': ');
								         var header = parts.shift();
								         var value = parts.join(': ');
								          
								         if(header.indexOf("process_msg_lst") == 0){
								       	   var item = new sap.m.StandardListItem();
								        	  
								           item.setTitle(value);
								        	  
								           lstValues.addItem(item);
								           
								         }
								          
							          }
							       	);
							    	
							    	createMessage(result, "Resultado del proceso.", lstValues);
							    	
					    		}catch(error) {
							   	  var lstValuesExc = new sap.m.List();
						       	  var itemExc = new sap.m.StandardListItem();
						        	  
						       	  itemExc.setTitle("Excepción en cliente:");
						       	  itemExc.setDescription(error);
						       	  
						          lstValuesExc.addItem(itemExc);
							    	
					      		  createMessage(result, "Sucedió un error.", lstValuesExc);
					    		}
								
					    	}else{
						   		var lstValues = new sap.m.List();
					       	  	var item = new sap.m.StandardListItem();
					        	  
					        	item.setTitle("Estatus de la respuesta: ");
					       	   	item.setDescription(xhr.status);
					       	  
					           	lstValues.addItem(item);

						    	createMessage("Error", "Sucedió un error.", lstValues);
						    	
						    }
					    	
					    	oBusyInd.close();
					    	
					    }
					}
					
					xhr.send(sParams);
					
				}else{
			    	oBusyInd.close();
				}

			}
			
			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 2,
					columnsL: 	8,
					emptySpanL: 2,

					labelSpanM: 2,
					columnsM: 	10,
					emptySpanM: 0,

					labelSpanS: 2,
					emptySpanS: 0,
					
					content: [
						
						new sap.m.Label(
							"lblAccomplishment", 
							{
								text:"Opinión de Cumplimiento",
								labelFor: "uploaderAccomplishment",
								required: true
							}
						),
						
						oUploaderAccomplishment,

						new sap.m.Label(
							"lblBankAccount", 
							{
								text:"Cuenta Bancaria",
								labelFor: "uploaderBankAccount",
								required: true
							}
						),
						
						oUploaderBankAccount,
						
					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Actualización de Información",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

						new sap.m.Button(
							"btnSubmit",
							{
								type: sap.m.ButtonType.Accept,
								text: "Guardar",
								press: function(){
									saveData();
								}
							}
						)
					]
				}
			);
			
			simpleForm.setToolbar(toolbar);
			
			simpleForm.placeAt("divForm");
			
		</script>
		
	
  </head>
  <body>
  		<div id="divForm"></div>
  </body>
</html>