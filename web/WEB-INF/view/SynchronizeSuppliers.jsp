<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Sincronizar Proveedores</title>
	
		<script id='sap-ui-bootstrap' 
		      src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
		      data-sap-ui-theme='sap_bluecrystal'  
		      data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	          
	    <script type="text/javascript">
	
			function synchronize(){
				
				var sParams = "&" + "_csrf=" + "${_csrf.token}";
				
				var sTarget = "synchronizeSuppliers";
				
				executePostSaveData(
						sTarget, 
						sParams, 
						function(){
							
						}
				);
					
			}
			
			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 1,
					columnsL: 	2,
					emptySpanL: 9,
	
					labelSpanM: 2,
					columnsM: 	4,
					emptySpanM: 6,
	
					labelSpanS: 1,
					emptySpanS: 4,
					
					content: [
						
						new sap.m.Label(
							"lblSync", 
							{
								text:"",
								labelFor: "btnSync",
								required: false
							}
						),
						

						new sap.m.Button(
							"btnSync",
							{
								type: sap.m.ButtonType.Accept,
								text: "Sincronizar",
								press: function(){
									synchronize();
								}
							}
						)
						
					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Sincronizar Usuarios",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

					]
				}
			);
			
			simpleForm.setToolbar(toolbar);
			
			simpleForm.placeAt("divForm");
			
	    </script>
	    
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>