<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<!DOCTYPE html>
<html>
	<head>
            <style type="text/css">
                #contactUsAdminItem{
                   color: green !important;
                   font-family: Arial;
                   background-color: #B31414;
                   margin-bottom: 0%;
                   width: 300px;
                   height: 60px;
               }
               #contactUsAdminItemWhitoutMsg{
                   color: green !important;
                   font-family: Arial;
                   margin-bottom: 0%;
                   text-combine-upright: alpha; 
              }
            </style>
		<meta charset="UTF-8">
		<title>Portal de Proveedores - LeBelier</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.tnt, sap.ui.core'></script> 
	          
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	          
	    <script type="text/javascript">
	    
		 	var toolPage = new sap.tnt.ToolPage(
		 		"toolPage",
		 		{
		 			sideExpanded: true
		 		}
		 	);
	 
	 		var toolHeader = new sap.tnt.ToolHeader(
	 			"toolHeader",
	 			{
	 				
	 				content: [
	 					new sap.m.Button(
	 						"btnMenu",
	 						{
		 						icon: "sap-icon://menu2",
		 						type: sap.m.ButtonType.Transparent,
		 						press: function() {
		 							
		 							toolPage.setSideExpanded(!toolPage.getSideExpanded());
		 							
		 						},
		 						layoutData: new sap.m.OverflowToolbarLayoutData(
		 							{
		 								priority: sap.m.OverflowToolbarPriority.NeverOverflow
		 							}
		 						)
	 						}
	 					),
	 					new sap.m.Image(
	 						"ImgLogo",
	 						{
		 						src: "img/puce.png",
	 						}
	 					),
	 					new sap.m.Text(
							"txtTitle",
							{
								text: "Portal de Proveedores",
								wrapping: false
							}
						),
	 					new sap.m.ToolbarSpacer(
							"spacer1",
							{
							}
						),
	 					new sap.m.Text(
	 						"txtName",
	 						{
	 							text: '${user_name} ${user_lastname}'
	 						}
	 					),
	 					new sap.m.Button(
	 						"btnGrid",
	 						{
		 						icon: "sap-icon://log",
		 						type: sap.m.ButtonType.Transparent,
		 						press: function() {
		 							var formLogout = document.getElementById("formLogout");
		 							formLogout.submit();
		 						},
		 						layoutData: new sap.m.OverflowToolbarLayoutData(
		 							{
		 								priority: sap.m.OverflowToolbarPriority.NeverOverflow
		 							}
		 						)
	 						}
	 					)
	 					
	 				]
	 			}
	 		);
	 		
	 		var sideNavigation = new sap.tnt.SideNavigation(
	 			"sideNavigation",
	 			{
					expanded: false,
	 			}
	 		);
	 		
	 		var dynNavigationList = new sap.tnt.NavigationList(
	 			"dynNavigationList",
	 			{
	 			}
	 		);
	 		
	 		var invoicesItem = new sap.tnt.NavigationListItem(
				"invoicesItem",
				{
					text: "Facturas",
					icon: "sap-icon://my-sales-order",
					expanded: false,
					select: function(){
						if(invoicesItem.getExpanded()){
							invoicesItem.setExpanded(false);
						}else{
							invoicesItem.setExpanded(true);
						}
						
					}
				}
			);
	 		
<security:authorize access="hasRole('UPDATE_CONTACTS')">
	 		var contactsInfoItem = new sap.tnt.NavigationListItem(
				"contactsInfoItem",
				{
					text: "Actualizar información",
					icon: "sap-icon://request",
					expanded: false
				}
			);
	 		dynNavigationList.insertItem(contactsInfoItem);
	 		

			var html = new sap.ui.core.HTML(
				{
				  preferDOM: true,
				  content: "<iframe src='contacts/updateInformation' width='100%' height='100%''></iframe>"
				}
			);
			toolPage.destroyMainContents();
			toolPage.addMainContent(html);
			
</security:authorize>

	 		
<security:authorize access="hasAnyRole('SUPPLIER_MX', 'SUPPLIER_FOREIGN', 'CUSTOM_BROKER')">
	 		var invoicesSummaryItem = new sap.tnt.NavigationListItem(
				"invoicesSummaryItem",
				{
					text: "Resumen Estatus de Facturas",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='report/ReportInvoicesStatus' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
	
	 		invoicesItem.insertItem(invoicesSummaryItem);
</security:authorize>

<security:authorize access="hasRole('SUPPLIER_FOREIGN')">

			var invoicesForeignWoDeliveryItem = new sap.tnt.NavigationListItem(
				"invoicesForeignWoDeliveryItem",
				{
					text: "Sin Orden de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='foreign/ForeignInvoicesWoDelivery' width='100%' height='100%''></iframe>"
							  
							}
	
						).attachAfterRendering(
							function(){
						//		ogBusyInd.close()
							}
						);
	
						//ogBusyInd.open();
	
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
						
					},
				
				}
			);
	
	 		invoicesItem.insertItem(invoicesForeignWoDeliveryItem);

	 		var invoicesForeignItem = new sap.tnt.NavigationListItem(
				"invoicesForeignItem",
				{
					text: "Con Orden de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='foreign/ForeignInvoices' width='100%' height='100%''></iframe>"
							  
							}

						).attachAfterRendering(
							function(){
						//		ogBusyInd.close()
								}
						);

						//ogBusyInd.open();

						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
						
					},
				
				}
			);
	
	 		invoicesItem.insertItem(invoicesForeignItem);
</security:authorize>	 		

<security:authorize access="hasRole('CUSTOM_BROKER')">
			var invoicesCustomBroker = new sap.tnt.NavigationListItem(
				"invoicesCustomBroker",
				{
					text: "Registrar Facturas",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='customBroker/InvoicesCustomBroker' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
 		
	 		invoicesItem.insertItem(invoicesCustomBroker);

</security:authorize>


<security:authorize access="hasAnyRole('PAYABLE_ACCOUNTS')">

	var invoicesWoPaymentItem = new sap.tnt.NavigationListItem(
			"invoicesWoPaymentItem",
			{
				text: "Facturas sin Complemento de Pago",
				select: function(){
					var html = new sap.ui.core.HTML(
						{
						  preferDOM: true,
						  content: "<iframe src='payableAccounts/InvoicesWithoutPayment' width='100%' height='100%''></iframe>"
						}
					);
					toolPage.destroyMainContents();
					toolPage.addMainContent(html);
				}
			}
		);
	
	invoicesItem.insertItem(invoicesWoPaymentItem);



		var invoicesAuthItem = new sap.tnt.NavigationListItem(
				"invoicesAuthItem",
				{
					text: "Autorización de Facturas",
					//icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='payableAccounts/AuthorizeInvoices' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
	
		invoicesItem.insertItem(invoicesAuthItem);
		
</security:authorize>

<security:authorize access="hasAnyRole('SUPPLIER_MX')">
			var invoicesWoNumItem = new sap.tnt.NavigationListItem(
				"invoicesWoNumItem",
				{
					text: "Sin Número de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='invoice/InvoicesWithoutDeliveries' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
 		
	 		invoicesItem.insertItem(invoicesWoNumItem);
	 		
			var invoicesWNumItem = new sap.tnt.NavigationListItem(
				"invoicesWNumItem",
				{
					text: "Con Número de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='invoice/InvoicesWithDeliveries' width='100%' height='100%''></iframe>"
							}
						);
						
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
						
					}
				}
			);
	
			invoicesItem.insertItem(invoicesWNumItem);
	
</security:authorize>

<security:authorize access="hasAnyRole('PAYABLE_ACCOUNTS')">
		var invoicesWoNumItem = new sap.tnt.NavigationListItem(
			"travelExpensesItem",
			{
				text: "Gastos de Viaje",
				icon: "sap-icon://my-sales-order",
				select: function(){
					var html = new sap.ui.core.HTML(
						{
						  preferDOM: true,
						  content: "<iframe src='payableAccounts/InvoicesTravelExpenses' width='100%' height='100%''></iframe>"
						}
					);
					toolPage.destroyMainContents();
					toolPage.addMainContent(html);
				}
			}
		);
	
		invoicesItem.insertItem(invoicesWoNumItem);
		
			var invoicesWoNumItem = new sap.tnt.NavigationListItem(
				"invoicesWoNumItem",
				{
					text: "Sin Número de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='payableAccounts/InvoicesWithoutDeliveries' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
                        
                        
	 		invoicesItem.insertItem(invoicesWoNumItem);
	 		
			var invoicesWNumItem = new sap.tnt.NavigationListItem(
				"invoicesWNumItem",
				{
					text: "Con Número de Entrega",
					icon: "sap-icon://my-sales-order",
					select: function(){
						
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='payableAccounts/InvoicesWithDeliveries' width='100%' height='100%''></iframe>"
							}
						);
						
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
						
					}
				}
			);
	
			invoicesItem.insertItem(invoicesWNumItem);
	
</security:authorize>
	 		
<security:authorize access="hasRole('ADMINISTRATOR')">
	 			var tieneMensajes = '${tieneMensajes}';
                                var cantidadMensajes='${cantidadMensajes}'
                                var icono="sap-icon://headset";
                                var idMensajes="contactUsAdminItemWhitoutMsg";
                                var cantidadMensajesAux="("+0+")";
                                if(tieneMensajes && cantidadMensajes>0){
                                    icono="sap-icon://bell"
                                    idMensajes="contactUsAdminItem"
                                    var cantidadMensajesAux="("+cantidadMensajes+")";
                                }
                                

		 		var contactUsAdminItem = new sap.tnt.NavigationListItem(
					idMensajes,
					{
						text: "Mensajes de contacto "+cantidadMensajesAux,
						icon: icono,
                                               
						select: function(){
                                                       
                                                        var html = new sap.ui.core.HTML(
                                                        {
                                                          preferDOM: true,
                                                          content: "<iframe src='admin/ContactUs' width='100%' height='100%''></iframe>"
                                                        }
							);
							toolPage.destroyMainContents();
							toolPage.addMainContent(html);
                                                    }
                                                    
					}
				)
                                
                                
		 		dynNavigationList.insertItem(contactUsAdminItem);
	 			
		 		var announcementsAdminItem = new sap.tnt.NavigationListItem(
					"announcementsAdminItem",
					{
						text: "Administración de Anuncios",
						icon: "sap-icon://marketing-campaign",
						select: function(){
							var html = new sap.ui.core.HTML(
								{
								  preferDOM: true,
								  content: "<iframe src='admin/AnnouncementsAdmin' width='100%' height='100%''></iframe>"
								}
							);
							toolPage.destroyMainContents();
							toolPage.addMainContent(html);
						}
					}
				)
                                
		 		dynNavigationList.insertItem(announcementsAdminItem);

	 		var usersAdminItem = new sap.tnt.NavigationListItem(
				"usersAdminItem",
				{
					text: "Administración de Usuarios",
					icon: "sap-icon://person-placeholder",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='admin/UsersAdministration' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);

	 		dynNavigationList.insertItem(usersAdminItem);

</security:authorize>


<security:authorize access="hasAnyRole('SUPPLIER_MX', 'PAYABLE_ACCOUNTS')">
			var paymentsItem = new sap.tnt.NavigationListItem(
				"paymentsItem",
				{
					text: "Complementos de Pago",
					icon: "sap-icon://money-bills",
					select: function(){

						ogBusyInd.open();
						
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='payment/PaymentComplements' width='100%' height='100%''></iframe>"
							}
						).attachAfterRendering(
							function(){
								ogBusyInd.close()
							}
						);
						
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);
			
	 		dynNavigationList.insertItem(paymentsItem);
</security:authorize>
	 		
<security:authorize access="hasAnyRole('SUPPLIER_MX', 'SUPPLIER_FOREIGN', 'PAYABLE_ACCOUNTS', 'CUSTOM_BROKER')">
	 		dynNavigationList.insertItem(invoicesItem);
</security:authorize>
	 		
	 		var staticNavigationList = new sap.tnt.NavigationList(
	 			"staticNavigationList",
	 			{
	 			}
	 		);

	 		<security:authorize access="hasRole('ADMINISTRATOR')">
	 			 		var syncSyppliersItem = new sap.tnt.NavigationListItem(
	 						"syncSyppliersItem",
	 						{
	 							text: "Sincronizar Proveedores",
	 							icon: "sap-icon://synchronize",
	 							select: function(){
	 								var html = new sap.ui.core.HTML(
	 									{
	 									  preferDOM: true,
	 									  content: "<iframe src='admin/SynchronizeSuppliers' width='100%' height='100%''></iframe>"
	 									}
	 								);
	 								toolPage.destroyMainContents();
	 								toolPage.addMainContent(html);
	 							}
	 						}
	 					);

	 			 		staticNavigationList.insertItem(syncSyppliersItem);
	 		</security:authorize>

<security:authorize access="hasAnyRole('SUPPLIER_MX', 'SUPPLIER_FOREIGN', 'CUSTOM_BROKER')">
	 		var contactUsItem = new sap.tnt.NavigationListItem(
				"contactUsItem",
				{
					text: "Contáctanos",
					icon: "sap-icon://headset",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='contact_us/Messages' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);

	 		staticNavigationList.insertItem(contactUsItem);
</security:authorize>

<security:authorize access="hasAnyRole('SUPPLIER_MX', 'SUPPLIER_FOREIGN')">
	 		var announcementsItem = new sap.tnt.NavigationListItem(
				"announcementsItem",
				{
					text: "Anuncios Importantes",
					icon: "sap-icon://marketing-campaign",
					select: function(){
						var html = new sap.ui.core.HTML(
							{
							  preferDOM: true,
							  content: "<iframe src='announcements/Supplier' width='100%' height='100%''></iframe>"
							}
						);
						toolPage.destroyMainContents();
						toolPage.addMainContent(html);
					}
				}
			);

	 		staticNavigationList.insertItem(announcementsItem);
</security:authorize>
	
	 		sideNavigation.setItem(dynNavigationList);
	 		sideNavigation.setFixedItem(staticNavigationList);
	 		
	 		var mainContent = new sap.m.Image(
				"ImgLeBelier",
				{
					src: "img/LeBelier-logo.png",
					width: "100%"
				}
			);
	 		
	 		toolPage.setHeader(toolHeader);
	 		toolPage.setSideContent(sideNavigation);
	 		
	 		toolPage.addMainContent(mainContent);
	 		
	 		toolPage.placeAt("menuContainer");
	 		
	    </script>
	     
	</head>
	<body>
		<div id="menuContainer"></div>
		
		<form:form id="formLogout" action="logout" method="POST"></form:form>
		
	</body>
</html>