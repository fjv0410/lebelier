<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Administración de Usuarios</title>
		
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
		<script type="text/javascript">
			var oModelUsers = new sap.ui.model.json.JSONModel("getUsersCatalog");
			var oModelSuppliers = new sap.ui.model.json.JSONModel("getSuppliersCatalog");
			var oModelRoles = new sap.ui.model.json.JSONModel("getRolesCatalog");

			oModelUsers.setSizeLimit(2000);
			oModelSuppliers.setSizeLimit(2000);
			
			function validateEmail(email) {
			  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			  return re.test(email);
			}
			
			function validateEmptyText(currentField){
				if(currentField.getValue().trim() == ""){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("El campo está vacío.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				return true;
			}
			
			function validateUserForm(){
				var currentField = oName;
				
				if(!validateEmptyText(currentField)){
					return false;
				}
				
				currentField = oLastname;

				if(!validateEmptyText(currentField)){
					return false;
				}
				
				currentField = oEmail;

				if(!validateEmptyText(currentField)){
					return false;
				}
				
				if(!validateEmail(currentField.getValue().trim())){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("Correo Electrónico no válido.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);

				currentField = oSupplier;

				if(!validateEmptyText(currentField)){
					return false;
				}
				
				var suppFound = currentField.getAggregation("suggestionItems").find(
					function(supplier) {
						return (currentField.getValue() == supplier.mProperties.text);
					}
				);
				
				if(!suppFound){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("Valor no corresponde con catálogo.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);

				currentField = oRole;
				
				if(currentField.getSelectedKey() == "0"){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("Seleccione un valor válido.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				return true;
				
			}
			
			function saveUser(p_action, p_id, p_state, p_username){
				
				oBusyInd.open();
				
				var sParams = "";
				var target = "";
				
				sParams = "name=" + oName.getValue() + "&" +
					"lastname=" + oLastname.getValue() + "&" +
					"email=" + oEmail.getValue() + "&"; 
//					"username=" + oUsername.getValue() + "&";
				
				if(p_action == "new"){
					target = "createUser";
					sParams += "supplier=" + oSupplier.getSelectedKey() + "&" +
						"role=" + oRole.getSelectedKey();
				}else if(p_action == "edit"){
					target = "updateUser";
					sParams += "id=" + p_id;
				}else if(p_action == "enable"){
					target = "enableUser";
					sParams = "id=" + p_id + "&"  +
					  		  "username=" + p_username + "&"  +
							  "enable=" + p_state;
				}else if(p_action = "delete"){
					target = "deleteUser";
					sParams = "id=" + p_id + "&"  +
					  		  "username=" + p_username;
				}
				
				sParams +=  "&" + "_csrf=" + "${_csrf.token}";
				
				var xhr = new XMLHttpRequest();
				xhr.open('POST', target, true);
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				
				xhr.onreadystatechange = function() {
					
				    if (xhr.readyState == XMLHttpRequest.DONE) {
				    	
				    	if (xhr.status == "200") {
							
				    		try {
					    		
						    	var result = xhr.getResponseHeader("process_result");
						    	var message = xhr.getResponseHeader("process_msg");
						    	
						    	var headers = xhr.getAllResponseHeaders();
						    	var arr = headers.trim().split(/[\r\n]+/);
						    	
						    	var lstValues = new sap.m.List();
						    	
						        arr.forEach(
						       	  function (line) {
							         var parts = line.split(': ');
							         var header = parts.shift();
							         var value = parts.join(': ');
							          
							         if(header.indexOf("process_msg_lst") == 0){
							       	   var item = new sap.m.StandardListItem();
							        	  
							           item.setTitle(value);
							        	  
							           lstValues.addItem(item);
							           
							         }
							          
						          }
						       	);
						    	
						    	createMessage(result, "Resultado del proceso.", lstValues);
						    	
				    		}catch(error) {
						   	  var lstValuesExc = new sap.m.List();
					       	  var itemExc = new sap.m.StandardListItem();
					        	  
					       	  itemExc.setTitle("Excepción en cliente:");
					       	  itemExc.setDescription(error);
					       	  
					          lstValuesExc.addItem(itemExc);
						    	
				      		  createMessage(result, "Sucedió un error.", lstValuesExc);
				    		}
							
				    	}else{
					   		var lstValues = new sap.m.List();
				       	  	var item = new sap.m.StandardListItem();
				        	  
				        	item.setTitle("Estatus de la respuesta: ");
				       	   	item.setDescription(xhr.status);
				       	  
				           	lstValues.addItem(item);

					    	createMessage("Error", "Sucedió un error.", lstValues);
					    	
					    }
				    	
				    	oBusyInd.close();
				    	
				    }
				}
				
				xhr.send(sParams);
				
			}
			
			var oName = new sap.m.Input(
				"txtName",
				{
					type: sap.m.InputType.Text,
					required: true,
					width: "100%",
					maxLength: 50
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oLastname = oName.clone("txtLastname");

			var oEmail = new sap.m.Input(
				"txtEmail",
				{
					type: sap.m.InputType.Email,
					required: true,
					width: "100%",
					maxLength: 50
				}
			).addStyleClass("sapUiSmallMarginBottom");

			//var oUsername = oName.clone("txtUsername");

			var oSupplier = new sap.m.Input(
				"txtSuppliers",
				{
					placeholder: "Buscar Proveedor...",
					showSuggestion: true,
					
					suggestionItems:{
						path:"/",

						templateShareable: false,

						template: new sap.ui.core.ListItem(
							"listItemSuppliers",
							{
								key: "{id}",
								text: "{name}",
								additionalText: "{idSap}"
							}
						)
					}
				}
			).setModel(oModelSuppliers);

			var oRole = new sap.m.Select(
				"selectRole",
				{
					forceSelection: true,
					
					width: "100%",
					
					items: {
						path: "/",
						
						template: new sap.ui.core.ListItem(
							"listItemRoles",
							{
								key: "{id}",
								text:"{name}"
							}
						)
					}
					
				}
			).setModel(oModelRoles);
			
			var pageUserRegistration = new sap.m.Page(
				"pageUserRegistration",
				{
					showHeader: false,
					showFooter: false,
					
					content: [
						new sap.m.VBox(
							"vBoxNewUser", 
							{
								fitContainer: true,
								
								alignItems: sap.m.FlexAlignItems.Stretch, 
								
								items: [ 

									new sap.m.Label(
										"lblNameNewUser", 
										{
											text:"Nombre",
											labelFor: "txtName",
											required: true
										}
									),
									
									oName,

									new sap.m.Label(
										"lblLastnameNewUser", 
										{
											text:"Apellido",
											labelFor: "txtLastname",
											required: true
										}
									),
									
									oLastname,

									new sap.m.Label(
										"lblEmailNewUser", 
										{
											text:"Correo Electrónico",
											labelFor: "txtEmail",
											required: true
										}
									),
									
									oEmail,
									
									new sap.m.Label(
										"lblSupplierNewUser", 
										{
											text:"Proveedor",
											labelFor: "txtSuppliers",
											required: true
										}
									),
									
									oSupplier,

									new sap.m.Label(
										"lblRoleNewUser", 
										{
											text:"Role",
											labelFor: "selRole",
											required: true
										}
									),
									
									oRole
									
								]
							}
						)
						
					]
				}
			);
			
			var oDialogEditUser = new sap.m.Dialog(
				"dialogEditUser",
				{
					title: 'Registrar Usuario.',
					
					stretch: true,
					
					content: [
						pageUserRegistration
					],
					
					beginButton: new sap.m.Button(
						"btnSaveUser",
						{
							text: 'Guardar',
							press: function (oEvent) {
								var sEditMode = oEvent.getSource().getParent().getCustomData()[0].mProperties.value;

								if(validateUserForm()){
									if(sEditMode == "new"){
										saveUser("new");
									}else if(sEditMode == "edit"){
										var sId = oEvent.getSource().getParent().getCustomData()[1].mProperties.value;
										saveUser("edit", sId);
									}
								}
								
							}
						}
					),
					
					endButton: new sap.m.Button(
						"brnCancelSaveUser",
						{
							text: 'Cancelar',
							press: function (oEvent) {
								var sParentId = oEvent.getSource().getParent().sId;
								oDialogEditUser.close();
							}
						}
					)
				
				}
			);
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "${pageContext.request.contextPath}/img/puce.png",
				}
			);

			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								dialog.close();
								
								if(sMsgType == "Success" || sMsgType == "Warning"){
									
									oDialogEditUser.close();
									
									tblUsers.setModel(new sap.ui.model.json.JSONModel("getUsersCatalog"));
									
								}
								
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}
			
			var tblUsers = new sap.ui.table.Table(
				"tblUsers",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 15,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://my-view",
										alt: "Usuarios",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Administración de Usuarios", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					toolbar: new sap.m.OverflowToolbar(
						"toolbar", 
						{
							content: [
								
								new sap.m.ToolbarSpacer( ),
								
								new sap.m.Button(
									"btnNewUser",
									{
										icon: "sap-icon://add-employee", 
										tooltip: "Crear Usuario",
										press: function(){
											
											oDialogEditUser.setTitle('Registrar Usuario.');
											oDialogEditUser.removeAllCustomData();
											oDialogEditUser.addCustomData(
												new sap.ui.core.CustomData(
													{
														key: "mode",
														value: "new"
													}
												)
											)
											
											oDialogEditUser.open();
											
											oName.setValue("");
											oLastname.setValue("");
											oEmail.setValue("");
											//oUsername.setValue("");
											oSupplier.setValue("");
											oRole.setSelectedKey(0);
											
										}
									}
								)
								
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
							"colId", 
							{
								//visible: false,
								width: "0rem",
								label: new sap.m.Label(
									"lblId",
									{
										text: ""
									}
								),
								template: new sap.ui.core.InvisibleText(
									"textId",
									{
										text: "{id}"
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colName", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblName",
									{
										text: "Nombre"
									}
								),
								template: new sap.m.Text(
									"textName",
									{
										text: "{name}",
										wrapping: true
									}
								),filterProperty: "name",
                                                                  sortProperty: 'name'
							}
						),
						new sap.ui.table.Column(
							"colEmail", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblEmail",
									{
										text: "Email"
									}
								),
								template: new sap.m.Text(
									"textEmail",
									{
										text: "{email}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colUsername", 
							{
								width: "13rem",
								label: new sap.m.Label(
									"lblUsername",
									{
										text: "Usuario Portal"
									}
								),
								template: new sap.m.Text(
									"textUsername",
									{
										text: "{username}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colSupplier", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblSupplier",
									{
										text: "Proveedor"
									}
								),
								template: new sap.m.Text(
									"textSupplier",
									{
										text: "{supplierName}",
										wrapping: true
									}
								),filterProperty: "supplierName",
                                                                  sortProperty: 'supplierName'
							}
						),
						new sap.ui.table.Column(
							"colStatus", 
							{
								width: "5rem",
								label: new sap.m.Label(
									"lblStatus",
									{
										text: "Habilitar"
									}
								),
								template: new sap.m.Switch(
									"switchStatus",
									{
										type: sap.m.SwitchType.AcceptReject,
										state: "{enabled}",
										customData: [
											new sap.ui.core.CustomData(
												{ key: "id", value: "{id}" }
											),
											new sap.ui.core.CustomData(
												{ key: "username", value: "{username}" }
											)
										],
										change: function(oEvent) {
											var oCustomData = oEvent.getSource().getCustomData();
											var id = oCustomData[0].mProperties.value;
											var username = oCustomData[1].mProperties.value;
											var state = oEvent.getSource().getState();
											
											saveUser("enable", id, state, username);
											
										}
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colEdit", 
							{
								width: "4rem",
								label: new sap.m.Label(
									"lblEdit",
									{
										text: "Editar"
									}
								),
								template:  new sap.m.Button(
									"btnEdit",
									{
										icon: "sap-icon://user-edit", 
										tooltip: "Modificar Usuario",
										customData: new sap.ui.core.CustomData(
											{ key: "id", value: "{id}" }
										),
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											var id = oCustomData[0].mProperties.value;
											
											if(id){

												var oModelUser = new sap.ui.model.json.JSONModel("getUser?id=" + id);
												oModelUser.attachRequestCompleted(
													function(){
														var oProp = oModelUser.getProperty("/");
														oName.setValue(oProp.name);
														oLastname.setValue(oProp.lastname);
														oEmail.setValue(oProp.email);
														//oUsername.setValue(oProp.username);
														oSupplier.setSelectedKey(oProp.supplierId);
														oRole.setSelectedKey(oProp.roleId);

														//oUsername.setEnabled(false);
														oSupplier.setEnabled(false);
														oRole.setEnabled(false);
														
													}
												);

												oDialogEditUser.setTitle('Actualizar Usuario.');
												oDialogEditUser.removeAllCustomData();
												oDialogEditUser.addCustomData(
													new sap.ui.core.CustomData(
														{
															key: "mode",
															value: "edit"
														}
													)
												);
												oDialogEditUser.addCustomData(
													new sap.ui.core.CustomData(
														{
															key: "id",
															value: id
														}
													)
												);
												
												oDialogEditUser.open();
												
											}
												
										}
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colDelete", 
								{
									width: "5rem",
									label: new sap.m.Label(
										"lblDelete",
										{
											text: "Eliminar"
										}
									),
									template:  new sap.m.Button(
										"btnDelete",
										{
											icon: "sap-icon://delete", 
											tooltip: "Eliminar Usuario",

											customData: [
												new sap.ui.core.CustomData(
													{ key: "id", value: "{id}" }
												),
												new sap.ui.core.CustomData(
													{ key: "username", value: "{username}" }
												)
											],
											
											press: function(oEvent){
												var oCustomData = oEvent.getSource().getCustomData();
												var id = oCustomData[0].mProperties.value;
												var username = oCustomData[1].mProperties.value;
												
												saveUser("delete", id, null, username);
													
											}
										}
									)
								}
							)
					
					],
					
				}
				
			).setModel(oModelUsers);
			
			
			tblUsers.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>