<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Facturas sin Complemento de Pago</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	          
		<script type="text/javascript">
		
			var oModeInvoices = new sap.ui.model.json.JSONModel();
			
			oModeInvoices.attachRequestSent(
					function(){
						ogBusyInd.open();
					}
				).attachRequestFailed(
					function(oEvent){
						ogBusyInd.close();
						
				   		var list = new sap.m.List();
			       	  	var item = new sap.m.StandardListItem();
			        	  
			        	item.setTitle("Sucedió un error al cargar los datos.");
			       	  
			        	list.addItem(item);

						createMessage("Error", "Error", list);
						
					}
				).attachRequestCompleted(
					function(oEvent){
						ogBusyInd.close();
					}
				);
			
			oModeInvoices.loadData("InvoicesWithoutPayments");
			
			var tblInvoices = new sap.ui.table.Table(
				"tblInvoices",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: '{/}',
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										alt: "Facturas sin Complemento de Pago",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Facturas sin Complemento de Pago", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
								"colSupplier", 
								{
									width: "8rem",
									label: new sap.m.Label(
										"lblSupplier",
										{
											text: "Número de Proveedor",
											wrapping: true
										}
									),
									template: new sap.m.Text(
										"textSupplier",
										{
											text: "{supplier}",
											wrapping: true
										}
									)
								}
							),
						new sap.ui.table.Column(
							"colReference", 
							{
								width: "8rem",
								label: new sap.m.Label(
									"lblReference",
									{
										text: "Documento Referencia SAP",
										wrapping: true
									}
								),
								template: new sap.m.Text(
									"textReference",
									{
										text: "{reference}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colFolio", 
							{
								width: "8rem",
								label: new sap.m.Label(
									"lblFolio",
									{
										text: "Folio Factura",
										wrapping: true
									}
								),
								template: new sap.m.Text(
									"texFolio",
									{
										text: "{folio}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colCompany", 
							{
								width: "7rem",
								label: new sap.m.Label(
									"lblCompany",
									{
										text: "Sociedad"
									}
								),
								template: new sap.m.Text(
									"textCompany",
									{
										text: "{company}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colPaymentDoc", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblPaymentDoc",
									{
										text: "Documento de Compensación SAP",
										wrapping: true
									}
								),
								template: new sap.m.Text(
									"textPaymentDoc",
									{
										text: "{paymentDoc}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colPaymentDate", 
							{
								width: "7rem",
								label: new sap.m.Label(
									"lblPaymentDate",
									{
										text: "Fecha de Pago",
										wrapping: true
									}
								),
								template: new sap.m.Text(
									"textPaymentDate",
									{
										text: "{paymentDate}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colAmount", 
								{
									width: "7rem",
									label: new sap.m.Label(
										"lblAmount",
										{
											text: "Importe"
										}
									),
									template: new sap.m.Text(
										"textAmount",
										{
											text: "{amount}",
											wrapping: true
										}
									)
								}
							)
					
					],
					
				}
				
			).setModel(oModeInvoices);
			
			
			tblInvoices.placeAt("divForm");
			
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>