<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cambiar Contraseņa</title>
	
	<style type="text/css">
		
		body{
			font-family: 'Roboto', sans-serif;
			width: 420px;
			padding: 0 8% 40px;
			margin: 20px 0;
		}
		
		input[type="text"], input[type="password"]{	
			min-width: 100px;
			border: 1px solid #00204B;
			padding: 6px;
			margin: 5px 0;
			width: 100%;
			font-size: 14px;
			box-sizing: border-box;
			min-height: 35px;
		}
		
		a{
			color: #4b701b;
		}
		
		a:hover {
			text-decoration: none !important;
			color: #4b701b;
		}
		
		.button{
			text-transform: uppercase;
			font-size: 14px;
			width: 100%;
			height: 34px;
			margin: 0;
			
			background : #4b701b !important ;
			color : #fff !important ;
			
			font-weight : bold ; 
			padding : 2px 3px ;
		}
		
		.title{
		
			line-height:1.2em;
			
			padding:5px 0px ;
			
			text-align: left;
			padding-top: 5px;
			
			padding-left: 40px;
			
			font-size: 23px;
			font-weight: 400;
			background:transparent;
			margin-bottom:25px;
			color: #4a6725;
			
		}
		
	</style>
	
</head>
	<body id="body">
	
		<div class="page">
			
			<div id="contain">
				
				<div id="LeftForm" class="form2cols">
	
					<div>
						<div align="center">
							<span class="title">Cambiar Contraseņa</span>
						</div>
					</div>
	
					<div class="contain2">
	
						<!-- Login Form -->
						<form:form action="changePassword" method="POST">
						
							<div>
								<input type="password" name="password" placeholder="Contraseņa">
							</div>
							
							<div>
								<input type="password" name="confirmation" placeholder="Confirmar Contraseņa">
							</div>
							
							<div>
								<input type="hidden" name="id" value="<%=request.getParameter("id")%>">
								<input type="hidden" name="code" value="<%=request.getParameter("code")%>">
							</div>
							
							<div>
								<button type="submit" class="c button">Enviar</button>
							</div>
							
							<br/>
							<div>${result_msg_success}</div>
							<div>${result_msg_error}</div>
							
						</form:form>
	
					</div>
	
				</div>
	
			</div>
	
		</div>
	
	</body>
</html>