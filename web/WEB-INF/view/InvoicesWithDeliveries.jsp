<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		
		<title>Facturas con Número de Entrega</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='<c:url value="/resources/sap-ui-core.js" />'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.tnt, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	          
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	   	
		<script type="text/javascript">
		
			var GV_DELIV_CURR_ID = 1;
			var GV_DELIV_COUNT = 1;
			
			var GV_ARR_DELIV_OBJ = [null, null, null, null, null];
			var GV_ARR_DATES_OBJ = [null, null, null, null, null];
			
			var oDelivNumbTempl = new sap.m.MaskInput(
				{
					mask: "9999999999",
					placeholderSymbol: " ",
					placeholder: "XXXXXXXXXX",
					required: true,
					name: "deliveryNumber",
					width: "100%"
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oDelivDateTempl = new sap.m.DatePicker(
				{
					displayFormat: "dd/MM/yyyy",
					valueFormat: "yyyy-MM-dd",
					required: true,
					placeholder: "dd/mm/yyyy",
					name: "deliveryDate",
					width: "100%"
					
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oUploaderXml = new sap.ui.unified.FileUploader(
				"xmlFile",
				{
					fileType: "xml",
					placeholder: "Selecciona XML",
					uploadUrl: "uploadXmlWithDeliveries",
					buttonText: "Selecciona...",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,

					iconOnly: true,
					icon: "sap-icon://attachment-html",
					
					width: "100%",

					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					)
					
				}
			);
			
			var oUploaderPdf =  new sap.ui.unified.FileUploader(
				"pdfFile",
				{
					fileType: "pdf",
					placeholder: "Selecciona PDF",
					uploadUrl: "uploadPdfWithDeliveries",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,
					
					iconOnly: true,
					icon: "sap-icon://pdf-attachment",
					
					width: "100%",

					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					)
					
				}
			);

			function validateForm(){
				
				var lv_index = 0;
				var lo_deliv_numb, lo_deliv_date;
				
				oDelivDateTempl.setValueState(sap.ui.core.ValueState.None);
				oUploaderXml.setValueState(sap.ui.core.ValueState.None);
				oUploaderPdf.setValueState(sap.ui.core.ValueState.None);
				
				for(lv_index = 0; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
					lo_deliv_numb = GV_ARR_DELIV_OBJ[lv_index];
					lo_deliv_date = GV_ARR_DATES_OBJ[lv_index];
					
					if(lo_deliv_date != null){
						lo_deliv_date.setValueState(sap.ui.core.ValueState.None);
					}
					
					if(lo_deliv_numb != null){
						lo_deliv_numb.setValueState(sap.ui.core.ValueState.None);
					}
					
				}
				
				for(lv_index = 0; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
					lo_deliv_numb = GV_ARR_DELIV_OBJ[lv_index];
					
					if(lo_deliv_numb != null){
						
						for(var j = lv_index + 1; j < GV_ARR_DELIV_OBJ.length; j++ ){
							var lv_deliv_aux = GV_ARR_DELIV_OBJ[j];

							if(lv_deliv_aux != null){
								if(lo_deliv_numb.getValue() == lv_deliv_aux.getValue()){
									lo_deliv_numb.setValueState(sap.ui.core.ValueState.Error);
									lo_deliv_numb.setValueStateText("Entrega duplicada.");
									lv_deliv_aux.setValueState(sap.ui.core.ValueState.Error);
									lv_deliv_aux.setValueStateText("Entrega duplicada.");

									return false;
									
								}
							}
						}
						
					}
					
				}

				oUploaderXml.setValueState(sap.ui.core.ValueState.None);
				if(!validateEmptyText(oUploaderXml)){
					return false;
				}

				oUploaderPdf.setValueState(sap.ui.core.ValueState.None);
				if(!validateEmptyText(oUploaderPdf)){
					return false;
				}
				
				for(lv_index = 0; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
					lo_deliv_numb = GV_ARR_DELIV_OBJ[lv_index];
					lo_deliv_date = GV_ARR_DATES_OBJ[lv_index];
					
					if(lo_deliv_date != null){
						lo_deliv_date.setValueState(sap.ui.core.ValueState.None);
						if(!validateEmptyText(lo_deliv_date) || !validateDate(lo_deliv_date)){
							return false;
						}
					}

					if(lo_deliv_numb != null){
						lo_deliv_numb.setValueState(sap.ui.core.ValueState.None);
						if(!validateEmptyText(lo_deliv_numb) || !validateDeliveryNumber(lo_deliv_numb)){
							return false;
						}
					}
					
				}
				
				return true;
				
			}
			
			function saveData(){
				
				if(validateForm()){
					
					var lv_index = 0;
					var lo_deliv_numb;
					var sParams;
					
					for(lv_index = 0; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
						lo_deliv_numb = GV_ARR_DELIV_OBJ[lv_index];
						lo_deliv_date = GV_ARR_DATES_OBJ[lv_index];
						
						if(lo_deliv_numb != null){
							sParams += "&delivery=" + lo_deliv_numb.getValue();
						}

						if(lo_deliv_date != null){
							sParams += "@" + lo_deliv_date.getValue();
						}
						
					}
					
					sParams +=  "&" + "_csrf=" + "${_csrf.token}";

					var sTarget = "invoiceWithDeliveries";
					
					executePostSaveData(
							sTarget, 
							sParams, 
							function(){
								oUploaderXml.setValue(null);
								oUploaderPdf.setValue(null);

								for(lv_index = 0; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
									lo_deliv_numb = GV_ARR_DELIV_OBJ[lv_index];
									lo_deliv_date = GV_ARR_DATES_OBJ[lv_index];
									
									if(lo_deliv_numb != null){
										lo_deliv_numb.setValue("");
									}

									if(lo_deliv_date != null){
										lo_deliv_date.setValue("");
									}
									
								}
								
							}
					);
					
				}
				
			}
			
			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 2,
					columnsL: 7,
					emptySpanL: 3,

					labelSpanM: 3,
					columnsM: 7,
					emptySpanM: 2,

					labelSpanS: 12,
					emptySpanS: 0,
					
					content: [
						
						new sap.m.Label(
							"lblUploadXml", 
							{
								text:"Archivo XML",
								labelFor: "uploaderXml",
								required: true
							}
						),
						
						oUploaderXml,
							
						new sap.m.Label(
							"lblUploadPdf", 
							{
								text:"Archivo PDF",
								labelFor: "uploaderPdf",
								required: true
							}
						),
						
						oUploaderPdf,
						
					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Registro de Facturas con Número de Entrega",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

						new sap.m.Button(
							"btnSubmit",
							{
								type: sap.m.ButtonType.Accept,
								text: "Guardar",
								press: function(){
									saveData();
								}
							}
						)
					]
				}
			);
			
			function insertDeliveryNumber(){

				var lv_index = 0;
				
				for(; lv_index < GV_ARR_DELIV_OBJ.length; lv_index++ ){
					if(GV_ARR_DELIV_OBJ[lv_index] == null){
						break;
					}
				}
				
				var lv_lblId = "lblNumber_" + lv_index;
				var lv_txtId = "deliveryNumber_" + lv_index;
				var lv_DateId = "deliveryDate_" + lv_index;
				var lv_btnRemId = "remove_" + lv_index;
				var lv_btnAddId = "add_" + lv_index;
				
				var lv_txtInvId = "hidd_" + lv_index;
				
				var lo_lbl = new sap.m.Label(
						lv_lblId, 
						{
							text: (lv_index == 0 ? "Entrega(s)" : ""),
							labelFor: lv_txtId
						}
					);
				
				var lo_txt = oDelivNumbTempl.clone(lv_txtId);
				lo_txt.setLayoutData(
					new sap.ui.layout.GridData(
							{span: "L3 M3"}
						)
				);

				var lo_date = oDelivDateTempl.clone(lv_DateId);
				lo_date.setLayoutData(
						new sap.ui.layout.GridData(
								{span: "L3 M3"}
							)
					);

				simpleForm.addContent( lo_lbl );

				simpleForm.addContent( lo_date );
				
				simpleForm.addContent( lo_txt );

				if( lv_index == 0 ){
					
					var lo_btn_add = new sap.m.Button(
							lv_btnAddId,
							{
								icon: "sap-icon://add",
								width: "38px",
								
								press: function(){
									
									if(GV_DELIV_COUNT != "5"){
										
										insertDeliveryNumber();
										
										GV_DELIV_COUNT = GV_DELIV_COUNT + 1;
										
									}
									
								}
								
							}
						);
					
					lo_btn_add.setLayoutData(
							new sap.ui.layout.GridData(
									{span: "L1 M2"}
								)
						);
					
					simpleForm.addContent( lo_btn_add );
					
				}else{
					var lo_btn_rem = new sap.m.Button(
						lv_btnRemId,
						{
							icon: "sap-icon://less",
							width: "38px",
	
							press: function(oEvent){
								lo_lbl.destroy();
								lo_txt.destroy();
								lo_btn_rem.destroy();
								lo_txtInv.destroy();
								lo_date.destroy();
								
								GV_ARR_DELIV_OBJ[lo_txtInv.getText()] = null;
								GV_ARR_DATES_OBJ[lo_txtInv.getText()] = null;
	
								GV_DELIV_COUNT = GV_DELIV_COUNT - 1;
								
							}
							
						});

					lo_btn_rem.setLayoutData(
							new sap.ui.layout.GridData(
									{span: "L1 M2"}
								)
						);
					
					
					simpleForm.addContent( lo_btn_rem );

				}
				
				var lo_txtInv = new sap.m.Text(
						lv_txtInvId, 
						{
							text: lv_index,
							visible: false
						}
					);
				
				simpleForm.addContent( lo_txtInv );
				
				GV_ARR_DELIV_OBJ[lv_index] = lo_txt;
				GV_ARR_DATES_OBJ[lv_index] = lo_date;
				
			}
			
			insertDeliveryNumber();
			
			simpleForm.setToolbar(toolbar);
			
			simpleForm.placeAt("divForm");
			
		</script>
		
	</head>
	
	<body>
		
		<div id="divForm"></div>
		
	</body>
</html>