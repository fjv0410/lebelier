<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Resumen Estatus de Facturas</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	   	<script id='jsonmodel-js' src='<c:url value='/js/common/JsonModelUtils.js'/>'></script> 
	          
		<script type="text/javascript">
		
			ogModel.loadData("InvoicesStatus");
			
			var tblInvoices = new sap.ui.table.Table(
				"tblInvoices",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: '{/0/lbq}',
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://sales-order",
										alt: "Facturas",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Resumen de Estatus de Facturas", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
								"colCompany", 
								{
									width: "6rem",
									label: new sap.m.Label(
										"lblCompany",
										{
											text: "Sociedad"
										}
									),
									template: new sap.m.Text(
										"textCompany",
										{
											text: "{company}",
											wrapping: true
										}
									)
								}
							),
						new sap.ui.table.Column(
							"colReference", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblReference",
									{
										text: "Referencia"
									}
								),
								template: new sap.m.Text(
									"textReference",
									{
										text: "{reference}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colRecDate", 
							{
								width: "8rem",
								label: new sap.m.Label(
									"lblRecDate",
									{
										text: "Fecha Recepción"
									}
								),
								template: new sap.m.Text(
									"textRecDate",
									{
										text: "{recDate}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colAmount", 
							{
								width: "7rem",
								label: new sap.m.Label(
									"lblAmount",
									{
										text: "Importe"
									}
								),
								template: new sap.m.Text(
									"textAmount",
									{
										text: "{amount}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colPayDate", 
								{
									width: "8rem",
									label: new sap.m.Label(
										"lblPayDate",
										{
											text: "Fecha propuesta para Pago",
												wrapping: true
										}
									),
									template: new sap.m.Text(
										"textPayDate",
										{
											text: "{payDate}",
											wrapping: true
										}
									)
								}
							),
						new sap.ui.table.Column(
							"colCurrency", 
							{
								width: "5rem",
								label: new sap.m.Label(
									"lblCurrency",
									{
										text: "Moneda"
									}
								),
								template: new sap.m.Text(
									"textCurrency",
									{
										text: "{currency}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colUuid", 
								{
									width: "22rem",
									label: new sap.m.Label(
										"lblUuid",
										{
											text: "UUID"
										}
									),
									template: new sap.m.Text(
										"textUuid",
										{
											text: "{uuid}",
											wrapping: true
										}
									)
								}
							),
						new sap.ui.table.Column(
							"colStatus", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblStatus",
									{
										text: "Estatus"
									}
								),
								template: new sap.m.Text(
									"textStatus",
									{
										text: "{status}",
										wrapping: true
									}
								),
                                                            filterProperty: "status",
                                                            sortProperty: 'status'
							}
						)
					
					],
					
				}
				
			).setModel(ogModel);
			
			
			tblInvoices.placeAt("divLbq");
			
			var tblBqm = tblInvoices.clone();
			tblBqm.destroyTitle();
			tblBqm.bindRows('/1/bqm');
			tblBqm.placeAt("divBqm");
			
		</script>
		
	</head>
	<body>
		<div id="divLbq"></div>
		<br/>
		<div id="divBqm"></div>
	</body>
</html>