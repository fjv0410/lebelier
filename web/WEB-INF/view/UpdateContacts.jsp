<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		
		<title>Actualización de Información</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='<c:url value="/resources/sap-ui-core.js" />'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.tnt, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	          
		<script type="text/javascript">

			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "<c:url value='/img/puce.png'/>",
				}
			);

			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								
								dialog.close();
								
						    	if(sMsgType == "success"){
									window.top.location.reload(true);
						    	}
						    	
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}
			
			var oCommercialName = new sap.m.Input(
				"commercialName",
				{
					type: "Text",
					placeholder: "Nombre",
					required: true,
					width: "100%",
					//maxLength: 14
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oCommercialPhone = new sap.m.Input(
				"commercialPhone",
				{
					type: sap.m.InputType.Tel,
					placeholder: "Teléfono",
					required: true,
					width: "100%",
					//maxLength: 14
				}
			).addStyleClass("sapUiSmallMarginBottom");

			var oCommercialEmail = new sap.m.Input(
				"commercialEmail",
				{
					type: sap.m.InputType.Email,
					placeholder: "Email",
					required: true,
					width: "100%",
					//maxLength: 14
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oFiscalName  = oCommercialName.clone("fiscalName");
			var oFiscalPhone = oCommercialPhone.clone("fiscalPhone");
			var oFiscalEmail = oCommercialEmail.clone("fiscalEmail");

			var oAccountsName  = oCommercialName.clone("accountsName");
			var oAccountsPhone = oCommercialPhone.clone("accountsPhone");
			var oAccountsEmail = oCommercialEmail.clone("accountsEmail");

			function validateEmptyText(currentField){
				if(currentField.getValue().trim() == ""){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("El campo está vacío.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				return true;
			}

			function validateEmail(currentField) {
				var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			   	var email = currentField.getValue().trim();
				
		 		if(!regex.test(email)){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("Correo Electrónico no válido.");
					return false;
			 	}
		 		
			 	currentField.setValueState(sap.ui.core.ValueState.None);
				
			  	return true;
			}
			
			function validateForm(){
				
				oCommercialName.setValueState(sap.ui.core.ValueState.None);
				oCommercialPhone.setValueState(sap.ui.core.ValueState.None);
				oCommercialEmail.setValueState(sap.ui.core.ValueState.None);
				
				oFiscalName.setValueState(sap.ui.core.ValueState.None);
				oFiscalPhone.setValueState(sap.ui.core.ValueState.None);
				oFiscalEmail.setValueState(sap.ui.core.ValueState.None);
				
				oAccountsName.setValueState(sap.ui.core.ValueState.None);
				oAccountsPhone.setValueState(sap.ui.core.ValueState.None);
				oAccountsEmail.setValueState(sap.ui.core.ValueState.None);
				
				var currentField = oCommercialName;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oCommercialPhone;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oCommercialEmail;
				if(!validateEmptyText(currentField)){
					return false;
				}
				if(!validateEmail(currentField)){
					return false;
				}

				currentField = oFiscalName;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oFiscalPhone;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oFiscalEmail;
				if(!validateEmptyText(currentField)){
					return false;
				}
				if(!validateEmail(currentField)){
					return false;
				}

				currentField = oAccountsName;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oAccountsPhone;
				if(!validateEmptyText(currentField)){
					return false;
				}

				currentField = oAccountsEmail;
				if(!validateEmptyText(currentField)){
					return false;
				}
				if(!validateEmail(currentField)){
					return false;
				}
				
				return true;
				
			}
			
			function saveData(){

				oBusyInd.open();
				
				if(validateForm()){
					
					var sParams = "commercialName=" + oCommercialName.getValue();
					sParams += "&commercialPhone=" + oCommercialPhone.getValue();
					sParams += "&commercialEmail=" + oCommercialEmail.getValue();
					sParams += "&fiscalName=" + oFiscalName.getValue();
					sParams += "&fiscalPhone=" + oFiscalPhone.getValue();
					sParams += "&fiscalEmail=" + oFiscalEmail.getValue();
					sParams += "&accountsName=" + oAccountsName.getValue();
					sParams += "&accountsPhone=" + oAccountsPhone.getValue();
					sParams += "&accountsEmail=" + oAccountsEmail.getValue();
					
					var xhr = new XMLHttpRequest();
					xhr.open('GET', "pUpdateContactInformation" + "?" + sParams, true);
						
					xhr.onreadystatechange = function() {
					    if (xhr.readyState == XMLHttpRequest.DONE) {
					    	if (xhr.status == "200") {
								
					    		try {
						    		
							    	var result = xhr.getResponseHeader("process_result");
							    	var message = xhr.getResponseHeader("process_msg");
							    	
							    	var headers = xhr.getAllResponseHeaders();
							    	var arr = headers.trim().split(/[\r\n]+/);
							    	
							    	var lstValues = new sap.m.List();
							    	
							        arr.forEach(
							       	  function (line) {
								         var parts = line.split(': ');
								         var header = parts.shift();
								         var value = parts.join(': ');
								          
								         if(header.indexOf("process_msg_lst") == 0){
								       	   var item = new sap.m.StandardListItem();
	
								           item.setTitle(value);
								        	  
								           lstValues.addItem(item);
								           
								         }
								          
							          }
							       	);
							        
							    	createMessage(result, "Resultado del proceso.", lstValues);
							    	
					    		}
					    		catch(error) {
							   	  var lstValuesExc = new sap.m.List();
						       	  var itemExc = new sap.m.StandardListItem();
						        	  
						       	  itemExc.setTitle("Excepción en cliente:");
						       	  itemExc.setDescription(error);
						       	  
						          lstValuesExc.addItem(itemExc);
							    	
					      		  createMessage(result, "Sucedió un error.", lstValuesExc);
					    		}
								
					    	}else{
						   		var lstValues = new sap.m.List();
					       	  	var item = new sap.m.StandardListItem();
					        	  
					        	item.setTitle("Estatus de la respuesta: ");
					       	   	item.setDescription(xhr.status);
					       	  
					           	lstValues.addItem(item);
	
						    	createMessage("Error", "Sucedió un error.", lstValues);
						    	
						    }
	
					    	oBusyInd.close();
					    	
					    }
					}
						
					xhr.send(null);
					
				}else{

			    	oBusyInd.close();
			    	
				}
				
			}
			
			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 2,
					columnsL: 7,
					emptySpanL: 3,

					labelSpanM: 3,
					columnsM: 7,
					emptySpanM: 2,

					labelSpanS: 12,
					emptySpanS: 0,
					
					content: [
						
						new sap.m.Label(
							"lblCommercial", 
							{
								text:"Contacto Comercial",
								labelFor: "commercialName",
								required: true
							}
						),
						
						oCommercialName,
						
						oCommercialPhone,
						
						oCommercialEmail,

						new sap.m.Label(
							"lblFiscal", 
							{
								text:"Contacto Fiscal",
								labelFor: "fiscalName",
								required: true
							}
						),
						
						oFiscalName,
						
						oFiscalPhone,
						
						oFiscalEmail,

						new sap.m.Label(
							"lblAccounts", 
							{
								text:"Contacto Cuentas por Cobrar",
								labelFor: "accountsName",
								required: true
							}
						),
						
						oAccountsName,
						
						oAccountsPhone,
						
						oAccountsEmail
						
					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Actualización de Datos de Contacto.",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

						new sap.m.Button(
							"btnSubmit",
							{
								type: sap.m.ButtonType.Default,
								//text: "Guardar",
								icon: "sap-icon://save",
								press: function(){
									saveData();
								}
							}
						)
					]
				}
			);
			
			simpleForm.setToolbar(toolbar);
			
			simpleForm.placeAt("divForm");
			
		</script>
		
	</head>
	
	<body>
		
		<div id="divForm"></div>
		
	</body>
</html>