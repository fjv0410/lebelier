<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Consulta de Anuncios</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
		<script type="text/javascript">
			var oModelAnnouncements = new sap.ui.model.json.JSONModel("getAnnouncements");
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "${pageContext.request.contextPath}/img/puce.png",
				}
			);
			
			var tblAnnouncements = new sap.ui.table.Table(
				"tblAnnouncements",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://marketing-campaign",
										alt: "Anuncios",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Anuncios Importantes", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					toolbar: new sap.m.OverflowToolbar(
						"toolbar", 
						{
							content: [

								new sap.m.SearchField(
									"searchfield",
									{
										placeholder: "Buscar...", 
										width: "40%",
										
										search: function(oEvent){
											var sQuery = oEvent.getParameter("query");
											var filterUsers = [];
											
											if (sQuery) {
												filterUsers = new sap.ui.model.Filter(
													[
														new sap.ui.model.Filter("date", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("subject", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("receiver", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("body", sap.ui.model.FilterOperator.Contains, sQuery)
													], 
													false
												);
											}

											tblAnnouncements.getBinding("rows").filter(filterUsers, "Application");
											
										}
									
									}
								),
								
								new sap.m.ToolbarSpacer( ),
								
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
							"colDate", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblHeaderDate",
									{
										text: "Fecha"
									}
								),
								template: new sap.m.Text(
									"textHeaderDate",
									{
										text: "{date}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colSubject", 
							{
								width: "17rem",
								label: new sap.m.Label(
									"lblSubject",
									{
										text: "Asunto"
									}
								),
								template: new sap.m.Text(
									"textSubject",
									{
										text: "{subject}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colBody", 
							{
								width: "35rem",
								label: new sap.m.Label(
									"lblBody",
									{
										text: "Anuncio"
									}
								),
								template: new sap.m.Text(
									"textBody",
									{
										text: "{body}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colAttachment", 
							{
								width: "5rem",
								hAlign: sap.ui.core.HorizontalAlign.Center,
								label: new sap.m.Label(
									"lblAttachment",
									{
										text: "Archivo"
									}
								),
								template: new sap.m.Button(
									"textAttachment",
									{
										icon: "sap-icon://attachment", 
										tooltip: "{file_name}",
										visible: "{has_file}",
										customData: new sap.ui.core.CustomData(
											{ key: "file", value: "{file_name}" }
										),
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											var path = oCustomData[0].mProperties.value;
											sap.m.URLHelper.redirect("../files/announcements/" + path, true)
										}
									}
								)
							}
						),
						
						
					],
					
				}
				
			).setModel(oModelAnnouncements);
			
			
			tblAnnouncements.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>