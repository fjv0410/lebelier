<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Autorización de Facturas Sin Orden de Entrega</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	   	<script id='jsonmodel-js' src='<c:url value='/js/common/JsonModelUtils.js'/>'></script> 
	    
		<script type="text/javascript">
			ogModel.loadData("InvoicesToAuthorize");
			
			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								dialog.close();
								
								if(sMsgType == "Success" || sMsgType == "Warning"){
									
									ogModel.loadData("InvoicesToAuthorize");
									
								}
								
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}

			function authorizeInvoice(p_action, p_id, p_reference, p_comments){
				
				var sTarget = "authorizeInvoice";

				var sParams = "id=" + p_id + "&" +
							  "reference=" + p_reference + "&" +
							  "comments=" + p_comments + "&" + 
							  "auth=" + p_action;
				
				sParams +=  "&" + "_csrf=" + "${_csrf.token}";
				
				executePostSaveData(
						sTarget, 
						sParams, 
						function(){
							
						}
				);
				
				
			}
			
			var tblInvoices = new sap.ui.table.Table(
				"tblInvoices",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://sales-order",
										alt: "Facturas",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Autorización de Facturas Sin Orden de Entrega", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
								"colCompany", 
								{
									width: "6rem",
									label: new sap.m.Label(
										"lblCompany",
										{
											text: "Sociedad"
										}
									),
									template: new sap.m.Text(
										"textCompany",
										{
											text: "{company}",
											wrapping: true
										}
									),filterProperty: "company",
                                                                         sortProperty: 'company'
								}
							),
                                                new sap.ui.table.Column(
								"colDocSap", 
								{
									width: "6rem",
									label: new sap.m.Label(
										"lblDocSap",
										{
											text: "Doc SAP"
										}
									),
									template: new sap.m.Text(
										"textDocSap",
										{
											text: "{docSap}",
											wrapping: true
										}
									),filterProperty: "docSap",
                                                                         sortProperty: 'docSap'
								}
							),
                                                
                                                new sap.ui.table.Column(
								"colNumProveedor", 
								{
									width: "8rem",
									label: new sap.m.Label(
										"lblNumProveedor",
										{
											text: "Num. Proveedor"
										}
									),
									template: new sap.m.Text(
										"textNumProveedor",
										{
											text: "{numProveedor}",
											wrapping: true
										}
									),filterProperty: "numProveedor",
                                                                         sortProperty: 'numProveedor'
								}
							),
						new sap.ui.table.Column(
							"colReference", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblReference",
									{
										text: "Referencia"
									}
								),
								template: new sap.m.Text(
									"textReference",
									{
										text: "{reference}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colRecDate", 
							{
								width: "8rem",
								label: new sap.m.Label(
									"lblRecDate",
									{
										text: "Fecha Recepción"
									}
								),
								template: new sap.m.Text(
									"textRecDate",
									{
										text: "{recDate}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colAmount", 
							{
								width: "7rem",
								label: new sap.m.Label(
									"lblAmount",
									{
										text: "Importe"
									}
								),
								template: new sap.m.Text(
									"textAmount",
									{
										text: "{amount}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colCurrency", 
							{
								width: "5rem",
								label: new sap.m.Label(
									"lblCurrency",
									{
										text: "Moneda"
									}
								),
								template: new sap.m.Text(
									"textCurrency",
									{
										text: "{currency}",
										wrapping: true
									}
								)
							}
						),
						
						new sap.ui.table.Column(
							"colAuthorize", 
							{
								width: "5rem",
								label: new sap.m.Label(
									"lblAuthorize",
									{
										text: "Autorizar"
									}
								),
								template:  new sap.m.Button(
									"btnAuthorize",
									{
										icon: "sap-icon://accept", 
										tooltip: "Autorizar Factura",
										
										customData: [
											new sap.ui.core.CustomData(
												{ key: "id", value: "{id}" }
											),
											new sap.ui.core.CustomData(
												{ key: "reference", value: "{reference}" }
											)
										],
										
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											var id = oCustomData[0].mProperties.value;
											var ref = oCustomData[1].mProperties.value;
											
											var comments = oEvent.getSource().getParent().mAggregations.cells[7]._lastValue;
											
											authorizeInvoice("auth", id, ref, comments);
											
										}
										
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colReject", 
								{
									width: "6rem",
									label: new sap.m.Label(
										"lblReject",
										{
											text: "Rechazar"
										}
									),
									template:  new sap.m.Button(
										"btnReject",
										{
											icon: "sap-icon://decline", 
											tooltip: "Rechazar Factura",

											customData: [
												new sap.ui.core.CustomData(
													{ key: "id", value: "{id}" }
												),
												new sap.ui.core.CustomData(
													{ key: "reference", value: "{reference}" }
												)
											],
											
											press: function(oEvent){
												var oCustomData = oEvent.getSource().getCustomData();
												var id = oCustomData[0].mProperties.value;
												var ref = oCustomData[1].mProperties.value;

												var comments = oEvent.getSource().getParent().mAggregations.cells[7]._lastValue;
												
												authorizeInvoice("reject", id, ref, comments);
												
											}
										}
									)
								}
							),
							
							new sap.ui.table.Column(
								"colComments", 
								{
									width: "15rem",
									label: new sap.m.Label(
										"lblComments",
										{
											text: "Observaciones"
										}
									),
									template:  new sap.m.TextArea(
										"txtComments",
										{
											tooltip: "Observaciones",
											maxLength: 150,
											rows: 1,
											
											value: "{comments}",
											
										}
									)
								}
							)
							
					
					],
					
				}
				
			).setModel(ogModel);
			
			
			tblInvoices.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>