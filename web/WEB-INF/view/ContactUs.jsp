<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Contáctanos</title>
		
		<c:set var="currentDate" value="<%=new java.util.Date()%>" />
			
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
		<script type="text/javascript">
			var oModelAnnouncements = new sap.ui.model.json.JSONModel("getMessages");
			
			function validateEmptyText(currentField){
				if(currentField.getValue().trim() == ""){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("El campo está vacío.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				return true;
			}
			
			function validateForm(){
				
				oSubject.setValueState(sap.ui.core.ValueState.None);
				oBody.setValueState(sap.ui.core.ValueState.None);
				
				currentField = oSubject;

				if(!validateEmptyText(currentField)){
					return false;
				}
				
				currentField = oBody;
				
				if(!validateEmptyText(currentField)){
					return false;
				}
				
				return true;
				
			}
			
			function saveMessage(){
				
				oBusyInd.open();
				
				var target = "createMessage";
				var sParams = "subject=" + oSubject.getValue() + "&" +
						   	   "body=" + oBody.getValue();
				
				sParams +=  "&" + "_csrf=" + "${_csrf.token}";
				
				var xhr = new XMLHttpRequest();
				xhr.open('POST', target, true);
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				
				xhr.onreadystatechange = function() {
					
				    if (xhr.readyState == XMLHttpRequest.DONE) {
				    	
				    	if (xhr.status == "200") {
							
				    		try {
					    		
						    	var result = xhr.getResponseHeader("process_result");
						    	var message = xhr.getResponseHeader("process_msg");
						    	
						    	var headers = xhr.getAllResponseHeaders();
						    	var arr = headers.trim().split(/[\r\n]+/);
						    	
						    	var lstValues = new sap.m.List();
						    	
						        arr.forEach(
						       	  function (line) {
							         var parts = line.split(': ');
							         var header = parts.shift();
							         var value = parts.join(': ');
							          
							         if(header.indexOf("process_msg_lst") == 0){
							       	   var item = new sap.m.StandardListItem();
							        	  
							           item.setTitle(value);
							        	  
							           lstValues.addItem(item);
							           
							         }
							          
						          }
						       	);
						    	
						    	createMessage(result, "Resultado del proceso.", lstValues);
						    	
				    		}catch(error) {
						   	  var lstValuesExc = new sap.m.List();
					       	  var itemExc = new sap.m.StandardListItem();
					        	  
					       	  itemExc.setTitle("Excepción en cliente:");
					       	  itemExc.setDescription(error);
					       	  
					          lstValuesExc.addItem(itemExc);
						    	
				      		  createMessage(result, "Sucedió un error.", lstValuesExc);
				    		}
							
				    	}else{
					   		var lstValues = new sap.m.List();
				       	  	var item = new sap.m.StandardListItem();
				        	  
				        	item.setTitle("Estatus de la respuesta: ");
				       	   	item.setDescription(xhr.status);
				       	  
				           	lstValues.addItem(item);

					    	createMessage("Error", "Sucedió un error.", lstValues);
					    	
					    }
				    	
				    	oBusyInd.close();
				    	
				    }
				}
				
				xhr.send(sParams);
				
			}
			
			var oDate = new sap.m.Text(
				"txtEditDate",
				{
					text: '<fmt:formatDate pattern="dd/MM/yyyy" value="${currentDate}" />',
					width: "100%"
					
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oSubject = new sap.m.Input(
				"txtEditSubject",
				{
					type: sap.m.InputType.Text,
					required: true,
					width: "100%",
					maxLength: 100
				}
			).addStyleClass("sapUiSmallMarginBottom");

			var oBody = new sap.m.TextArea(
				"txtEditBody",
				{
					required: true,
					width: "100%",
					maxLength: 1000
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var pageNewMessage = new sap.m.Page(
				"pageNewMessage",
				{
					showHeader: false,
					showFooter: false,
					
					content: [
						new sap.m.VBox(
							"vBoxNewUser", 
							{
								fitContainer: true,
								
								alignItems: sap.m.FlexAlignItems.Stretch, 
								
								items: [ 

									new sap.m.Label(
										"lblEditDate", 
										{
											text:"Fecha",
											labelFor: "txtEditDate",
											required: true
										}
									),
									
									oDate,

									new sap.m.Label(
										"lblEditSubject", 
										{
											text:"Asunto",
											labelFor: "txtEditSubject",
											required: true
										}
									),
									
									oSubject,

									new sap.m.Label(
										"lblEditBody", 
										{
											text:"Mensaje",
											labelFor: "txtEditBody",
											required: true
										}
									),
									
									oBody,
									
								]
							}
						)
						
					]
				}
			);
			
			var oDialogNewMessage = new sap.m.Dialog(
				"dialogNewMessage",
				{
					title: 'Escribir Mensaje',
					
					stretch: true,
					
					content: [
						pageNewMessage
					],
					
					beginButton: new sap.m.Button(
						"btnSave",
						{
							text: 'Guardar',
							press: function (oEvent) {
								if(validateForm()){
									saveMessage();
								}
							}
						}
					),
					
					endButton: new sap.m.Button(
						"btnCancel",
						{
							text: 'Cancelar',
							press: function (oEvent) {
								oDialogNewMessage.close();
							}
						}
					)
				
				}
			);
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "${pageContext.request.contextPath}/img/puce.png",
				}
			);

			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								dialog.close();
								
								if(sMsgType == "Success" || sMsgType == "Warning"){
									
									oDialogNewMessage.close();
									
									tblMessages.setModel(new sap.ui.model.json.JSONModel("getMessages"));
									
								}
								
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}
			
			var tblMessages = new sap.ui.table.Table(
				"tblMessages",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://headset",
										alt: "Contáctanos",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Contáctanos", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					toolbar: new sap.m.OverflowToolbar(
						"toolbar", 
						{
							content: [

								new sap.m.SearchField(
									"searchfield",
									{
										placeholder: "Buscar...", 
										width: "40%",
										
										search: function(oEvent){
											var sQuery = oEvent.getParameter("query");
											var filterUsers = [];
											
											if (sQuery) {
												filterUsers = new sap.ui.model.Filter(
													[
														new sap.ui.model.Filter("date", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("subject", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("body", sap.ui.model.FilterOperator.Contains, sQuery)
													], 
													false
												);
											}

											tblMessages.getBinding("rows").filter(filterUsers, "Application");
											
										}
									
									}
								),
								
								new sap.m.ToolbarSpacer( ),
								
								new sap.m.Button(
									"btnNewMessage",
									{
										icon: "sap-icon://add-document", 
										tooltip: "Crear Anuncio",
										press: function(){

											oDate.setText('<fmt:formatDate pattern="dd/MM/yyyy" value="${currentDate}" />');
											oSubject.setValue("");
											oBody.setValue("");
											
											oDialogNewMessage.open();
											
										}
									}
								)
								
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						
						new sap.ui.table.Column(
							"colDate", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblHeaderDate",
									{
										text: "Fecha"
									}
								),
								template: new sap.m.Text(
									"textHeaderDate",
									{
										text: "{date}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colSubject", 
							{
								width: "20rem",
								label: new sap.m.Label(
									"lblSubject",
									{
										text: "Asunto"
									}
								),
								template: new sap.m.Text(
									"textSubject",
									{
										text: "{subject}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colBody", 
							{
								width: "40rem",
								label: new sap.m.Label(
									"lblBody",
									{
										text: "Mensaje"
									}
								),
								template: new sap.m.Text(
									"textBody",
									{
										text: "{body}",
										wrapping: true
									}
								)
							}
						)
					
					],
					
				}
				
			).setModel(oModelAnnouncements);
			
			
			tblMessages.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>