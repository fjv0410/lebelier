<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Contáctanos</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
		<script type="text/javascript">
			var oModelAnnouncements = new sap.ui.model.json.JSONModel("getMessages");
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "${pageContext.request.contextPath}/img/puce.png",
				}
			);

			var tblMessages = new sap.ui.table.Table(
				"tblMessages",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://headset",
										alt: "Contáctanos",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Contáctanos", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					toolbar: new sap.m.OverflowToolbar(
						"toolbar", 
						{
							content: [
								
								new sap.m.ToolbarSpacer( ),
								
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						
						new sap.ui.table.Column(
							"colDate", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblHeaderDate",
									{
										text: "Fecha"
									}
								),
								template: new sap.m.Text(
									"textHeaderDate",
									{
										text: "{date}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colSubject", 
							{
								width: "15rem",
								label: new sap.m.Label(
									"lblSubject",
									{
										text: "Asunto"
									}
								),
								template: new sap.m.Text(
									"textSubject",
									{
										text: "{subject}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colBody", 
							{
								width: "32rem",
								label: new sap.m.Label(
									"lblBody",
									{
										text: "Mensaje"
									}
								),
								template: new sap.m.Text(
									"textBody",
									{
										text: "{body}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colSender", 
								{
									width: "10rem",
									label: new sap.m.Label(
										"lblSender",
										{
											text: "Emisor"
										}
									),
									template: new sap.m.Text(
										"textSender",
										{
											text: "{sender_name}",
											wrapping: true
										}
									)
								}
							)
					
					],
					
				}
				
			).setModel(oModelAnnouncements);
			
			
			tblMessages.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>