<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  

<!DOCTYPE html>

<html>
  <head>
	<meta charset="UTF-8">
	<title>Facturas sin Orden de Entrega</title>
	
	   	<script id='sap-ui-bootstrap' 
	          src='<c:url value="/resources/sap-ui-core.js" />'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.tnt, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	          
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	   	
		<script type="text/javascript">
			
			var GV_UPLOADER_COUNT = 1;
			
			var GV_ARR_UPLOADER_XML = [null, null, null, null, null, null, null, null, null, null];
			var GV_ARR_UPLOADER_PDF = [null, null, null, null, null, null, null, null, null, null];
			
			function createParameter(p_name, p_value){
				
				return new sap.ui.unified.FileUploaderParameter(
					{
						name: p_name,  /*"${_csrf.parameterName}",*/
						value: p_value /*"${_csrf.token}" */
					}
				)
				
			}
			
			var oUploaderXml = new sap.ui.unified.FileUploader(
				"xmlFile",
				{
					name: "xmlFile",
					fileType: "xml",
					placeholder: "Selecciona XML",
					uploadUrl: "uploadXmlWithoutDeliveries",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,

					iconOnly: true,
					icon: "sap-icon://attachment-html",
					
					width: "100%",
					
					sameFilenameAllowed: false,
					
					layoutData: new sap.ui.layout.GridData(
						{span: "L4 M4 S4"}
					)
				}
			);
			
			var oUploaderPdf =  new sap.ui.unified.FileUploader(
				"pdfFile",
				{
					name: "pdfFile",
					fileType: "pdf",
					placeholder: "Selecciona PDF",
					uploadUrl: "uploadPdfWithoutDeliveries",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,
					
					iconOnly: true,
					icon: "sap-icon://pdf-attachment",
					
					width: "100%",
					
					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					),
					
					layoutData: new sap.ui.layout.GridData(
						{span: "L4 M4 S4"}
					),
					
				}
			);
			
			function validateDuplicates(p_arr_uploaders){

				var lv_index, lo_uploader, lo_uploader_aux;
				
				for(lv_index = 0; lv_index < p_arr_uploaders.length; lv_index++ ){
					lo_uploader = p_arr_uploaders[lv_index];
					
					if(lo_uploader != null && lo_uploader.getValue() != ""){
						
						for(var j = lv_index + 1; j < p_arr_uploaders.length; j++ ){
							lo_uploader_aux = p_arr_uploaders[j];

							if(lo_uploader_aux != null && (lo_uploader_aux.getValue() == lo_uploader.getValue()) ){
								lo_uploader.setValueState(sap.ui.core.ValueState.Error);
								lo_uploader.setValueStateText("Archivo duplicado.");
								lo_uploader_aux.setValueState(sap.ui.core.ValueState.Error);
								lo_uploader_aux.setValueStateText("Archivo duplicado.");
								
								return false;
									
							}
						}
						
					}
					
				}
				
				return true;
				
			}

			function validateForm(){
				
				var lv_index = 0;
				var lo_uploadXml, lo_uploadPdf;
				
				for(;lv_index < GV_ARR_UPLOADER_XML.length; lv_index++ ){
					lo_uploadXml = GV_ARR_UPLOADER_XML[lv_index];
					lo_uploadPdf = GV_ARR_UPLOADER_PDF[lv_index];

					if(lo_uploadXml != null){
						lo_uploadXml.setValueState(sap.ui.core.ValueState.None);
					}
					
					if(lo_uploadPdf != null){
						lo_uploadPdf.setValueState(sap.ui.core.ValueState.None);
					}
					
				}
				
				if(!validateDuplicates(GV_ARR_UPLOADER_XML)){
					return false;
				}

				if(!validateDuplicates(GV_ARR_UPLOADER_PDF)){
					return false;
				}
				
				for(lv_index = 0; lv_index < GV_ARR_UPLOADER_XML.length; lv_index++ ){
					lo_uploadXml = GV_ARR_UPLOADER_XML[lv_index];
					lo_uploadPdf = GV_ARR_UPLOADER_PDF[lv_index];

					if(lo_uploadXml != null && lo_uploadXml.getValue() == "" ){
						lo_uploadXml.setValueState(sap.ui.core.ValueState.Error);
						lo_uploadXml.setValueStateText("Valor incorrecto.");
						
						return false;
					}
					
					if(lo_uploadPdf != null && lo_uploadPdf.getValue() == "" ){
						lo_uploadPdf.setValueState(sap.ui.core.ValueState.Error);
						lo_uploadPdf.setValueStateText("Valor incorrecto.");
						
						return false;
					}
					
				}
				
				return true;
				
			}
			
			function saveData(){

				if(validateForm()){
					
					var sParams = "_csrf=" + "${_csrf.token}";
					var sTarget = "invoiceWithoutDelivery";
					
					executePostSaveData(
							sTarget, 
							sParams, 
							function(){

								for(lv_index = 0; lv_index < GV_ARR_UPLOADER_XML.length; lv_index++ ){
									lo_uploadXml = GV_ARR_UPLOADER_XML[lv_index];
									lo_uploadPdf = GV_ARR_UPLOADER_PDF[lv_index];

									if(lo_uploadXml != null){
										lo_uploadXml.setValue(null);
									}
									
									if(lo_uploadPdf != null){
										lo_uploadPdf.setValue(null);
									}
									
								}
								
							}
					);
					
					
				}

			}
			
			var simpleForm = new sap.ui.layout.form.SimpleForm(
				"simpleForm",
				{
					editable: false,
					layout: sap.ui.layout.form.SimpleFormLayout.ResponsiveGridLayout,
					adjustLabelSpan: false,
					singleContainerFullSize: true,
					
					labelSpanL: 2,
					columnsL: 	8,
					emptySpanL: 2,

					labelSpanM: 2,
					columnsM: 	10,
					emptySpanM: 0,

					labelSpanS: 2,
					emptySpanS: 0,
					
					content: [
						
						new sap.m.Label(
							"lblUploadFiles", 
							{
								text:"Archivos",
								labelFor: "uploaderXml",
								required: true
							}
						),
						
						oUploaderXml,
							
						oUploaderPdf,

						new sap.m.Button(
							"btnAdd",
							{
								icon: "sap-icon://add",
								width: "38px",
								
								layoutData: new sap.ui.layout.GridData(
									{span: "L1 M2 S2"}
								),
								
								press: function(){
									
									if(GV_UPLOADER_COUNT != "10"){
										
										createUploader();
										
										GV_UPLOADER_COUNT = GV_UPLOADER_COUNT + 1;
										
									}
									
								}
								
							}
						)
						
					]
				}
				
			);
			
			var toolbar = new sap.m.Toolbar(
				"toolbar",
				{
					content: [
						new sap.m.Title(
							"title",
							{
								text: "Registro de Facturas sin Número de Entrega",
								level: "H5",
								titleStyle: "H5"
							}	
						),
							
						new sap.m.ToolbarSpacer(),

						new sap.m.Button(
							"btnSubmit",
							{
								type: sap.m.ButtonType.Accept,
								text: "Guardar",
								press: function(){
									saveData();
								}
							}
						)
					]
				}
			);
			
			function createUploader(){

				var lv_index = 0;
				
				for(; lv_index < GV_ARR_UPLOADER_XML.length; lv_index++ ){
					if(GV_ARR_UPLOADER_XML[lv_index] == null){
						break;
					}
				}
				
				var lv_lblId = "lblUploader_" + lv_index;
				var lv_uploaderXmlId = lv_index;
				var lv_uploaderPdfId = lv_index;
				var lv_btnRemId = "btnRemove_" + lv_index;
				var lv_btnAddId = "btnAdd_" + lv_index;
				
				var lv_txtInvId = "hidd_" + lv_index;
				
				var lo_lbl = new sap.m.Label(
						lv_lblId,
						{
							text: ("")
						}
					);
				
				var lo_upldXml = oUploaderXml.clone(lv_uploaderXmlId);
				var lo_upldPdf = oUploaderPdf.clone(lv_uploaderPdfId);
				
				lo_upldXml.removeAllParameters();

				lo_upldXml.addParameter(
					createParameter("${_csrf.parameterName}", "${_csrf.token}")
				); 
				
				lo_upldXml.addParameter(
					createParameter("indXml", lv_index)
				);

				lo_upldPdf.removeAllParameters();

				lo_upldPdf.addParameter(
					createParameter("${_csrf.parameterName}", "${_csrf.token}")
				); 
				
				lo_upldPdf.addParameter(
					createParameter("indPdf", lv_index)
				);
				
				lo_upldXml.clear();
				lo_upldXml.setValueState(sap.ui.core.ValueState.None);
				lo_upldPdf.clear();
				lo_upldPdf.setValueState(sap.ui.core.ValueState.None);
				
				var lo_txtInv = new sap.m.Text(
						lv_txtInvId, 
						{
							text: lv_index,
							visible: false
						}
					);
				
				var lo_btnRem = new sap.m.Button(
						lv_btnRemId,
						{
							icon: "sap-icon://less",
							width: "38px",
							
							layoutData: new sap.ui.layout.GridData( {span: "L1 M2 S2"} ),
	
							press: function(oEvent){

								var sParams = "ind=" + lv_index + "" + "_csrf=" + "${_csrf.token}";
								var sTarget = "removeFile";
								
								var xhr = new XMLHttpRequest();
								xhr.open('POST', sTarget, true);
								xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
								xhr.send(sParams);
								
								lo_lbl.destroy();
								lo_upldXml.destroy();
								lo_upldPdf.destroy();
								lo_btnRem.destroy();
								lo_txtInv.destroy();
								
								GV_ARR_UPLOADER_XML[lo_txtInv.getText()] = null;
								GV_ARR_UPLOADER_PDF[lo_txtInv.getText()] = null;
	
								GV_UPLOADER_COUNT = GV_UPLOADER_COUNT - 1;
								
							}
							
						}
					);
				
				simpleForm.addContent( lo_lbl );
				simpleForm.addContent( lo_upldXml );
				simpleForm.addContent( lo_upldPdf );
				
				simpleForm.addContent( lo_btnRem );

				simpleForm.addContent( lo_txtInv );
				
				GV_ARR_UPLOADER_XML[lv_index] = lo_upldXml;
				GV_ARR_UPLOADER_PDF[lv_index] = lo_upldPdf;
				
			}

			oUploaderXml.addParameter(
				createParameter("${_csrf.parameterName}", "${_csrf.token}")
			); 
			
			oUploaderXml.addParameter(
				createParameter("indXml", 0)
			);

			oUploaderPdf.addParameter(
				createParameter("${_csrf.parameterName}", "${_csrf.token}")
			); 
			
			oUploaderPdf.addParameter(
				createParameter("indPdf", 0)
			);
			
			GV_ARR_UPLOADER_XML[0] = oUploaderXml;
			GV_ARR_UPLOADER_PDF[0] = oUploaderPdf;
			
			simpleForm.setToolbar(toolbar);
			
			simpleForm.placeAt("divForm");
			
		</script>
		
	
  </head>
  <body>
  		<div id="divForm"></div>
  </body>
</html>