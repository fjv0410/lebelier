<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Administración de Anuncios</title>
		
		<c:set var="currentDate" value="<%=new java.util.Date()%>" />
			
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
		<script type="text/javascript">
			var oModelAnnouncements = new sap.ui.model.json.JSONModel("getAnnouncements");
			
			var gv_action, gv_file, gv_id;
			
			function validateEmptyText(currentField){
				if(currentField.getValue().trim() == ""){
					currentField.setValueState(sap.ui.core.ValueState.Error);
					currentField.setValueStateText("El campo está vacío.");
					return false;
				}
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				return true;
			}
			
			function validateForm(){
				
				oSubject.setValueState(sap.ui.core.ValueState.None);
				oBody.setValueState(sap.ui.core.ValueState.None);
				oUploader.setValueState(sap.ui.core.ValueState.None);
				
				currentField = oSubject;

				if(!validateEmptyText(currentField)){
					return false;
				}
				
				currentField = oBody;
				
				if(!validateEmptyText(currentField)){
					return false;
				}
				
				return true;
				
			}
			
			function saveAnnouncement(p_action, p_id, p_file, p_subject){
				
				oBusyInd.open();
				
				var sParams = "";
				var target = "";
				
				if(p_file){
					sParams += "file=" + p_file + "&";
				}
				
				if(p_action == "new"){
					target = "createAnnouncement";
					sParams += "subject=" + oSubject.getValue() + "&" +
							   "body=" + oBody.getValue();
				}else if(p_action == "edit"){
					target = "updateAnnouncement";
					sParams += "id=" + p_id + "&"  +
							   "subject=" + oSubject.getValue() + "&" +
							   "body=" + oBody.getValue();
				}else if(p_action = "delete"){
					target = "deleteAnnouncement";
					sParams = "id=" + p_id + "&" +
					 		  "subject=" + p_subject;
				}
				
				sParams +=  "&" + "_csrf=" + "${_csrf.token}";
				
				var xhr = new XMLHttpRequest();
				xhr.open('POST', target, true);
				xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
				
				xhr.onreadystatechange = function() {
					
				    if (xhr.readyState == XMLHttpRequest.DONE) {
				    	
				    	if (xhr.status == "200") {
							
				    		try {
					    		
						    	var result = xhr.getResponseHeader("process_result");
						    	var message = xhr.getResponseHeader("process_msg");
						    	
						    	var headers = xhr.getAllResponseHeaders();
						    	var arr = headers.trim().split(/[\r\n]+/);
						    	
						    	var lstValues = new sap.m.List();
						    	
						        arr.forEach(
						       	  function (line) {
							         var parts = line.split(': ');
							         var header = parts.shift();
							         var value = parts.join(': ');
							          
							         if(header.indexOf("process_msg_lst") == 0){
							       	   var item = new sap.m.StandardListItem();
							        	  
							           item.setTitle(value);
							        	  
							           lstValues.addItem(item);
							           
							         }
							          
						          }
						       	);
						    	
						    	createMessage(result, "Resultado del proceso.", lstValues);
						    	
				    		}catch(error) {
						   	  var lstValuesExc = new sap.m.List();
					       	  var itemExc = new sap.m.StandardListItem();
					        	  
					       	  itemExc.setTitle("Excepción en cliente:");
					       	  itemExc.setDescription(error);
					       	  
					          lstValuesExc.addItem(itemExc);
						    	
				      		  createMessage(result, "Sucedió un error.", lstValuesExc);
				    		}
							
				    	}else{
					   		var lstValues = new sap.m.List();
				       	  	var item = new sap.m.StandardListItem();
				        	  
				        	item.setTitle("Estatus de la respuesta: ");
				       	   	item.setDescription(xhr.status);
				       	  
				           	lstValues.addItem(item);

					    	createMessage("Error", "Sucedió un error.", lstValues);
					    	
					    }
				    	
				    	oBusyInd.close();
				    	
				    }
				}
				
				xhr.send(sParams);
				
			}
			
			var oDate1 = new sap.m.DatePicker(
				"txtEditDate1",
				{
					displayFormat: "dd/MM/yyyy",
					valueFormat: "yyyy-MM-dd",
					required: true,
					placeholder: "dd/mm/yyyy",
					width: "100%"
					
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oDate = new sap.m.Text(
				"txtEditDate",
				{
					text: '<fmt:formatDate pattern="dd/MM/yyyy" value="${currentDate}" />',
					width: "100%"
					
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oSubject = new sap.m.Input(
				"txtEditSubject",
				{
					type: sap.m.InputType.Text,
					required: true,
					width: "100%",
					maxLength: 150
				}
			).addStyleClass("sapUiSmallMarginBottom");

			var oBody = new sap.m.TextArea(
				"txtEditBody",
				{
					required: true,
					width: "100%",
					maxLength: 500
				}
			).addStyleClass("sapUiSmallMarginBottom");
			
			var oUploader = new sap.ui.unified.FileUploader(
				"file",
				{
					placeholder: "Selecciona Archivo",
					uploadUrl: "uploadAnnouncementFile",
					uploadOnChange: false,
					multiple: false,
					sendXHR: true,

					iconOnly: true,
					icon: "sap-icon://attachment",
					
					width: "100%",
					
					sameFilenameAllowed: false,
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					),
					
					layoutData: new sap.ui.layout.GridData(
						{span: "L4 M4 S4"}
					),
					
					uploadComplete: function(oEvent){
						if(!verifyUploadedFile(oEvent)){
							this.setValue("");
							this.valueState = sap.ui.core.ValueState.Error;
						}else{
							saveAnnouncement(gv_action, gv_id, gv_file);
						}
					}
					
				}
			);

			function verifyUploadedFile(oEvent){
				
				var sStatusResp = oEvent.getParameter("status");
				
				if (sStatusResp == "200") {
					return true;
				} else {

					var sMsgType = "Error";
					
					var sMsgType = oEvent.getParameters().headers["upload_result"];
					var sMsg = oEvent.getParameters().headers["upload_msg"];
					
			   		var lstValues = new sap.m.List();
		       	  	var item = new sap.m.StandardListItem();
		       	  	
		       	  	var techInfo = oEvent.getParameters().headers["upload_msg_tech"];
		       	  	if(techInfo != null){
			       	   	item.setDescription(techInfo);
		       	  	}
		        	
		        	item.setTitle(sMsg);
		       	  
		           	lstValues.addItem(item);

					createMessage(sMsgType, "Sucedió un error.", lstValues);
					
					return false;
				}
				
			}
			
			function executeSaveAnnouncement(p_action, p_id, p_subject){
				if(p_action == "new" && oUploader && oUploader.getValue() != ""){
					gv_file = oUploader.getValue();
					gv_action = p_action;
					oUploader.upload();
				}else{
					gv_id = p_id;
					saveAnnouncement(p_action, p_id, null, p_subject);
				}
			}
			
			var pageEditAnnouncement = new sap.m.Page(
				"pageEditAnnouncement",
				{
					showHeader: false,
					showFooter: false,
					
					content: [
						new sap.m.VBox(
							"vBoxNewUser", 
							{
								fitContainer: true,
								
								alignItems: sap.m.FlexAlignItems.Stretch, 
								
								items: [ 

									new sap.m.Label(
										"lblEditDate", 
										{
											text:"Fecha",
											labelFor: "txtEditDate",
											required: true
										}
									),
									
									oDate,

									new sap.m.Label(
										"lblEditSubject", 
										{
											text:"Asunto",
											labelFor: "txtEditSubject",
											required: true
										}
									),
									
									oSubject,

									new sap.m.Label(
										"lblEditBody", 
										{
											text:"Anuncio",
											labelFor: "txtEditBody",
											required: true
										}
									),
									
									oBody,
									
									new sap.m.Label(
										"lblEditAttachment", 
										{
											text:"Adjunto",
											labelFor: "txtEditAttachment",
											required: true
										}
									),
									
									oUploader
									
								]
							}
						)
						
					]
				}
			);
			
			var oDialogEditAnnouncement = new sap.m.Dialog(
				"dialogEditAnnouncement",
				{
					title: 'Registrar Anuncio.',
					
					stretch: true,
					
					content: [
						pageEditAnnouncement
					],
					
					beginButton: new sap.m.Button(
						"btnSave",
						{
							text: 'Guardar',
							press: function (oEvent) {
								var sEditMode = oEvent.getSource().getParent().getCustomData()[0].mProperties.value;

								if(validateForm()){
									if(sEditMode == "new"){
										executeSaveAnnouncement("new");
									}else if(sEditMode == "edit"){
										var sId = oEvent.getSource().getParent().getCustomData()[1].mProperties.value;
										executeSaveAnnouncement("edit", sId);
									}
								}
								
							}
						}
					),
					
					endButton: new sap.m.Button(
						"brnCancel",
						{
							text: 'Cancelar',
							press: function (oEvent) {
								var sParentId = oEvent.getSource().getParent().sId;
								oDialogEditAnnouncement.close();
							}
						}
					)
				
				}
			);
			
			var oBusyInd = new sap.m.BusyDialog(
				{
					customIcon: "${pageContext.request.contextPath}/img/puce.png",
				}
			);

			function createMessage( sMsgType, sTitle, lstMsgs ){
				
				var dialog = new sap.m.Dialog(
					{
						type: 'Message',
						beginButton: new sap.m.Button({
							text: 'Aceptar',
							press: function () {
								dialog.close();
								
								if(sMsgType == "Success" || sMsgType == "Warning"){
									
									oDialogEditAnnouncement.close();
									
									tblAnnouncements.setModel(new sap.ui.model.json.JSONModel("getAnnouncements"));
									
								}
								
							}
						}),
						afterClose: function() {
							dialog.destroy();
						}
					}
				);
				
				dialog.setTitle(sTitle);
				
				dialog.setState(sMsgType);
				
				dialog.addContent(
					lstMsgs
				);
				
				dialog.open();
				
			}
			
			var tblAnnouncements = new sap.ui.table.Table(
				"tblAnnouncements",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 10,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://marketing-campaign",
										alt: "Anuncios",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Administración de Anuncios", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					toolbar: new sap.m.OverflowToolbar(
						"toolbar", 
						{
							content: [
								/*
								new sap.m.SearchField(
									"searchfield",
									{
										placeholder: "Buscar...", 
										width: "40%",
										
										search: function(oEvent){
											var sQuery = oEvent.getParameter("query");
											var filterUsers = [];
											
											if (sQuery) {
												filterUsers = new sap.ui.model.Filter(
													[
														new sap.ui.model.Filter("date", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("subject", sap.ui.model.FilterOperator.Contains, sQuery),
														new sap.ui.model.Filter("body", sap.ui.model.FilterOperator.Contains, sQuery)
													], 
													false
												);
											}

											tblAnnouncements.getBinding("rows").filter(filterUsers, "Application");
											
										}
									
									}
								),*/
								
								new sap.m.ToolbarSpacer( ),
								
								new sap.m.Button(
									"btnNewAnnouncement",
									{
										icon: "sap-icon://add-document", 
										tooltip: "Crear Anuncio",
										press: function(){

											oDate.setText('<fmt:formatDate pattern="dd/MM/yyyy" value="${currentDate}" />');
											oSubject.setValue("");
											oBody.setValue("");
											oUploader.setValue("");
											oUploader.setEnabled(true);
											
											oDialogEditAnnouncement.setTitle('Registrar Anuncio.');
											oDialogEditAnnouncement.removeAllCustomData();
											oDialogEditAnnouncement.addCustomData(
												new sap.ui.core.CustomData(
													{
														key: "mode",
														value: "new"
													}
												)
											)
											
											oDialogEditAnnouncement.open();
											
										}
									}
								)
								
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						new sap.ui.table.Column(
							"colId", 
							{
								width: "0rem",
								label: new sap.m.Label(
									"lblId",
									{
										text: ""
									}
								),
								template: new sap.ui.core.InvisibleText(
									"textId",
									{
										text: "{id}"
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colDate", 
							{
								width: "6rem",
								label: new sap.m.Label(
									"lblHeaderDate",
									{
										text: "Fecha"
									}
								),
								template: new sap.m.Text(
									"textHeaderDate",
									{
										text: "{date}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colSubject", 
							{
								width: "13rem",
								label: new sap.m.Label(
									"lblSubject",
									{
										text: "Asunto"
									}
								),
								template: new sap.m.Text(
									"textSubject",
									{
										text: "{subject}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colBody", 
							{
								width: "30rem",
								label: new sap.m.Label(
									"lblBody",
									{
										text: "Anuncio"
									}
								),
								template: new sap.m.Text(
									"textBody",
									{
										text: "{body}",
										wrapping: true
									}
								)
							}
						),
						new sap.ui.table.Column(
							"colAttachment", 
							{
								width: "5rem",
								hAlign: sap.ui.core.HorizontalAlign.Center,
								label: new sap.m.Label(
									"lblAttachment",
									{
										text: "Adjunto"
									}
								),
								template: new sap.m.Button(
									"textAttachment",
									{
										icon: "sap-icon://attachment", 
										tooltip: "{file_name}",
										visible: "{has_file}",
										customData: new sap.ui.core.CustomData(
											{ key: "file", value: "{file_name}" }
										),
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											var path = oCustomData[0].mProperties.value;
											sap.m.URLHelper.redirect("../files/announcements/" + path, true)
										}
									}
								)
							}
						),
						
						new sap.ui.table.Column(
							"colEdit", 
							{
								width: "4rem",
								hAlign: sap.ui.core.HorizontalAlign.Center,
								label: new sap.m.Label(
									"lblEdit",
									{
										text: "Editar"
									}
								),
								template:  new sap.m.Button(
									"btnEdit",
									{
										icon: "sap-icon://user-edit", 
										tooltip: "Modificar Anuncio",
										customData: new sap.ui.core.CustomData(
											{ key: "id", value: "{id}" }
										),
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											var id = oCustomData[0].mProperties.value;
											
											if(id){
												
												oDate.setText("");
												oSubject.setValue("");
												oBody.setValue("");
												oUploader.setValue("");
												
												oUploader.setEnabled(false);
												
												var oModelAnnouncement = new sap.ui.model.json.JSONModel("getAnnouncement?id=" + id);
												oModelAnnouncement.attachRequestCompleted(
													function(){
														var oProp = oModelAnnouncement.getProperty("/");
														oDate.setText(oProp.date);
														oSubject.setValue(oProp.subject);
														oBody.setValue(oProp.body);
														oUploader.setValue(oProp.file_name);

														oUploader.setEnabled(false);
														
													}
												);

												oDialogEditAnnouncement.setTitle('Actualizar Anuncio.');
												oDialogEditAnnouncement.removeAllCustomData();
												oDialogEditAnnouncement.addCustomData(
													new sap.ui.core.CustomData(
														{
															key: "mode",
															value: "edit"
														}
													)
												);
												oDialogEditAnnouncement.addCustomData(
													new sap.ui.core.CustomData(
														{
															key: "id",
															value: id
														}
													)
												);
												
												oDialogEditAnnouncement.open();
												
											}
												
										}
									}
								)
							}
						),
						new sap.ui.table.Column(
								"colDelete", 
								{
									width: "5rem",
									hAlign: sap.ui.core.HorizontalAlign.Center,
									label: new sap.m.Label(
										"lblDelete",
										{
											text: "Eliminar"
										}
									),
									template:  new sap.m.Button(
										"btnDelete",
										{
											icon: "sap-icon://delete", 
											tooltip: "Eliminar Anuncio",

											customData: [
												new sap.ui.core.CustomData(
													{ key: "id", value: "{id}" }
												),
												new sap.ui.core.CustomData(
													{ key: "subject", value: "{subject}" }
												)
											],
											
											press: function(oEvent){
												var oCustomData = oEvent.getSource().getCustomData();
												var id = oCustomData[0].mProperties.value;
												var subject = oCustomData[1].mProperties.value;
												
												executeSaveAnnouncement("delete", id, subject);
													
											}
										}
									)
								}
							)
					
					],
					
				}
				
			).setModel(oModelAnnouncements);
			
			
			tblAnnouncements.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>