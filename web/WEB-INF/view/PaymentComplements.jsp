<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Complementos de Pago</title>
		
	   	<script id='sap-ui-bootstrap' 
	          src='${pageContext.request.contextPath}/resources/sap-ui-core.js'  
	          data-sap-ui-theme='sap_bluecrystal'  
	          data-sap-ui-libs='sap.m, sap.ui.table, sap.ui.layout, sap.ui.unified, sap.ui.core'></script> 
	    
	   	<script id='suppliers-js' src='<c:url value='/js/common/SuppliersUtils.js'/>'></script> 
	   	<script id='jsonmodel-js' src='<c:url value='/js/common/JsonModelUtils.js'/>'></script> 
	    
		<script type="text/javascript">
			var gvCompany = "";
			var gvYear = "";
			var gvDoc = "";

			ogModel.loadData("listPaymentComplement");
			ogModel.setSizeLimit(2000);
			
			function validateForm(){
				var currentField = oUploaderXml;
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				if(!validateEmptyText(currentField)){
					return false;
				}
				
				currentField = oUploaderPdf;
				currentField.setValueState(sap.ui.core.ValueState.None);
				
				if(!validateEmptyText(currentField)){
					return false;
				}
				
				return true;
				
			};
			
			var oUploaderXml = new sap.ui.unified.FileUploader(
				"xmlFile",
				{
					fileType: "xml",
					placeholder: "Selecciona XML",
					uploadUrl: "uploadXmlPayment",
					buttonText: "Selecciona...",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,

					iconOnly: true,
					icon: "sap-icon://attachment-html",
					
					width: "100%",
					
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					)
				
				}
			);
			
			var oUploaderPdf =  new sap.ui.unified.FileUploader(
				"pdfFile",
				{
					fileType: "pdf",
					placeholder: "Selecciona PDF",
					uploadUrl: "uploadPdfPayments",
					uploadOnChange: true,
					multiple: false,
					sendXHR: true,
					
					iconOnly: true,
					icon: "sap-icon://pdf-attachment",
					
					width: "100%",
					
					parameters: new sap.ui.unified.FileUploaderParameter(
						{
							name: "${_csrf.parameterName}",
							value: "${_csrf.token}"
						}
					)
				
				}
			);
			
			var pagePaymentRegistration = new sap.m.Page(
				"pagePaymentRegistration",
				{
					showHeader: false,
					showFooter: false,
					
					content: [
						new sap.m.VBox(
							"vBoxNewUser", 
							{
								fitContainer: true,
								
								alignItems: sap.m.FlexAlignItems.Stretch, 
								
								items: [ 
									
									new sap.m.Label(
										"lblXml", 
										{
											text:"Archivo XML",
											labelFor: "uploaderXml",
											required: true
										}
									),
									
									oUploaderXml,

									new sap.m.Label(
										"lblPdf", 
										{
											text:"Archivo PDF",
											labelFor: "uploaderPdf",
											required: true
										}
									),
									
									oUploaderPdf

								]
							}
						)
					]
				}
			);
			
			function savePayment(){
				
				var sTarget = "paymentComplement";
				
				var sParams = "payment=" + gvDoc + "&" +
							  "company=" + gvCompany + "&" +
							  "year=" + gvYear;
				sParams +=  "&" + "_csrf=" + "${_csrf.token}";
				
				executePostSaveData(
						sTarget, 
						sParams, 
						function(){
							oUploaderXml.setValue(null);
							oUploaderPdf.setValue(null);
							
							oDialogPayment.close();
							
						}
				);
				
			}
			
			var oDialogPayment = new sap.m.Dialog(
				"dialogEditUser",
				{
					title: 'Registrar Complemento de Pago.',
					
					stretch: true,
					
					content: [
						pagePaymentRegistration
					],
					
					beginButton: new sap.m.Button(
						"btnSave",
						{
							text: 'Guardar',
							press: function (oEvent) {
								
								if(validateForm()){
									savePayment();
								}
								
							}
						}
					),
					
					endButton: new sap.m.Button(
						"brnCancelSave",
						{
							text: 'Cancelar',
							press: function (oEvent) {
								if(oDialogPayment){
									oDialogPayment.close();
								}
							}
						}
					)
				
				}
			);
			
			var tblPayments = new sap.ui.table.Table(
				"tblPayments",
				{
					selectionMode: sap.ui.table.SelectionMode.None,
					visibleRowCount: 15,
					rows: "{/}",
					ariaLabelledBy: "title",
					
					enableCellFilter: true,
					
					title: new sap.m.HBox(
						"hBoxTitle", 
						{
							items: [
								new sap.ui.core.Icon(
									"iconTableTitle",
									{
										src: "sap-icon://money-bills",
										alt: "Usuarios",
										size: "1.25rem"
									}
								),
								new sap.m.Title(
									"tableTitle",
									{
										text: "Complementos de Pago", 
										titleStyle: "H3"
									}
								).addStyleClass("sapUiTinyMarginBeginEnd")
							]
						}
					),
					
					noData: new sap.m.Text(
						"textNoData", 
						{
							text: "No existen datos para mostrar."
						}
					).addStyleClass("sapUiMediumMargin"),
					
					columns: [
						
						new sap.ui.table.Column(
							"colDocPagoSAP", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblDocPagoSAP",
									{
										text: "Documento de Pago SAP"
									}
								),
								template: new sap.m.Text(
									"txtDocPagoSAP",
									{
										text: "{paymentDoc}",
										wrapping: true
									}
								)
							}
						),
						
						new sap.ui.table.Column(
							"colReferenciaSAP", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblReferenciaSAP",
									{
										text: "Referencia Documento SAP"
									}
								),
								template: new sap.m.Text(
									"txtReferenciaSAP",
									{
										text: "{referenceDoc}",
										wrapping: true
									}
								)
							}
						),

						new sap.ui.table.Column(
							"colReferenciaSAT", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblReferenciaSAT",
									{
										text: "Referencia Documento SAT"
									}
								),
								template: new sap.m.Text(
									"txtReferenciaSAT",
									{
										text: "{referenceFolio}",
										wrapping: true
									}
								)
							}
						),

						new sap.ui.table.Column(
							"colImporte", 
							{
								width: "10rem",
								label: new sap.m.Label(
									"lblImporte",
									{
										text: "Importe"
									}
								),
								template: new sap.m.Text(
									"txtImporte",
									{
										text: "{amount}",
										wrapping: true
									}
								)
							}
						),

						new sap.ui.table.Column(
							"colSelect", 
							{
								width: "4rem",
								label: new sap.m.Label(
									"lblSelect",
									{
										text: "Seleccionar"
									}
								),
								template:  new sap.m.Button(
									"btnSelect",
									{
										icon: "sap-icon://attachment", 
										tooltip: "Complemento de Pago",
										visible: "{visible}",
										customData: [
											new sap.ui.core.CustomData( { key: "payment", value: "{paymentDoc}" } ),
											new sap.ui.core.CustomData( { key: "company", value: "{company}" } ),
											new sap.ui.core.CustomData( { key: "year", value: "{year}" } )	
										],
										
										press: function(oEvent){
											var oCustomData = oEvent.getSource().getCustomData();
											
											oUploaderXml.setValue(null);
											oUploaderPdf.setValue(null);
											
											gvDoc = oCustomData[0].mProperties.value;
											gvCompany = oCustomData[1].mProperties.value;
											gvYear = oCustomData[2].mProperties.value;
											
											oDialogPayment.open();
												
										}
									}
								)
							}
						)
						
					],
					
				}
				
			).setModel(ogModel);
			
			tblPayments.placeAt("divForm");
		
		</script>
		
	</head>
	<body>
		<div id="divForm"></div>
	</body>
</html>