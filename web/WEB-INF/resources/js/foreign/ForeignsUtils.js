var oDocumentTypeSelect = new sap.m.Select(
	"documentTypeSelect",
	{
		items: [
			new sap.ui.core.Item(
				"noDocTypeItem",
				{
					key: "",
					text: "",
				}
			),
			new sap.ui.core.Item(
				"invoiceItem",
				{
					key: "F",
					text: "Factura"
				}
			),
			new sap.ui.core.Item(
				"creditItem",
				{
					key: "NC",
					text: "Nota de Crédito"
				}
			),
		]
	}
).addStyleClass("sapUiSmallMarginBottom");

var oCompanySelect = new sap.m.Select(
	"companySelect",
	{
		items: [
			new sap.ui.core.Item(
				"noCompanyItem",
				{
					key: "",
					text: ""
				}
			),
			new sap.ui.core.Item(
				"lbqItem",
				{
					key: "ZMX1",
					text: "LBQ"
				}
			),
			new sap.ui.core.Item(
				"bqmItem",
				{
					key: "ZMX2",
					text: "BQM"
				}
			),
		]
	}
).addStyleClass("sapUiSmallMarginBottom");

var oDeliveryNumber = new sap.m.MaskInput(
	"deliveryNumber",
	{
		mask: "9999999999",
		placeholderSymbol: " ",
		placeholder: "XXXXXXXXXX",
		required: true,
		width: "100%"
	}
).addStyleClass("sapUiSmallMarginBottom");

var oInvoiceNumber = new sap.m.Input(
	"invoiceNumber",
	{
		type: "Text",
		required: true,
		width: "100%",
		maxLength: 30
	}
).addStyleClass("sapUiSmallMarginBottom");

var oInvoiceDate = new sap.m.DatePicker(
	"invoiceDate",
	{
		displayFormat: "dd/MM/yyyy",
		valueFormat: "yyyy-MM-dd",
		required: true,
		placeholder: "dd/mm/yyyy",
		width: "100%"
		
	}
).addStyleClass("sapUiSmallMarginBottom");

var oDocumentDate = new sap.m.DatePicker(
	"documentDate",
	{
		displayFormat: "dd/MM/yyyy",
		valueFormat: "yyyy-MM-dd",
		required: true,
		placeholder: "dd/mm/yyyy",
		width: "100%"
		
	}
).addStyleClass("sapUiSmallMarginBottom");

var oInvoiceAmount = new sap.m.Input(
	"invoiceAmount",
	{
		type: "Text",
		placeholder: "Hasta 11 posiciones enteras + 2 decimales",
		required: true,
		width: "100%",
		maxLength: 14
	}
).addStyleClass("sapUiSmallMarginBottom");


var oInvoiceDescription = new sap.m.Input(
	"invoiceDescription",
	{
		type: "Text",
		maxLength: 50,
		required: true,
		width: "100%"
		
	}
).addStyleClass("sapUiSmallMarginBottom");


var oInvoiceCurrency = new sap.m.Input(
	"invoiceCurrency",
	{
		type: "Text",
		maxLength: 3,
		required: true,
		placeholder: "---",
		width: "100%"
		
	}
).addStyleClass("sapUiSmallMarginBottom");

oCoinTypeSelect=new sap.m.Select(
	"coinTypeSelect",
	{
		items: [
			new sap.ui.core.Item(
				"coinTypeItem",
				{
					key: "",
					text: "",
				}
			),
			new sap.ui.core.Item(
				"usdItem",
				{
					key: "USD",
					text: "USD"
				}
			),
			new sap.ui.core.Item(
				"eurItem",
				{
					key: "EUR",
					text: "EUR"
				}
			),
                    new sap.ui.core.Item(
				"gbpItem",
				{
					key: "GBP",
					text: "GBP"
				}
			),
		]
	}
).addStyleClass("sapUiSmallMarginBottom");