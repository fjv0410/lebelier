var ogModel = new sap.ui.model.json.JSONModel();

ogModel.attachRequestSent(
		function(){
		}
	).attachRequestFailed(
		function(oEvent){
			
	   		var list = new sap.m.List();
       	  	var item = new sap.m.StandardListItem();
        	  
        	item.setTitle("Sucedió un error al cargar la información.");
       	  
        	list.addItem(item);

			createMessage("Error", "Error", list);
			
		}
	).attachRequestCompleted(
		function(oEvent){
		}
	);
