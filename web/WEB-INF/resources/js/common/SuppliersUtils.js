var ogBusyInd = new sap.m.BusyDialog(
	{
		customIcon: "/suppliers/img/puce.png",
	}
);

function createMessage( sMsgType, sTitle, lstMsgs ){
	
	var dialog = new sap.m.Dialog(
		{
			type: 'Message',
			beginButton: new sap.m.Button({
				text: 'Aceptar',
				press: function () {
					dialog.close();
				}
			}),
			afterClose: function() {
				dialog.destroy();
			}
		}
	);
	
	dialog.setTitle(sTitle);
	
	dialog.setState(sMsgType);
	
	dialog.addContent(
		lstMsgs
	);
	
	dialog.open();
	
}

function executePostSaveData(p_target, p_params, f_onSucced){

	ogBusyInd.open();
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', p_target, true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		
	xhr.onreadystatechange = function() {
	    if (xhr.readyState == XMLHttpRequest.DONE) {
	    	if (xhr.status == "200") {
				
	    		try {
		    		
			    	var result = xhr.getResponseHeader("process_result");
			    	var message = xhr.getResponseHeader("process_msg");
			    	
			    	if(result == "Success"){
			    		f_onSucced();
			    	}
			    	
			    	var headers = xhr.getAllResponseHeaders();
			    	var arr = headers.trim().split(/[\r\n]+/);
			    	
			    	var lstValues = new sap.m.List();
			    	
			        arr.forEach(
			       	  function (line) {
				         var parts = line.split(': ');
				         var header = parts.shift();
				         var value = parts.join(': ');
				          
				         if(header.indexOf("process_msg_lst") == 0){
				       	   var item = new sap.m.StandardListItem();

				           item.setTitle(value);
				        	  
				           lstValues.addItem(item);
				           
				         }
				          
			          }
			       	);
			        
			    	createMessage(result, "Resultado del proceso.", lstValues);
			    	
	    		}catch(error) {
			   	  var lstValuesExc = new sap.m.List();
		       	  var itemExc = new sap.m.StandardListItem();
		        	  
		       	  itemExc.setTitle("Excepción en cliente:");
		       	  itemExc.setDescription(error);
		       	  
		          lstValuesExc.addItem(itemExc);
			    	
	      		  createMessage(result, "Sucedió un error.", lstValuesExc);
	    		}
				
	    	}else{
		   		var lstValues = new sap.m.List();
	       	  	var item = new sap.m.StandardListItem();
	        	  
	        	item.setTitle("Estatus de la respuesta: ");
	       	   	item.setDescription(xhr.status);
	       	  
	           	lstValues.addItem(item);

		    	createMessage("Error", "Sucedió un error.", lstValues);
		    	
		    }
	    	
	    	ogBusyInd.close();
	    	
	    }
	}
		
	xhr.send(p_params);
		
}

function validateSelectedKey(oSelect){

	if(oSelect.getSelectedKey() == null || oSelect.getSelectedKey() == ""){
		oSelect.setValueState(sap.ui.core.ValueState.Error);
		oSelect.setValueStateText("Seleccione una opción válida.");
		return false;
	}
	oSelect.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

function validateDeliveryNumber(oDeliveryNumber){
	
	var lengthString = 10;
	
	if(oDeliveryNumber == null || !/^(\d){10}$/.test(oDeliveryNumber.getValue())){
		oDeliveryNumber.setValueState(sap.ui.core.ValueState.Error);
		oDeliveryNumber.setValueStateText("Número de Entrega incorrecto: proporcione 10 dígitos.");
		
		return false;
	}
	oDeliveryNumber.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

function validateInvoiceNumber(oInvoiceNumber){
	
	var lengthString = 10;

	if(oInvoiceNumber == null || !/^(\d){10}$/.test(oInvoiceNumber.getValue())){
		oInvoiceNumber.setValueState(sap.ui.core.ValueState.Error);
		oInvoiceNumber.setValueStateText("Número de Factura incorrecto: proporcione 10 dígitos.");
		
		return false;
	}
	oInvoiceNumber.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

function validateDate(oDate){

	if(oDate == null || oDate.getValue() == "" || !oDate.isValidValue() ){
		oDate.setValueState(sap.ui.core.ValueState.Error);
		oDate.setValueStateText("Proporcione una fecha en el formato indicado.");
		
		return false;
	}
	oDate.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

function validateAmount(oAmount){

	if(oAmount == null || !/^\d{1,11}\.\d{2}$/.test(oAmount.getValue()) ){
		oAmount.setValueState(sap.ui.core.ValueState.Error);
		oAmount.setValueStateText("Proporcione un monto correcto: hasta 11 posiciones enteras + dos decimales.");
		
		return false;
	}
	oAmount.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

function validateEmptyText(oText){

	if(oText != null && oText.getValue() == ""){
		oText.setValueState(sap.ui.core.ValueState.Error);
		oText.setValueStateText("Campo obligatorio, proporcione un valor.");
		
		return false;
	}
	oText.setValueState(sap.ui.core.ValueState.None);
	
	return true;

}

